<?php /* Smarty version 3.1.25, created on 2016-10-18 22:52:25
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/vocabulary/main.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:152485817258068b892640e6_71424100%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '639388be8fb8a4e1fec7901949d75475677a6b24' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/vocabulary/main.tpl',
      1 => 1458911907,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152485817258068b892640e6_71424100',
  'variables' => 
  array (
    'lang' => 0,
    'languages' => 0,
    '_GET' => 0,
    'language' => 0,
    'vocabularys' => 0,
    'vocabulary' => 0,
    'letter' => 0,
    'previousLetter' => 0,
    'word' => 0,
    'previousWord' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_58068b8a4082e1_39750287',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_58068b8a4082e1_39750287')) {
function content_58068b8a4082e1_39750287 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '152485817258068b892640e6_71424100';
?>
<div class="col-md-24">
    <a href="/user/vocabulary/add" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> <?php echo $_smarty_tpl->tpl_vars['lang']->value['vocabulary']['add'];?>
</a>
    <hr>
    <form role="form" method="GET" action="">

        <div class="row" style="padding-bottom: 10px;">

            <div class="col-md-10">
                <select class="form-control sourcelang width100" name="sourcelang" >
                    <option value=""><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['source_lang'];?>
</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['languages']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>   
                        <?php if ($_smarty_tpl->tpl_vars['_GET']->value['sourcelang'] == $_smarty_tpl->tpl_vars['language']->value->ID) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
" selected="selected"><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>
                        <?php }?>
                    <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
                </select>
            </div>
            <div class="col-md-10">
                <select class="form-control sourcelang width100" name="tolang" >
                    <option value=""><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['to_lang'];?>
</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['languages']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>      
                        <?php if ($_smarty_tpl->tpl_vars['_GET']->value['tolang'] == $_smarty_tpl->tpl_vars['language']->value->ID) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
" selected="selected"><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>
                        <?php }?>
                    <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
                </select>
            </div>
            <div class="col-md-4"> 
                <button type="submit" class="btn btn-info center-block width100" ><i class="fa fa-filter"></i> <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['button_filter'];?>
</button>

            </div>
        </div>
    </form>
    <?php $_smarty_tpl->tpl_vars["previousLetter"] = new Smarty_Variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["previousWord"] = new Smarty_Variable('', null, 0);?>
    <?php
$_from = $_smarty_tpl->tpl_vars['vocabularys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['vocabulary'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['vocabulary']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['vocabulary']->value) {
$_smarty_tpl->tpl_vars['vocabulary']->_loop = true;
$foreach_vocabulary_Sav = $_smarty_tpl->tpl_vars['vocabulary'];
?>
        <?php $_smarty_tpl->tpl_vars["letter"] = new Smarty_Variable(mb_strtoupper(substr($_smarty_tpl->tpl_vars['vocabulary']->value->ORIGINAL,0,1), 'UTF-8'), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["word"] = new Smarty_Variable($_smarty_tpl->tpl_vars['vocabulary']->value->ORIGINAL, null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['letter']->value !== $_smarty_tpl->tpl_vars['previousLetter']->value) {?>
            <h1><?php echo $_smarty_tpl->tpl_vars['letter']->value;?>
</h1>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['word']->value == $_smarty_tpl->tpl_vars['previousWord']->value) {?>
            <div class="row">
                <div class="col-md-11 col-md-offset-11">
                    <?php echo $_smarty_tpl->tpl_vars['vocabulary']->value->TRANSLATED;?>
<br />
                </div>
            </div>
        <?php } else { ?>
            <div class="row padding-top-3 margin-top-10" style="border-top: 1px solid #ccc;">
                <div class="col-md-11">
                    <?php echo $_smarty_tpl->tpl_vars['vocabulary']->value->ORIGINAL;?>

                </div>
                <div class="col-md-11">
                    <?php echo $_smarty_tpl->tpl_vars['vocabulary']->value->TRANSLATED;?>
<br />
                </div>
            </div>
        <?php }?>

        <?php $_smarty_tpl->tpl_vars["previousLetter"] = new Smarty_Variable(mb_strtoupper(substr($_smarty_tpl->tpl_vars['vocabulary']->value->ORIGINAL,0,1), 'UTF-8'), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["previousWord"] = new Smarty_Variable($_smarty_tpl->tpl_vars['vocabulary']->value->ORIGINAL, null, 0);?>
    <?php
$_smarty_tpl->tpl_vars['vocabulary'] = $foreach_vocabulary_Sav;
}
if (!$_smarty_tpl->tpl_vars['vocabulary']->_loop) {
?>
        <?php echo $_smarty_tpl->tpl_vars['lang']->value['vocabulary']['no_text'];?>

    <?php
}
?>

</div><?php }
}
?>