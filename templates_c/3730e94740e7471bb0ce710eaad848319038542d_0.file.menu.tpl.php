<?php /* Smarty version 3.1.25, created on 2016-10-18 22:42:23
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/inc/menu.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:7208648125806892ff11424_45385408%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3730e94740e7471bb0ce710eaad848319038542d' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/inc/menu.tpl',
      1 => 1466875869,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7208648125806892ff11424_45385408',
  'variables' => 
  array (
    'lang' => 0,
    'menu' => 0,
    'row' => 0,
    'urlmenu' => 0,
    'name' => 0,
    'item' => 0,
    'key' => 0,
    'user' => 0,
    'alerts' => 0,
    'alert' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_580689300b3b15_32350381',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_580689300b3b15_32350381')) {
function content_580689300b3b15_32350381 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '7208648125806892ff11424_45385408';
?>
<!-- MENU -->
<?php ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['home'];
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['projects'];
$_tmp2=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['princip'];
$_tmp3=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['statictics'];
$_tmp4=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['statictics_users'];
$_tmp5=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['statictics_users_title'];
$_tmp6=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['statictics_projects'];
$_tmp7=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['statictics_projects_title'];
$_tmp8=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['menu']['contact'];
$_tmp9=ob_get_clean();
$_smarty_tpl->tpl_vars['menu'] = new Smarty_Variable(array($_tmp1=>array("icon"=>"home","url"=>"home"),$_tmp2=>array("icon"=>"file","url"=>"project"),$_tmp3=>array("icon"=>"question","url"=>"error"),$_tmp4=>array("optgroup"=>array($_tmp5=>array("icon"=>"users","url"=>"error","title"=>$_tmp6),$_tmp7=>array("icon"=>"gamepad","url"=>"error","title"=>$_tmp8))),$_tmp9=>array("icon"=>"envelope","url"=>"user/register")), null, 0);?>


<div class="visible-xs visible-sm mobile-menu red-background ">
    <a href="/" class="">
        <img class="center-block" src="/img/logo-dark-mini.png" />
    </a>

    <?php $_smarty_tpl->tpl_vars["urlmenu"] = new Smarty_Variable(explode("/",$_SERVER['REQUEST_URI']), null, 0);?>

    <select id="mobile-menu" class="selectpicker shadow" data-mobile="true" data-width='100%' data-style="btn-warning">
        <?php
$_from = $_smarty_tpl->tpl_vars['menu']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
$_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['name']->value => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
            <?php if (!$_smarty_tpl->tpl_vars['row']->value["optgroup"]) {?>
                <option data-icon="fa-<?php echo $_smarty_tpl->tpl_vars['row']->value["icon"];?>
" value='/<?php echo $_smarty_tpl->tpl_vars['row']->value["url"];?>
' <?php ob_start();
echo $_smarty_tpl->tpl_vars['urlmenu']->value[1];
$_tmp10=ob_get_clean();
if ($_smarty_tpl->tpl_vars['row']->value["url"] == $_tmp10) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</option>
            <?php } else { ?>
                <optgroup label="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
">
                    <?php
$_from = $_smarty_tpl->tpl_vars['row']->value["optgroup"];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                        <option  value='/<?php echo $_smarty_tpl->tpl_vars['item']->value["url"];?>
' title="<span class='fa fa-<?php echo $_smarty_tpl->tpl_vars['item']->value["icon"];?>
'></span> <?php echo $_smarty_tpl->tpl_vars['item']->value["title"];?>
" <?php ob_start();
echo $_smarty_tpl->tpl_vars['urlmenu']->value[1];
$_tmp11=ob_get_clean();
if ($_smarty_tpl->tpl_vars['item']->value["url"] == $_tmp11) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                </optgroup>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
        <?php if (!$_smarty_tpl->tpl_vars['user']->value) {?>
            <optgroup label="<?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['account'];?>
">
                <option  value='/user/login' title="<span class='fa fa-sign-in'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['login'];?>
" <?php ob_start();
echo $_SERVER['REQUEST_URI'];
$_tmp12=ob_get_clean();
if ("/user/login" == $_tmp12) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['login'];?>
</option>
                <option  value='/user/register' title="<span class='fa fa-list'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['register'];?>
" <?php ob_start();
echo $_SERVER['REQUEST_URI'];
$_tmp13=ob_get_clean();
if ("/user/register" == $_tmp13) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['register'];?>
</option>
            </optgroup>
        <?php } else { ?>
            <optgroup label="<?php echo $_smarty_tpl->tpl_vars['user']->value['name'];?>
">
                <option value="/project/my-projects"  <?php ob_start();
echo $_SERVER['REQUEST_URI'];
$_tmp14=ob_get_clean();
if ("/project/my-projects" == $_tmp14) {?>selected="selected"<?php }?> title='<span class="fa fa-file"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['my_projects'];?>
'><?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['my_projects'];?>
</option>
                <option value="/user/profile"  <?php ob_start();
echo $_SERVER['REQUEST_URI'];
$_tmp15=ob_get_clean();
if ("/user/profile" == $_tmp15) {?>selected="selected"<?php }?> title='<span class="fa fa-list-alt"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['show_profile'];?>
'><?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['show_profile'];?>
</option>
                <option value="/user/settings"  <?php ob_start();
echo $_SERVER['REQUEST_URI'];
$_tmp16=ob_get_clean();
if ("/user/settings" == $_tmp16) {?>selected="selected"<?php }?> title='<span class="fa fa-cogs"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['settings'];?>
'><?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['settings'];?>
</option>
                <option value="/user/vocabulary"  <?php ob_start();
echo $_SERVER['REQUEST_URI'];
$_tmp17=ob_get_clean();
if ("/user/vocabulary" == $_tmp17) {?>selected="selected"<?php }?> title='<span class="fa fa-list"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['vocabulary'];?>
'><?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['vocabulary'];?>
</option>
                <option value="/user/logout"  <?php ob_start();
echo $_SERVER['REQUEST_URI'];
$_tmp18=ob_get_clean();
if ("/user/logout" == $_tmp18) {?>selected="selected"<?php }?> title='<span class="fa fa-sign-out"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['logout'];?>
'><?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['logout'];?>
</option>
            </optgroup>
        <?php }?>
    </select>
</div>

<div class="hidden-xs hidden-sm">
    <div class="navbar header navbar-default">
        <div class="container">
            <div class="col-md-12">
                <div class="navbar-header">
                    <a href="/" class="navbar-brand logo hidden-xs"><h1><?php echo mb_strtolower($_smarty_tpl->tpl_vars['lang']->value['webname'], 'UTF-8');?>
</h1></a>
                </div>
            </div>
            <div class="col-md-12">
                <?php echo $_smarty_tpl->getSubTemplate ("view/inc/user-card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </div>



            <div class="col-md-24 padding-0">
                <div class="navbar-collapse collapse">
                    <div class="masthead ">
                        <nav class="">
                            <ul class="nav nav-justified">
                                <li><a class="menu-item" href='/project'><span class='fa fa-file'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['projects'];?>
</a></li>
                                <li><a class="menu-item" href='/home'><span class='fa fa-question'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['princip'];?>
</a></li>
                                <li><a class="menu-item" href='/statistic'><span class='fa fa-bar-chart'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['statictics'];?>
</a></li>
                                <li><a class="menu-item" href='/contact'><span class='fa fa-envelope'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['contact'];?>
</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>    
            </div>

        </div>
    </div>
</div>
<!-- /MENU -->
<nav class="navbar-fixed-top red-background shadow padding-0 hidden my-menu">
    <div class="container">
        <div class="col-md-24">

            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav nav-justified ">
                    <li><a class="menu-item" href='/project'><span class='fa fa-file'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['projects'];?>
</a></li>
                    <li><a class="menu-item" href='/home'><span class='fa fa-question'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['princip'];?>
</a></li>
                    <li><a class="menu-item" href='/home'><span class='fa fa-bar-chart'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['statictics'];?>
</a></li>
                    <li><a class="menu-item" href='/home'><span class='fa fa-envelope'></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['menu']['contact'];?>
</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</nav>



<div class="container">

    <div id="alert">
        <?php
$_from = $_smarty_tpl->tpl_vars['alerts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['alert'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['alert']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['alert']->value) {
$_smarty_tpl->tpl_vars['alert']->_loop = true;
$foreach_alert_Sav = $_smarty_tpl->tpl_vars['alert'];
?>
            <p class='bg-<?php echo $_smarty_tpl->tpl_vars['alert']->value['type'];?>
 padding-10'>
                <?php echo $_smarty_tpl->tpl_vars['alert']->value['text'];?>
<br />                                            
            </p>
        <?php
$_smarty_tpl->tpl_vars['alert'] = $foreach_alert_Sav;
}
?>
    </div><?php }
}
?>