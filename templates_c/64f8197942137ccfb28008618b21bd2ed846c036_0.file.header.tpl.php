<?php /* Smarty version 3.1.25, created on 2017-07-14 14:44:10
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad/view/inc/header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:20564933025968bc9a24a694_79014270%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64f8197942137ccfb28008618b21bd2ed846c036' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad/view/inc/header.tpl',
      1 => 1458906505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20564933025968bc9a24a694_79014270',
  'variables' => 
  array (
    'lang' => 0,
    'title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_5968bc9a2d3244_64856191',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5968bc9a2d3244_64856191')) {
function content_5968bc9a2d3244_64856191 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '20564933025968bc9a24a694_79014270';
?>
<!DOCTYPE html>
<html lang="<?php echo $_smarty_tpl->tpl_vars['lang']->value['html']['lang'];?>
">
    <head>
        <meta charset="UTF-8" />
        <title><?php if ($_smarty_tpl->tpl_vars['title']->value) {
echo ucfirst($_smarty_tpl->tpl_vars['title']->value);?>
 - <?php echo $_smarty_tpl->tpl_vars['lang']->value['html']['title'];
} else {
echo $_smarty_tpl->tpl_vars['lang']->value['html']['title'];
}?></title>
        <!--
==========================================================================================
==========================================================================================
   _____ _____   ____ _____ _      ______ _____             _      ______ _____ _______ _ 
  / ____|  __ \ / __ \_   _| |    |  ____|  __ \      /\   | |    |  ____|  __ \__   __| |
 | (___ | |__) | |  | || | | |    | |__  | |__) |    /  \  | |    | |__  | |__) | | |  | |
  \___ \|  ___/| |  | || | | |    |  __| |  _  /    / /\ \ | |    |  __| |  _  /  | |  | |
  ____) | |    | |__| || |_| |____| |____| | \ \   / ____ \| |____| |____| | \ \  | |  |_|
 |_____/|_|     \____/_____|______|______|_|  \_\ /_/    \_\______|______|_|  \_\ |_|  (_)
                                                                                          
                       This part of code show how the webpage (don't) work. 
                       So skip this part if you don't know that information.                                                                      
===========================================================================================
===========================================================================================

        -->
        
        <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['lang']->value['meta']['description'];?>
" />
        <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['lang']->value['meta']['keyword'];?>
" />
        <meta name="robots" content="noindex, nofollow">
        <meta name="author" content="Filip Macek" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="theme-color" content="#da4848"> 
        <meta name="msapplication-navbutton-color" content="#da4848"> 
        <meta name="apple-mobile-web-app-status-bar-style" content="#da4848"> 

        
        <?php echo '<script'; ?>
 src='https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js'><?php echo '</script'; ?>
>
        
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        
        <link href="/inc/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <?php echo '<script'; ?>
 src="/inc/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
        
        <link href="/inc/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen" />
        
        <link href="/inc/css/flags.css" rel="stylesheet" />
        
        <link href="/inc/bootstrap/plugin/messenger/build/css/messenger.css" rel="stylesheet">
        <link href="/inc/bootstrap/plugin/messenger/build/css/messenger-theme-future.css" rel="stylesheet">
        
        <link href="/inc/bootstrap/plugin/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
        
        <link href="/inc/bootstrap/plugin/select/dist/css/bootstrap-select.min.css" rel="stylesheet"/>
        
        <link href="/inc/css/style.css" rel="stylesheet" />

    </head>
    <body>
        <?php echo $_smarty_tpl->getSubTemplate ("view/inc/menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

<?php }
}
?>