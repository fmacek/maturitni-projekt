<?php /* Smarty version 3.1.25, created on 2016-12-06 20:56:16
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/user/login.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:7701237584717e0c8e104_51054654%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f99d72b513eed8cdf6b32333ed33238b1939045' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/user/login.tpl',
      1 => 1458909237,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7701237584717e0c8e104_51054654',
  'variables' => 
  array (
    'lang' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_584717e0dcd3e3_19365710',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_584717e0dcd3e3_19365710')) {
function content_584717e0dcd3e3_19365710 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '7701237584717e0c8e104_51054654';
?>
<h2><?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['login'];?>
</h2>
<?php if ($_smarty_tpl->tpl_vars['user']->value) {?>
    <meta http-equiv="refresh" content="1; URL=/">      
<?php }?>
<form class="form-horizontal" role="form" method="POST" action="">
    <div class="form-group">
        <div class="input-group">
            <label class="sr-only" for="email"><?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['email'];?>
</label>
            <div class="input-group-addon"><span class="fa fa-at"></span></div>
            <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['email'];?>
" required="required">
        </div>
        <div class="input-group">
            <label class="sr-only" for="pass"><?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['pass'];?>
</label>
            <div class="input-group-addon"><span class="fa fa-key"></span></div>
            <input type="password" class="form-control" id="pass" name="pass" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['pass'];?>
" required="required">
        </div>
    </div>
    <br><br>
    <button type="submit" class="btn btn-default center-block width100" name="login"><span class="fa fa-sign-in"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['send'];?>
</button>
        <a href="user/register" class="btn btn-default width100"><span class="fa fa-list"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['register'];?>
</a>

</form><?php }
}
?>