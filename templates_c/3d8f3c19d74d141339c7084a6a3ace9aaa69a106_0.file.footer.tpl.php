<?php /* Smarty version 3.1.25, created on 2017-07-14 14:44:10
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad/view/inc/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:16913100455968bc9a4bdab3_82224286%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d8f3c19d74d141339c7084a6a3ace9aaa69a106' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad/view/inc/footer.tpl',
      1 => 1458916376,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16913100455968bc9a4bdab3_82224286',
  'variables' => 
  array (
    'lang' => 0,
    'webLangVersion' => 0,
    'weblang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_5968bc9a4d7ff1_56856421',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5968bc9a4d7ff1_56856421')) {
function content_5968bc9a4d7ff1_56856421 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'inc/libs/Smarty/libs/plugins/modifier.date_format.php';

$_smarty_tpl->properties['nocache_hash'] = '16913100455968bc9a4bdab3_82224286';
?>
<!-- FOOTER -->

</div>
<div class="visible-xs">
    <hr />
    <p class="text-center">&copy; 2014 - <?php echo smarty_modifier_date_format(time(),"%Y");?>
 <?php echo $_smarty_tpl->tpl_vars['lang']->value["webname"];?>
</p>
</div>

<div class="hidden-xs">

    <footer class="footer">
        <div class="col-md-8 col-md-offset-8">
            <p class="text-center">&copy; 2015 - <?php echo smarty_modifier_date_format(time(),"%Y");?>
 Filip Macek</p>
        </div>
        <div class="col-md-8 text-right">
            <form method="POST" id="changelang">
                <select name="selectlang" id="changelangselect" class="form-control selectpicker" data-mobile="true">
                    <option><?php echo $_smarty_tpl->tpl_vars['lang']->value['lang']['select'];?>
</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['webLangVersion']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['weblang'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['weblang']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['weblang']->value) {
$_smarty_tpl->tpl_vars['weblang']->_loop = true;
$foreach_weblang_Sav = $_smarty_tpl->tpl_vars['weblang'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['weblang']->value->CODE;?>
"><?php echo $_smarty_tpl->tpl_vars['weblang']->value->NAME;?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['weblang'] = $foreach_weblang_Sav;
}
?>
                </select>
            </form>
        </div>

    </footer>
</div>
<?php echo '<script'; ?>
 src="/inc/js/script.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/inc/bootstrap/plugin/x-editable/bootstrap3-editable/js/bootstrap-editable.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://apis.google.com/js/platform.js" async defer><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/inc/bootstrap/plugin/messenger/build/js/messenger.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/inc/bootstrap/plugin/messenger/build/js/messenger-theme-future.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/inc/bootstrap/plugin/select/dist/js/bootstrap-select.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/inc/bootstrap/plugin/select/dist/js/i18n/defaults-cs_CZ.min.js"><?php echo '</script'; ?>
><?php }
}
?>