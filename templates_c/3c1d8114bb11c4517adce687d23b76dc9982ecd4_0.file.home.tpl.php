<?php /* Smarty version 3.1.25, created on 2017-07-14 14:44:10
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad/view/home.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:14911338415968bc9a45bbf2_52854872%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3c1d8114bb11c4517adce687d23b76dc9982ecd4' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad/view/home.tpl',
      1 => 1458910414,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14911338415968bc9a45bbf2_52854872',
  'variables' => 
  array (
    'head' => 0,
    'content' => 0,
    'lang' => 0,
    'topProjects' => 0,
    'key' => 0,
    'colors' => 0,
    'topProject' => 0,
    'topUsers' => 0,
    'topUser' => 0,
    'statistics' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_5968bc9a493208_36893489',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5968bc9a493208_36893489')) {
function content_5968bc9a493208_36893489 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '14911338415968bc9a45bbf2_52854872';
$_smarty_tpl->tpl_vars["colors"] = new Smarty_Variable(array("FFD700","C0C0C0","CD7F32"), null, 0);?>

<section>
    <header>
        <h2 class="padding-left-13"><?php echo $_smarty_tpl->tpl_vars['head']->value;?>
</h2>
    </header>
    <div class="col-md-16">
        <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

    </div>
    <div class="col-md-8">
        <span class="h3"><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['top5project'];?>
</span>        

        <table class="table table-striped table-bordered margin-top-10">
            <tr><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['project'];?>
</th></tr>
                    <?php
$_from = $_smarty_tpl->tpl_vars['topProjects']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['topProject'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['topProject']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['topProject']->value) {
$_smarty_tpl->tpl_vars['topProject']->_loop = true;
$foreach_topProject_Sav = $_smarty_tpl->tpl_vars['topProject'];
?>
                <tr <?php if ($_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->tpl_vars['key']->value]) {?>style="background-color:#<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->tpl_vars['key']->value];?>
;"<?php }?>>
                    <td ><?php echo $_smarty_tpl->tpl_vars['topProject']->value['project']->NAME;?>
</td>
                </tr>
            <?php
$_smarty_tpl->tpl_vars['topProject'] = $foreach_topProject_Sav;
}
?>
        </table>
        <span class="h3"><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['top5user'];?>
</span>
        <table class="table table-striped table-bordered margin-top-10">
            <tr><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['user'];?>
</th><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['points'];?>
</th></tr>
                    <?php
$_from = $_smarty_tpl->tpl_vars['topUsers']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['topUser'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['topUser']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['topUser']->value) {
$_smarty_tpl->tpl_vars['topUser']->_loop = true;
$foreach_topUser_Sav = $_smarty_tpl->tpl_vars['topUser'];
?>
                <tr <?php if ($_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->tpl_vars['key']->value]) {?>style="background-color:#<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->tpl_vars['key']->value];?>
;"<?php }?>>
                    <td ><?php echo $_smarty_tpl->tpl_vars['topUser']->value->NICK;?>
</td><td><?php echo $_smarty_tpl->tpl_vars['topUser']->value->POINT;?>
</td>
                </tr>
            <?php
$_smarty_tpl->tpl_vars['topUser'] = $foreach_topUser_Sav;
}
?>
        </table>
        <span class="h3"><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['stats'];?>
</span>
        <table class="table table-striped table-bordered margin-top-10">
            <tr><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['s1'];?>
</th><td><?php echo $_smarty_tpl->tpl_vars['statistics']->value['projectCount'];?>
</td></tr>
            <tr><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['s2'];?>
</th><td><?php echo $_smarty_tpl->tpl_vars['statistics']->value['textCount'];?>
</td></tr>
            <tr><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['s3'];?>
</th><td><?php echo $_smarty_tpl->tpl_vars['statistics']->value['translateCount'];?>
</td></tr>
            <tr><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['s4'];?>
</th><td><?php echo $_smarty_tpl->tpl_vars['statistics']->value['userCount'];?>
</td></tr>
            <tr><th><?php echo $_smarty_tpl->tpl_vars['lang']->value['home']['s5'];?>
</th><td><?php echo $_smarty_tpl->tpl_vars['statistics']->value['pointCount'];?>
</td></tr>
        </table>

    </div> 

</section><?php }
}
?>