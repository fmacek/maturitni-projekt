<?php /* Smarty version 3.1.25, created on 2016-10-18 22:42:24
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/inc/user-card.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:33945866580689300c9cd7_37915838%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '45b6bf015e60726e36bfe989dbc4d73f406b728c' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/inc/user-card.tpl',
      1 => 1455302052,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '33945866580689300c9cd7_37915838',
  'variables' => 
  array (
    'user' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_580689300fd854_79031077',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_580689300fd854_79031077')) {
function content_580689300fd854_79031077 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '33945866580689300c9cd7_37915838';
?>
<ul class="nav navbar-nav navbar-right margin-top-35">
    <li class="dropdown">
        <?php if (!$_smarty_tpl->tpl_vars['user']->value) {?>
            <div class="btn-group" role="group" aria-label="">
                <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#loginBox">Login</button>
                <a href="/user/register" class="btn btn-default btn-lg">Register</a>
            </div>
            <!-- Modal -->
            <div id="loginBox" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Login</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>
">
                                <div class="form-group">
                                    <div class="col-md-24">

                                        <div class="input-group">
                                            <label class="sr-only" for="email"><?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['email'];?>
</label>
                                            <div class="input-group-addon"><span class="fa fa-at fa-fw"></span></div>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['email'];?>
" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-24">

                                        <div class="input-group">
                                            <label class="sr-only" for="pass"><?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['pass'];?>
</label>
                                            <div class="input-group-addon"><span class="fa fa-key fa-fw"></span></div>
                                            <input type="password" class="form-control" id="pass" name="pass" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['pass'];?>
" required="required">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <button type="submit" class="btn btn-default center-block width100" name="login"><span class="fa fa-sign-in"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['login_form']['send'];?>
</button>
                                <button type="submit" class="btn btn-danger center-block width100" name="login"><span class="fa fa-google-plus"></span> G+ login</button>

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        <?php } else { ?>
            <form class="form-inline" method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>
">
                <div class="col-md-24 ">
                    <div class="btn-group btn-group-justified">
                        <a href="/project/my-projects" class="btn btn-default center-block width100"><span class="fa fa-file"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['my_projects'];?>
</a>
                        <a href="/user/profile" class="btn btn-default center-block width100"><span class="fa fa-user"></span> <?php echo $_smarty_tpl->tpl_vars['user']->value['nick'];?>
</a>
                        <a href="/user/vocabulary" class="btn btn-default center-block width100"><span class="fa fa-list"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['vocabulary'];?>
</a>
                    </div>
                </div>
                <div class="col-md-24">
                    <div class="btn-group btn-group-justified">
                        <a href="/user/profile"  class="btn btn-default center-block"><span class="fa fa-list-alt"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['show_profile'];?>
</a>
                        <a href="/user/logout"  class="btn btn-warning center-block"><span class="fa fa-sign-out"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['logout'];?>
</a>
                        <a href="/user/settings" class="btn btn-default center-block"><span class="fa fa-cogs"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['settings'];?>
</a>
                    </div>
                </div>
            </form>

            <!--
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class='fa fa-user'></span> <?php echo $_smarty_tpl->tpl_vars['user']->value['nick'];?>
 <span class='fa fa-caret-down'></span></a>
            <ul class="dropdown-menu">
                <a href="/project/my-projects" class="btn btn-default center-block"><span class="fa fa-file"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['my_projects'];?>
</a>
                <a href="/user/profile" class="btn btn-default center-block"><span class="fa fa-list-alt"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['show_profile'];?>
</a>
                <a href="/user/settings" class="btn btn-default center-block"><span class="fa fa-cogs"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['settings'];?>
</a>
                <a href="/user/vocabulary" class="btn btn-default center-block"><span class="fa fa-list"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['user_card']['vocabulary'];?>
</a>
                <form class="form-inline" method="POST" action="">
                    <button type="submit" class="btn btn-warning center-block width100" name="logout"><span class="fa fa-sign-out"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['account']['logout'];?>
</button>
                </form>
            </ul>
            -->
        <?php }?>
    </li>
</ul>
                
<?php }
}
?>