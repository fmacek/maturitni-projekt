<?php /* Smarty version 3.1.25, created on 2017-07-03 15:18:23
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/text.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:511501529595a441f40f9a6_93429818%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '75d5539d77a15c5eb36ed34255095461e0f0e134' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/text.tpl',
      1 => 1458914393,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '511501529595a441f40f9a6_93429818',
  'variables' => 
  array (
    'url' => 0,
    'project' => 0,
    'user' => 0,
    'lang' => 0,
    'allowedToProcess' => 0,
    'filter' => 0,
    'key' => 0,
    'name' => 0,
    'texts' => 0,
    'idCol' => 0,
    'origCol' => 0,
    'suggCol' => 0,
    'language' => 0,
    'text' => 0,
    'suggestion' => 0,
    'canAddText' => 0,
    'vocabulary' => 0,
    'page' => 0,
    'maxPage' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_595a442647fe83_38289140',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_595a442647fe83_38289140')) {
function content_595a442647fe83_38289140 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '511501529595a441f40f9a6_93429818';
?>

<?php $_smarty_tpl->tpl_vars['idCol'] = new Smarty_Variable(1, null, 0);?>
<?php $_smarty_tpl->tpl_vars['origCol'] = new Smarty_Variable(10, null, 0);?>
<?php $_smarty_tpl->tpl_vars['suggCol'] = new Smarty_Variable(13, null, 0);?>
<?php if (empty($_smarty_tpl->tpl_vars['url']->value[3])) {?>
    <?php $_smarty_tpl->createLocalArrayVariable('url', null, 0);
$_smarty_tpl->tpl_vars['url']->value['3'] = $_smarty_tpl->tpl_vars['project']->value['translateTo'][0]->LANG_ID;?>
<?php }?>
<?php if (empty($_smarty_tpl->tpl_vars['url']->value[4])) {?>
    <?php $_smarty_tpl->createLocalArrayVariable('url', null, 0);
$_smarty_tpl->tpl_vars['url']->value['4'] = 'non-approved';?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['user']->value) {?>
     
        <?php echo '<script'; ?>
 type="text/javascript">


            function disableVote(params) {
                console.log(JSON.stringify(params));
                var transId = params.idt;
                var type = params.type;
                var count = params.count;
                $(".like-button" + transId).attr("disabled", true);
                $(".dislike-button" + transId).attr("disabled", true);
                $(".like-button" + transId).removeClass("ajax");
                $(".dislike-button" + transId).removeClass("ajax");
                $("#" + type + transId).html(count + 1);
                console.log("disableVote");
            }

            function addText(params) {
                $("#form" + params.idt).remove();
                $("#new-added-text" + params.idt).html("<span class='fa fa-edit'></span> <a class='xeditable' id='changeOwnSuggestion' data-type='textarea' data-pk='" + newidt + "' data-url='/ajax/project-text.php' data-mode='inline' data-onblur='submit' data-title='Změna textu'>" + params.text + "</a>");
                $('.xeditable').editable();
            }

            $(document).ready(function () {

                $(".ajax").click(function () {//hlasování pro text
                    var ido = $(this).data('ido') ? $(this).data('ido') : "";
                    var idt = $(this).data('idt') ? $(this).data('idt') : "";
                    var ajaxData = {"ido": ido, "idt": idt, "switch": $(this).data('switch')};
                    var vars = $(this).data('vars') ? $(this).data('vars').split(" ") : 0;
                    var vals = $(this).data('vals') ? $(this).data('vals').split(" ") : 0;
                    var fcetrue = $(this).data('fcet') ? $(this).data('fcet').split(" ") : 0;
                    var fcefalse = $(this).data('fcef') ? $(this).data('fcef').split(" ") : 0;
                    var fceall = $(this).data('fce') ? $(this).data('fce').split(" ") : 0;
                    console.log("fcet " + $(this).data('fcet') + " - " + fcetrue);
                    for (var i = 0; i < vars.length; i++) {
                        ajaxData[vars[i]] = $(this).data(vars[i]);
                    }
                    for (var i = 0; i < vals.length; i++) {
                        ajaxData[vals[i]] = $(this).data(vals[i]);
                    }
                    console.log(JSON.stringify(ajaxData));
                    $.ajax({
                        type: "post",
                        url: "/ajax/project-text.php",
                        data: ajaxData,
                        success: function (html) {
                            if (html) {//pokud je v tom nějaký text
                                console.log("html");
                                Messenger().run;
                                if (html.match("^success:")) {//pokud to není úspěšný text                             console.log(html);
                                    Messenger().post({
                                        message: html.substr(8),
                                        type: 'success',
                                        showCloseButton: true
                                    });
                                    for (var i = 0; i < fcetrue.length; i++) {
                                        console.log(fcetrue[i]);
                                        window[fcetrue[i]](ajaxData);
                                    }
                                }
                                else if (html.match("^error:")) {
                                    console.log(html);
                                    Messenger().post({
                                        message: html.substr(6),
                                        type: 'error',
                                        showCloseButton: true
                                    });
                                    for (var i = 0; i < fcefalse.length; i++) {
                                        console.log(fcefalse[i]);
                                        window[fcefalse[i]](ajaxData);
                                    }

                                }
                                else {
                                    console.log("fail:" + html);
                                }
                            }

                            for (var i = 0; i < fceall.length; i++) {
                                console.log(fceall[i]);
                                window[fceall[i]](ajaxData);
                            }

                        }
                    });
                });
                /*
                 var transId = $(this).data('id'); 
                 var type = $(this).data('type'); 
                 var count = $(this).data('count') + 1;
                 var dataString = {"transId": transId, "type": type, "switch": "vote"}
                 $.ajax({
                 type: "post",
                 url: "/ajax/project-text.php",
                 data: dataString,
                 success: function (html) {
                 if (html) {//pokud je v tom nějaký text
                 Messenger().run;
                 if (!html.match("^success:")) {//pokud to není úspěšný text
                 Messenger().post({
                 message: html,
                 type: 'error',
                 showCloseButton: true
                 });
                 }
                 else {
                 Messenger().post({
                 message: html.substr(8),
                 type: 'success',
                 showCloseButton: true
                 });
                 $(".like-button" + transId).attr("disabled", true);
                 $(".dislike-button" + transId).attr("disabled", true);
                 $(".like-button" + transId).removeClass("vote");
                 $(".dislike-button" + transId).removeClass("vote");
                 $("#" + type + transId).html(count);
                 }
                 }
                 else {
                 $('#' + type + transId).html(count);
                 Messenger().run;
                 
                 Messenger().post({
                 message: html,
                 type: 'error',
                 showCloseButton: true
                 });
                 }
                 }
                 });
                 //*/
                /*
                 
                 $(".vote").click(function () {//hlasování pro text
                 var transId = $(this).data('id'); 
                 var type = $(this).data('type'); 
                 var count = $(this).data('count') + 1;
                 var dataString = {"transId": transId, "type": type, "switch": "vote"}
                 $.ajax({
                 type: "post",
                 url: "/ajax/project-text.php",
                 data: dataString,
                 success: function (html) {
                 if (html) {//pokud je v tom nějaký text
                 Messenger().run;
                 if (!html.match("^success:")) {//pokud to není úspěšný text
                 Messenger().post({
                 message: html,
                 type: 'error',
                 showCloseButton: true
                 });
                 }
                 else {
                 Messenger().post({
                 message: html.substr(8),
                 type: 'success',
                 showCloseButton: true
                 });
                 $(".like-button" + transId).attr("disabled", true);
                 $(".dislike-button" + transId).attr("disabled", true);
                 $(".like-button" + transId).removeClass("vote");
                 $(".dislike-button" + transId).removeClass("vote");
                 $("#" + type + transId).html(count);
                 }
                 }
                 else {
                 $('#' + type + transId).html(count);
                 Messenger().run;
                 
                 Messenger().post({
                 message: html,
                 type: 'error',
                 showCloseButton: true
                 });
                 }
                 }
                 });
                 });
                 
                 ////////////////////////////////////////////////////////////////////////////////
                 */
                $(".add-text").click(function () {//přidávání návrhu textu

                    var id = $(this).data('ido');
                    var newText = ".new-text-val" + id;
                    var langId = $(this).data('lang');
                    var vocabulary = $(newText).data('vocabulary');
                    var text = $(newText).val();
                    if (!text || (text == "NULL" && vocabulary == true)) {
                        Messenger().run;
                        Messenger().post({
                            message: "Musíš vyplnit textové pole.",
                            type: 'error',
                            showCloseButton: true});
                    }
                    else {

                        var dataString = {"ido": id, "text": text, "switch": "addText", "langId": langId, "vocabulary": vocabulary};
                        console.log(JSON.stringify(dataString));

                        $.ajax({
                            type: "post",
                            url: "/ajax/project-text.php",
                            data: dataString,
                            success: function (html) {
                                console.log(html);
                                if (html.match("^id:")) {//pokud to není úspěšný text
                                    if (vocabulary == true) {
                                        console.log("select text");
                                        var text = $(newText + " option:selected").text();
                                    }
                                    else {
                                        var text = $(newText).val();
                                        console.log("no change" + text);
                                    }
                                    $("#form" + id).remove();
                                    $("#new-added-text" + id).html("<span class='fa fa-edit'></span> <a class='xeditable' id='changeOwnSuggestion' data-type='textarea' data-pk='" + html.substr(3) + "' data-url='/ajax/project-text.php' data-mode='inline' data-onblur='submit' data-title='<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['change'];?>
'>" + text + "</a>");
                                    $('.xeditable').editable();
                                } else {
                                    Messenger().run;
                                    Messenger().post({
                                        message: html,
                                        type: 'error',
                                        showCloseButton: true
                                    });
                                }
                            }
                        });
                        // END AJAX */
                    }
                });
                /*
                 $(".add-debug").click(function () {//přidávání debugu
                 var id = $("#idTransDebug").val();
                 var description = $(".debug-description").val();
                 var text = $(".debug-new-translation").val();
                 var langId = $(this).data('lang-id');
                 
                 if (!text) {
                 Messenger().run;
                 Messenger().post({
                 message: "Musíš vyplnit textové pole.",
                 type: 'error',
                 showCloseButton: true
                 });
                 }
                 else {
                 var dataString = {"id": id, "text": text, "description": description, "lang": langId, "switch": "addDebug"}
                 $.ajax({
                 type: "post",
                 url: "/ajax/project-text.php",
                 data: dataString,
                 success: function (html) {
                 Messenger().run;
                 Messenger().post({
                 message: "Návrh k opravě byl poslán.",
                 type: 'success',
                 showCloseButton: true
                 });
                 }
                 });
                 }
                 
                 });
                 
                 $(".debugOpenButton").click(function () {//dodávání dat do debugBoxu
                 var tid = $(this).data('tid'); 
                 var text = $(".suggestion" + tid).text();
                 $("#debugOriginalText").html(text);
                 $("#idTransDebug").val(tid);
                 });
                 */

                $(".vocabulary")
                        .change(function () {
                            var val = $(this).val();
                            var ido = $(this).data('ido');
                            console.log(val);
                            console.log(ido);
                            if (val == "OWN") {
                                console.log("lol");
                                $(".vocabulary" + ido).addClass("hidden");
                                $(".new-text" + ido).removeClass("hidden");
                                $(".back-vocabulary" + ido).removeClass("hidden");
                                $(".new-text" + ido).addClass("new-text-val" + ido);
                                $(".vocabulary" + ido).removeClass("new-text-val" + ido);

                            }
                        })
                        .change();
                $(".back-vocabulary").click(function () {
                    var ido = $(this).data('ido');
                    $(".vocabulary" + ido).removeClass("hidden");
                    $(".new-text" + ido).addClass("hidden");
                    $(".back-vocabulary" + ido).addClass("hidden");
                    $(".vocabulary" + ido).val('NULL');
                    $(".new-text" + ido).removeClass("new-text-val" + ido);
                    $(".vocabulary" + ido).addClass("new-text-val" + ido);

                });
         

        <?php if ($_smarty_tpl->tpl_vars['user']->value['status'] == "admin" || in_array($_smarty_tpl->tpl_vars['user']->value['id'],$_smarty_tpl->tpl_vars['allowedToProcess']->value)) {?>
             

                $(".process-text").click(function () {//schvalování/zamítnutí textu
                    var idt = $(this).data('idt');
                    var ido = $(this).data('ido');
                    var type = $(this).data('type');
                    var newText = "#processed-text" + idt;
                    var text = $(newText).val();
                    if (!text) {
                        Messenger().run;
                        Messenger().post({
                            message: "<?php echo $_smarty_tpl->tpl_vars['project']->value['text']['must_fill'];?>
",
                            type: 'error',
                            showCloseButton: true
                        });
                    }
                    else {
                        var dataString = {"idt": idt, "ido": ido, "text": text, "switch": "processText", "type": type};
                        console.log(JSON.stringify(dataString));
                        $.ajax({
                            type: "post",
                            url: "/ajax/project-text.php",
                            data: dataString, success: function (html) {
                                if (html) {//pokud je v tom nějaký text
                                    console.log(html);
                                    Messenger().post({
                                        message: html,
                                        type: 'success',
                                        showCloseButton: true
                                    });
                                    $(".text-row-" + ido).remove();
                                }
                            }
                        });
                    }
                });
                //*/

             


        <?php }?>
//*/


            }); //document ready END

            /*
             $('input[size="100"]').each(function () {
             var style = $(this).attr('style'),
             textbox = "<textarea style='"+style+"'>"+text+"</textarea>"
             $(this).replaceWith(textbox);
             });
             //*/

    <?php echo '</script'; ?>
>

<?php }?>

<div id="debugBox" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nahlásit špatný překlad</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>
">
                    <input type="hidden" id="idTransDebug" name="tid" >
                    <input type='hidden' class="lang-id" value='<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
'>

                    Původní text:<br />
                    <span id="debugOriginalText"></span>
                    <textarea class="form-control debug-new-translation" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['new_trans'];?>
"></textarea>
                    <textarea class="form-control debug-description" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['new_trans_reason'];?>
"></textarea>
                    <br><br>
                    <button type="button" class="btn btn-default center-block width100 add-debug" data-lang-id="<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
"  ><span class="fa fa-send"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['form']['send'];?>
</button>

                </form>

            </div>
        </div>

    </div>
</div>



<div class="padding-left-20 padding-right-20">
    <div class="row">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-btn ">
                        <span class="h2 padding-right-20"><?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
</span> 
                      
                </div>
            </div>
        <?php ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['filter']['all'];
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['filter']['approved'];
$_tmp2=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['filter']['non_approved'];
$_tmp3=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['filter']['suggestion'];
$_tmp4=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['filter']['non_suggestion'];
$_tmp5=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['filter']['debug'];
$_tmp6=ob_get_clean();
$_smarty_tpl->tpl_vars['filter'] = new Smarty_Variable(array("all"=>$_tmp1,"approved"=>$_tmp2,"non-approved"=>$_tmp3,"suggestion"=>$_tmp4,"non-suggestion"=>$_tmp5,"debug"=>$_tmp6), null, 0);?>

        <div class="btn-group btn-group-justified width100" role="group" aria-label="">
            <?php
$_from = $_smarty_tpl->tpl_vars['filter']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['name']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
$foreach_name_Sav = $_smarty_tpl->tpl_vars['name'];
?>
                <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['url']->value[4]) {?>
                    <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
/1" class="btn btn-info btn-sm center-block"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</a>
                <?php } else { ?>
                    <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
/1" class="btn btn-default btn-sm center-block"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</a>
                <?php }?>
            <?php
$_smarty_tpl->tpl_vars['name'] = $foreach_name_Sav;
}
?>
        </div>
        <!--
            <div class="progress margin-bottom-0">
                <div class="progress-bar progress-bar-success bold" style="width: 75.454610283106%">
                    75%</div>
                <div class="progress-bar progress-bar-danger bold" style="width: 24.545389716894%">
                    25%</div>
            </div>
        <!-- -->
    </div>

    <?php if ($_smarty_tpl->tpl_vars['texts']->value) {?>
        <div class="row lichy vcenter" >
            <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['idCol']->value;?>
 bold"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['id'];?>
</div>
            <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['origCol']->value-1;?>
 bold"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['original_text'];?>
</div>
            <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['suggCol']->value;?>
 bold">
                <select name="selectlang" id="change-lang-project-translate" class="form-control">
                    <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                        <?php if ($_smarty_tpl->tpl_vars['language']->value->LANG_ID == $_smarty_tpl->tpl_vars['url']->value[3]) {?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->LANG_ID;?>
" selected><?php echo $_smarty_tpl->tpl_vars['language']->value->LANG;?>
</option>
                        <?php } else { ?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->LANG_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->LANG;?>
</option>
                        <?php }?>
                    <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
                </select>
            </div>
        </div>
        <?php
$_from = $_smarty_tpl->tpl_vars['texts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['text'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['text']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['text']->value) {
$_smarty_tpl->tpl_vars['text']->_loop = true;
$foreach_text_Sav = $_smarty_tpl->tpl_vars['text'];
?>
            <?php $_smarty_tpl->tpl_vars['canAddText'] = new Smarty_Variable("TRUE", null, 0);?>
            <?php if (!$_smarty_tpl->tpl_vars['user']->value) {?>
                <div class="row lichy vcenter margin-bottom-and-up-10px" >
                    <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['idCol']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['text']->value['id_web'];?>
</div>
                    <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['origCol']->value;?>
">
                        <small>
                            <i style="word-break: break-all;"><?php echo $_smarty_tpl->tpl_vars['text']->value['description'];?>
</i>
                        </small><br />
                        <b><?php echo $_smarty_tpl->tpl_vars['text']->value['text'];?>
</b>
                    </div>
                    <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['suggCol']->value;?>
">
                        <?php
$_from = $_smarty_tpl->tpl_vars['text']->value['suggestions'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['suggestion'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['suggestion']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['suggestion']->value) {
$_smarty_tpl->tpl_vars['suggestion']->_loop = true;
$foreach_suggestion_Sav = $_smarty_tpl->tpl_vars['suggestion'];
?>
                            <div class="col-md-15">
                                <?php echo $_smarty_tpl->tpl_vars['suggestion']->value['text'];?>

                            </div>
                            <div class="col-md-9 margin-bottom-2 margin-top-2"> 
                                <div class="btn-group btn-group-justified width100" role="group" aria-label="">
                                    <span class="btn btn-success btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['you_voted'];?>
">
                                        <i class="fa fa-thumbs-up fa-flip-horizontal"></i> <?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['like']);?>
</span>
                                    <span class="btn btn-danger btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['you_voted'];?>
">
                                        <i class="fa fa-thumbs-up fa-rotate-180"></i> <?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['dislike']);?>

                                    </span>
                                </div>
                            </div>
                        <?php
$_smarty_tpl->tpl_vars['suggestion'] = $foreach_suggestion_Sav;
}
?>
                    </div>

                </div>
            <?php } else { ?>
                <div class="row lichy vcenter margin-bottom-10 margin-top-10 text-row-<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" >
                    <div class="col-md-1"><?php echo $_smarty_tpl->tpl_vars['text']->value['id_web'];?>
</div>
                    <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['origCol']->value;?>
">
                        <b><?php echo $_smarty_tpl->tpl_vars['text']->value['text'];?>
</b><br />
                        <small>
                            <i style="word-break: break-all;"><?php echo $_smarty_tpl->tpl_vars['text']->value['description'];?>
</i>
                        </small>
                    </div>
                    <div class="col-md-<?php echo $_smarty_tpl->tpl_vars['suggCol']->value;?>
">
                        <?php
$_from = $_smarty_tpl->tpl_vars['text']->value['suggestions'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['suggestion'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['suggestion']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['suggestion']->value) {
$_smarty_tpl->tpl_vars['suggestion']->_loop = true;
$foreach_suggestion_Sav = $_smarty_tpl->tpl_vars['suggestion'];
?>
                            <?php if ($_smarty_tpl->tpl_vars['suggestion']->value['author'] == $_smarty_tpl->tpl_vars['user']->value['nick']) {?>
                                <?php $_smarty_tpl->tpl_vars['canAddText'] = new Smarty_Variable("FALSE", null, 0);?>
                            <?php }?>

                            <form method="POST" action="">
                                <div class="row">
                                    <input type="hidden" name="id_o" value="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
">
                                    <input type="hidden" name="id_t" value="<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
">
                                    <div class="col-md-16"> 
 
                                        <?php if (($_smarty_tpl->tpl_vars['user']->value['status'] == "admin" || in_array($_smarty_tpl->tpl_vars['user']->value['id'],$_smarty_tpl->tpl_vars['allowedToProcess']->value))) {?>
                                            <div class="input-group <?php if ($_smarty_tpl->tpl_vars['suggestion']->value['status'] == "debug") {?> has-warning<?php }?>">
                                                <span class="input-group-addon btn btn-success process-text" data-type="aprove" data-idt="<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
" data-ido="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-lang-id="<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
"   data-toggle="tooltip" data-placement="left" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['aprove_trans'];?>
"><i class="fa fa-check"></i></span>
                                                <textarea class="form-control suggestion<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
 " name="trans" id="processed-text<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['suggestion']->value['text'];?>
</textarea>
                                                <span class="input-group-addon btn btn-danger process-text" data-type="delete" data-idt="<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
" data-ido="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-lang-id="<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
"  data-toggle="tooltip" data-placement="right" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['delete_trans'];?>
"><i class="fa fa-times"></i></span>
                                            </div>
                                            <?php echo $_smarty_tpl->tpl_vars['suggestion']->value['author'];?>
 
                                        <?php } elseif ($_smarty_tpl->tpl_vars['suggestion']->value['author'] == $_smarty_tpl->tpl_vars['user']->value['nick'] && count($_smarty_tpl->tpl_vars['suggestion']->value['like']) == 0 && count($_smarty_tpl->tpl_vars['suggestion']->value['dislike']) == 0) {?>

                                            <span class='fa fa-edit'></span> 
                                            <a class='xeditable' id='changeOwnSuggestion' data-type='textarea' data-pk='<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
' data-url='/ajax/project-text.php' data-mode='inline' data-onblur='submit' data-title='Změna textu'><?php echo $_smarty_tpl->tpl_vars['suggestion']->value['text'];?>
</a>
                                        <?php } else { ?>
                                            <p class="suggestion<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['suggestion']->value['text'];?>
</p>

                                        <?php }?>
                                        <p class="description<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
 italic h6"><span class='margin-left-20'><?php echo $_smarty_tpl->tpl_vars['suggestion']->value['description'];?>
</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <?php if (!in_array($_smarty_tpl->tpl_vars['user']->value['nick'],$_smarty_tpl->tpl_vars['suggestion']->value['like']) && !in_array($_smarty_tpl->tpl_vars['user']->value['nick'],$_smarty_tpl->tpl_vars['suggestion']->value['dislike']) && $_smarty_tpl->tpl_vars['suggestion']->value['author'] !== $_smarty_tpl->tpl_vars['user']->value['nick']) {?> 
                                                <div class="col-md-24 margin-bottom-2 margin-top-2"> 
                                                    <div class="btn-group btn-group-justified width100" role="group" aria-label="">
                                                        <div class="ajax btn btn-success btn-sm center-block like-button<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
" data-ido="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-idt="<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
" data-vars="type count" data-fcet="disableVote"  data-type="like" data-switch="vote" data-count="<?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['like']);?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['like'];?>
">
                                                            <i class="fa fa-thumbs-up fa-flip-horizontal"></i> <span id="like<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
"><?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['like']);?>
</span>
                                                        </div>
                                                        <div class="ajax btn btn-danger btn-sm center-block dislike-button<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
" data-ido="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-idt="<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
" data-vars="type count"  data-fcet="disableVote" data-type="dislike" data-switch="vote" data-count="<?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['dislike']);?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['dislike'];?>
">
                                                            <i class="fa fa-thumbs-up fa-rotate-180"></i> <span id="dislike<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
"><?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['dislike']);?>
</span>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } else { ?>
                                            <?php $_smarty_tpl->tpl_vars['canAddText'] = new Smarty_Variable("FALSE", null, 0);?>

                                            <div class="col-md-24 margin-bottom-2 margin-top-2"> 

                                                <div class="btn-group btn-group-justified width100" role="group" aria-label="">
                                                    <?php if ($_smarty_tpl->tpl_vars['suggestion']->value['status'] == "approved") {?>                                    
                                                        <span data-tid="<?php echo $_smarty_tpl->tpl_vars['suggestion']->value['id'];?>
" type="button" class="debugOpenButton btn btn-warning btn-sm" data-toggle="modal" data-target="#debugBox" data-toggle="tooltip" data-placement="top" title="Nahlásit špatný překlad"><i class="fa fa-warning"></i></span>
                                                        <?php }?>
                                                    <span class="btn btn-success btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['you_voted'];?>
">
                                                        <i class="fa fa-thumbs-up fa-flip-horizontal"></i> <?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['like']);?>

                                                    </span>
                                                    <span class="btn btn-danger btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['you_voted'];?>
">
                                                        <i class="fa fa-thumbs-up fa-rotate-180"></i> <?php echo count($_smarty_tpl->tpl_vars['suggestion']->value['dislike']);?>

                                                    </span>
                                                </div>
                                            </div>
                                        <?php }?>
                                    </div>
                                </div>
                            </form>
                            <?php
$_smarty_tpl->tpl_vars['suggestion'] = $foreach_suggestion_Sav;
}
if (!$_smarty_tpl->tpl_vars['suggestion']->_loop) {
?>
                                <?php $_smarty_tpl->tpl_vars['canAddText'] = new Smarty_Variable("TRUE", null, 0);?>
                                <?php
}
?>
                                    <?php if ($_smarty_tpl->tpl_vars['canAddText']->value == "TRUE") {?>
                                        <div id="new-added-text<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
"></div>
                                        <div class="row" id="form<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
">
                                            <form method="POST" action="">
                                                <div class="input-group">
                                                    <?php if ($_smarty_tpl->tpl_vars['text']->value['vocabulary']) {?>
                                                        <select class="form-control vocabulary vocabulary<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
 new-text-val<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-ido="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-vocabulary="true">
                                                            <option value="NULL" ><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['vocabulary']['select'];?>
</option>
                                                            <?php
$_from = $_smarty_tpl->tpl_vars['text']->value['vocabulary'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['vocabulary'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['vocabulary']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['vocabulary']->value) {
$_smarty_tpl->tpl_vars['vocabulary']->_loop = true;
$foreach_vocabulary_Sav = $_smarty_tpl->tpl_vars['vocabulary'];
?>
                                                                <option value="<?php echo $_smarty_tpl->tpl_vars['vocabulary']->value->ID;?>
" ><?php echo $_smarty_tpl->tpl_vars['vocabulary']->value->TEXT;?>
</option>
                                                            <?php
$_smarty_tpl->tpl_vars['vocabulary'] = $foreach_vocabulary_Sav;
}
?>
                                                            <option value="OWN" ><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['vocabulary']['own'];?>
</option>

                                                        </select>
                                                        <span class="input-group-addon btn btn-info back-vocabulary back-vocabulary<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
 hidden" data-ido="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-toggle="tooltip" data-placement="left" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['vocabulary']['back'];?>
"><i class="fa fa-backward"></i></span>
                                                        <input placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['add_new'];?>
" class="hidden form-control custom-control new-text<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" required="required" data-vocabulary="false">
                                                    <?php } else { ?>
                                                        <input placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['add_new'];?>
" class="form-control custom-control new-text-val<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" required="required" data-vocabulary="false">
                                                    <?php }?>
                                                    <span class="input-group-addon btn btn-info add-text" data-switch="addText" data-ido="<?php echo $_smarty_tpl->tpl_vars['text']->value['id'];?>
" data-lang="<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
"  data-toggle="tooltip" data-placement="right" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['form']['send'];?>
"><i class="fa fa-send"></i></span>

                                                </div>
                                            </form>
                                                    
                                        </div>
                                    <?php }?>
                                </div>
                                <?php if (count($_smarty_tpl->tpl_vars['text']->value['suggestions']) > 1) {?>
                                    <div class="col-md-1">
                                        
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-1"></div>
                                <?php }?>
                            </div>
                            <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['text'] = $foreach_text_Sav;
}
?>


                                    <center>
                                        <div class="btn-group" role="group" aria-label="">
                                            <?php if ($_smarty_tpl->tpl_vars['page']->value != 1) {?>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/1" class="btn btn-default"><span class="fa fa-angle-double-left"></span</a>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" class="btn btn-default"><span class="fa fa-angle-left"></span></a>
                                                <?php }?>
                                                <?php if ($_smarty_tpl->tpl_vars['page']->value-2 >= 1) {?>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['page']->value-2;?>
" class="btn btn-default"><?php echo $_smarty_tpl->tpl_vars['page']->value-2;?>
</a>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['page']->value-1 >= 1) {?>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
" class="btn btn-default"><?php echo $_smarty_tpl->tpl_vars['page']->value-1;?>
</a>
                                            <?php }?>
                                            <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['page']->value;?>
" class="btn btn-info" disabled><?php echo $_smarty_tpl->tpl_vars['page']->value;?>
</a>
                                            <?php if ($_smarty_tpl->tpl_vars['page']->value+1 <= $_smarty_tpl->tpl_vars['maxPage']->value) {?>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" class="btn btn-default"><?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
</a>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['page']->value+2 <= $_smarty_tpl->tpl_vars['maxPage']->value) {?>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['page']->value+2;?>
" class="btn btn-default"><?php echo $_smarty_tpl->tpl_vars['page']->value+2;?>
</a>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['page']->value != $_smarty_tpl->tpl_vars['maxPage']->value) {?>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['page']->value+1;?>
" class="btn btn-default"><span class="fa fa-angle-right"></span></a>
                                                <a href="/<?php echo $_smarty_tpl->tpl_vars['url']->value[0];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[1];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[3];?>
/<?php echo $_smarty_tpl->tpl_vars['url']->value[4];?>
/<?php echo $_smarty_tpl->tpl_vars['maxPage']->value;?>
" class="btn btn-default"><span class="fa fa-angle-double-right"></span></a>
                                                <?php }?>
                                        </div>
                                    </center>
                                    <?php } else { ?>
                                        <p class="h2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['text']['no_text'];?>
</p>
                                        <?php }?>
                                        </div><?php }
}
?>