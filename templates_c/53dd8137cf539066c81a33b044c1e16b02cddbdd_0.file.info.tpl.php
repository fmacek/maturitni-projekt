<?php /* Smarty version 3.1.25, created on 2016-10-18 22:52:56
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/info.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:10483743158068ba8b13069_93998631%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53dd8137cf539066c81a33b044c1e16b02cddbdd' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/info.tpl',
      1 => 1459033572,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10483743158068ba8b13069_93998631',
  'variables' => 
  array (
    'project' => 0,
    'lang' => 0,
    'profile' => 0,
    'manager' => 0,
    'user' => 0,
    'language' => 0,
    'xyz' => 0,
    'id' => 0,
    'file' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_58068ba8be01a0_02108465',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_58068ba8be01a0_02108465')) {
function content_58068ba8be01a0_02108465 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '10483743158068ba8b13069_93998631';
?>
<div class="col-md-8">
    <h2><?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
</h2> 
    <?php if (!empty($_smarty_tpl->tpl_vars['project']->value['image'])) {?>
        <img src="/img/project/<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['project']->value['image'];?>
" class="img-responsive img-thumbnail">
    <?php } else { ?> 
        <img src="/img/project/default.jpg" class="img-responsive img-thumbnail">
    <?php }?>
    <div class="col-md-24">
        <h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['owner'];?>
</h3>
        <?php if ($_smarty_tpl->tpl_vars['profile']->value->PHOTO) {?>
            <img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/<?php echo $_smarty_tpl->tpl_vars['project']->value['owner']->PHOTO;?>
' alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['user_photo']['owner'];?>
" class="img-responsive img-thumbnail center-block"><br />
        <?php } else { ?>
            <img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/img/user-photo/user-default.png' alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['user_photo']['owner'];?>
" class="img-responsive img-thumbnail center-block"><br />
        <?php }?>
        <p class="text-center h4">
            <?php echo $_smarty_tpl->tpl_vars['project']->value['owner']->NICK;?>

        </p>
    </div>
    <div class="col-md-24">
        <h3>Správci</h3>
        <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['managers'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['manager'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['manager']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['manager']->value) {
$_smarty_tpl->tpl_vars['manager']->_loop = true;
$foreach_manager_Sav = $_smarty_tpl->tpl_vars['manager'];
?>
            <?php if ($_smarty_tpl->tpl_vars['manager']->value->NICK !== $_smarty_tpl->tpl_vars['project']->value['owner']->NICK) {?>
                <div class="col-md-12">
                    <span class="center-block">
                        <?php echo $_smarty_tpl->tpl_vars['manager']->value->NICK;?>

                    </span>
                    <img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/<?php echo $_smarty_tpl->tpl_vars['manager']->value->PHOTO;?>
' alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['user_photo']['manager'];?>
" class="img-responsive img-thumbnail center-block"><br />
                </div>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['manager'] = $foreach_manager_Sav;
}
?>
    </div>
</div>
<div class="col-md-16">
    <h3 class="h2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['lang_prog'];?>
</h3>
    <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
        <?php if ($_smarty_tpl->tpl_vars['user']->value && 1 == 2) {?>
            <div class='col-md-2'>


                <form method="POST">
                    <input type="hidden" name="project_id" value="<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
" />
                    <?php if ($_smarty_tpl->tpl_vars['language']->value['favorite']) {?>
                        <button type="submit" name="favorite_project" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['favorite_project']['remove'];?>
"><span class="fa fa-star"></span></button>
                        <?php } else { ?>
                        <button type="submit" name="favorite_project" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['favorite_project']['add'];?>
"><span class="fa fa-star-o"></span></button>
                        <?php }?>
                </form>


            </div>
        <?php }?>
        <div class='col-md-2'>
            <?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>

        </div>
        <div class='col-md-22'>
            <div class="progress">
                <div class="progress-bar progress-bar-success progress-bar-striped" style="width: <?php echo $_smarty_tpl->tpl_vars['language']->value['done'];?>
%">
                    <strong><?php echo $_smarty_tpl->tpl_vars['language']->value['done'];?>
% </strong>
                </div>
                <div class="progress-bar progress-bar-danger progress-bar-striped" style="width: <?php echo $_smarty_tpl->tpl_vars['language']->value['not_done'];?>
%">
                    <strong><?php echo $_smarty_tpl->tpl_vars['language']->value['not_done'];?>
%</strong>
                </div>
            </div>
        </div>
    <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
    <p class="h2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['about'];?>
</p>
    <?php echo $_smarty_tpl->tpl_vars['project']->value['description'];?>

    <p class="h2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['downloadable'];?>
</p>
    <select id="showFilesByLang" class="form-control">
        <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
</option>
        <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
    </select>
    <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['xyz'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['xyz']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['xyz']->value) {
$_smarty_tpl->tpl_vars['xyz']->_loop = true;
$foreach_xyz_Sav = $_smarty_tpl->tpl_vars['xyz'];
?>
        <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_Variable($_smarty_tpl->tpl_vars['xyz']->value['id'], null, 0);?>
        <div class="files files-<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
 hidden">
            <p class="h3"><?php echo $_smarty_tpl->tpl_vars['xyz']->value['name'];?>
</p>
            <?php if ($_smarty_tpl->tpl_vars['project']->value['files'][$_smarty_tpl->tpl_vars['id']->value]) {?>
                <table class="table table-striped table-responsive">
                    <tr>
                        <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['type'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['version'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['des'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['date'];?>
</th>
                        <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['download'];?>
</th>
                    </tr>
                <?php }?>
                <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['files'][$_smarty_tpl->tpl_vars['id']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['file']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->_loop = true;
$foreach_file_Sav = $_smarty_tpl->tpl_vars['file'];
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['file']->value->TYPE;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['file']->value->VERSION;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['file']->value->DESCRIPTION;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['file']->value->TIME;?>
</td>
                        <td><button class="btn btn-sm btn-info"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['download'];?>
</button></td>
                    </tr>
                    <?php echo $_smarty_tpl->tpl_vars['file']->value->FILEPATH;?>

                    <br>
                <?php
$_smarty_tpl->tpl_vars['file'] = $foreach_file_Sav;
}
if (!$_smarty_tpl->tpl_vars['file']->_loop) {
?>
                    <p><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['no_file'];?>
</p>
                <?php
}
?>
                <?php if ($_smarty_tpl->tpl_vars['project']->value['files'][$_smarty_tpl->tpl_vars['id']->value]) {?>
                </table>
            <?php }?>
        </div>
    <?php
$_smarty_tpl->tpl_vars['xyz'] = $foreach_xyz_Sav;
}
?>

</div>


<?php echo '<script'; ?>
>
    
        $(document).ready(function () {
            $("#showFilesByLang").click(function () {//schvalování/zamítnutí textu
                var id = $(this).val();
                $("#showFilesByLang option").each(function ()
                {
                    $(".files-" + $(this).val()).addClass("hidden");
                });
                $(".files-" + id).removeClass("hidden");
            }
            );
            $(".files").first().removeClass("hidden");
        });
    

<?php echo '</script'; ?>
><?php }
}
?>