<?php /* Smarty version 3.1.25, created on 2016-10-18 22:53:03
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/statistic/user.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3464879858068baf610765_90919276%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '263939167e66dea2fb05dd129a2f57165ba3bc0b' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/statistic/user.tpl',
      1 => 1459040529,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3464879858068baf610765_90919276',
  'variables' => 
  array (
    'lang' => 0,
    'langs' => 0,
    '_POST' => 0,
    'language' => 0,
    'selected' => 0,
    'users' => 0,
    'position' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_58068baf66aac1_87234847',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_58068baf66aac1_87234847')) {
function content_58068baf66aac1_87234847 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3464879858068baf610765_90919276';
?>
<form method="POST" action="" >

    <div class="col-md-18">
        <select name="lang[]" class="selectpicker show-menu-arrow" multiple data-live-search="true" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['source_lang'];?>
" data-width="100%">
            <?php
$_from = $_smarty_tpl->tpl_vars['langs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable('', null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['_POST']->value["lang"]) {?>
                    <?php if (in_array($_smarty_tpl->tpl_vars['language']->value->ID,$_smarty_tpl->tpl_vars['_POST']->value["lang"])) {?>
                        <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable("selected='selected'", null, 0);?>
                    <?php }?>
                <?php } else { ?>
                    <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable("selected='selected'", null, 0);?>
                <?php }?>

                <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
" <?php echo $_smarty_tpl->tpl_vars['selected']->value;?>
 ><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>
            <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
        </select>
    </div>
    <div class="col-md-4">
        <button type="submit" name="filter" class="btn btn-primary width100"><span class="fa fa-filter"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['button_filter'];?>
</button>
    </div>
</form>
<div class="col-md-24">
    <div class="row visible-lg visible-md">
        <div class="col-md-1 center-text">
            #
        </div>
        <div class="col-md-3 center-text">
            Uživatel
        </div>
        <div class="col-md-5 center-text">
            Body
        </div>
        <div class="col-md-5 center-text">
            Počet návrhů
        </div>
        <div class="col-md-5 center-text">
            Počet schválených
        </div>
        <div class="col-md-5 center-text">
            Počet hlasování
        </div>
    </div>
    <hr>
    <?php $_smarty_tpl->tpl_vars["position"] = new Smarty_Variable(1, null, 0);?>

    <?php
$_from = $_smarty_tpl->tpl_vars['users']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['user']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
$foreach_user_Sav = $_smarty_tpl->tpl_vars['user'];
?>
        <div class="row lichy padding-top-10">
            <div class="col-md-1 center-text visible-lg visible-md">
                <?php echo $_smarty_tpl->tpl_vars['position']->value;?>

            </div>
            <div class="col-md-3 center-text">
                <div class="col-md-24">
                    <span class="visible-sm visible-xs"><a href="/user/profile/<?php echo $_smarty_tpl->tpl_vars['user']->value["user"]->NICK;?>
" class="h3">#<?php echo $_smarty_tpl->tpl_vars['position']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value["user"]->NICK;?>
</a></span> 
                    <span class="visible-md visible-lg"><a href="/user/profile/<?php echo $_smarty_tpl->tpl_vars['user']->value["user"]->NICK;?>
" class="h3"> <?php echo $_smarty_tpl->tpl_vars['user']->value["user"]->NICK;?>
</a></span> 

                </div>
                <div class="col-md-24">

                    <a href="/user/profile/<?php echo $_smarty_tpl->tpl_vars['user']->value["user"]->NICK;?>
" class="h3"> 
                        <?php if (!empty($_smarty_tpl->tpl_vars['user']->value["user"]->PHOTO)) {?>
                            <img src="/<?php echo $_smarty_tpl->tpl_vars['user']->value["user"]->PHOTO;?>
" class="img-responsive img-thumbnail" alt="">
                        <?php } else { ?> 
                            <img src="/img/user-photo/user-default.png" class="img-responsive img-thumbnail" alt="">
                        <?php }?>
                    </a>
                </div>
            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Body:</span> <?php echo $_smarty_tpl->tpl_vars['user']->value["point"];?>

            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Počet návrhů:</span> <?php echo $_smarty_tpl->tpl_vars['user']->value["sug"];?>

            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Počet schválených</span> <?php echo $_smarty_tpl->tpl_vars['user']->value["app"];?>

            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Počet hlasování:</span> <?php echo $_smarty_tpl->tpl_vars['user']->value["vote"];?>

            </div>
        </div>
        <?php $_smarty_tpl->tpl_vars["position"] = new Smarty_Variable($_smarty_tpl->tpl_vars['position']->value+1, null, 0);?>


    <?php
$_smarty_tpl->tpl_vars['user'] = $foreach_user_Sav;
}
if (!$_smarty_tpl->tpl_vars['user']->_loop) {
?>
        <p>Nenalezen žádný uživatel.</p>
    <?php
}
?>
</div><?php }
}
?>