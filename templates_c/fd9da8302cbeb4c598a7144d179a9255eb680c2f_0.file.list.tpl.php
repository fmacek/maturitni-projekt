<?php /* Smarty version 3.1.25, created on 2016-10-18 22:42:39
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/list.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:10845767385806893f9ddc97_26788964%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fd9da8302cbeb4c598a7144d179a9255eb680c2f' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/list.tpl',
      1 => 1466878967,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10845767385806893f9ddc97_26788964',
  'variables' => 
  array (
    'lang' => 0,
    'langs' => 0,
    '_POST' => 0,
    'language' => 0,
    'selected' => 0,
    'projects' => 0,
    'project' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_5806893fab2ae9_29379356',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5806893fab2ae9_29379356')) {
function content_5806893fab2ae9_29379356 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '10845767385806893f9ddc97_26788964';
?>
<div class="visible-xs visible-sm">
    <center><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['filter'];?>
</center>
</div>
<form method="POST" action="" >
    <div class="col-md-8">
        <select name="fromlang[]" class="selectpicker show-menu-arrow" multiple data-live-search="true" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['source_lang'];?>
" data-width="100%">
            <?php
$_from = $_smarty_tpl->tpl_vars['langs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable('', null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['_POST']->value["fromlang"]) {?>
                    <?php if (in_array($_smarty_tpl->tpl_vars['language']->value->ID,$_smarty_tpl->tpl_vars['_POST']->value["fromlang"])) {?>
                        <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable("selected='selected'", null, 0);?>
                    <?php }?>
                <?php }?>

                <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
" <?php echo $_smarty_tpl->tpl_vars['selected']->value;?>
><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>
            <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
        </select>
    </div>
    <div class="col-md-8">
        <select name="tolang[]" class="selectpicker show-menu-arrow" multiple data-live-search="true" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['to_lang'];?>
" data-width="100%">
            <?php
$_from = $_smarty_tpl->tpl_vars['langs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable('', null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['_POST']->value["tolang"]) {?>
                    <?php if (in_array($_smarty_tpl->tpl_vars['language']->value->ID,$_smarty_tpl->tpl_vars['_POST']->value["tolang"])) {?>
                        <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable("selected='selected'", null, 0);?>
                    <?php }?>
                <?php }?>

                <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
" <?php echo $_smarty_tpl->tpl_vars['selected']->value;?>
><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>

            <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
        </select>
    </div>
    <div class="col-md-8">
        <button type="submit" name="filter_project" class="btn btn-primary width100"><span class="fa fa-filter"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['button_filter'];?>
</button>
    </div>
</form>

<div class="col-md-24">

    <div class="page-header">
        <h2 class="padding-top-10 margin-top-10"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['head'];?>
</h2>
        
    </div>


    <?php
$_from = $_smarty_tpl->tpl_vars['projects']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['project'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['project']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['project']->value) {
$_smarty_tpl->tpl_vars['project']->_loop = true;
$foreach_project_Sav = $_smarty_tpl->tpl_vars['project'];
?>

            


            <div class="row">
            
                <div class="col-md-3 col-xs-12">
                    <a href="/project/info/<?php echo $_smarty_tpl->tpl_vars['project']->value['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['img'];?>
">
                        <?php if (!empty($_smarty_tpl->tpl_vars['project']->value['image'])) {?>
                            <img src="/img/project/<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['project']->value['image'];?>
" class="img-responsive img-thumbnail" alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['image'];
echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
">
                        <?php } else { ?> 
                            <img src="/img/project/default.jpg" class="img-responsive img-thumbnail" alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['image'];?>
 <?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
">
                        <?php }?>
                    </a>
                </div>
                
                <div class="col-md-21 col-xs-12">
                    <div class="row">
                         <h3 style='margin: 0 0 10px 0'>   
                            <?php if ($_smarty_tpl->tpl_vars['project']->value['user_id'] == $_smarty_tpl->tpl_vars['user']->value['id']) {?>
                                    <a href="/project/settings/<?php echo $_smarty_tpl->tpl_vars['project']->value['url'];?>
" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['button']['settings'];?>
"><span class="fa fa-gear"></span></a>
                                    <?php }?>
                                <a href="/project/info/<?php echo $_smarty_tpl->tpl_vars['project']->value['url'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['img'];?>
">
                                    <?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['project']->value['language'];?>
)
                                </a>
                          </h3>
                    </div>
                    <div class="row">
                          <div class="col-md-7 col-xs-12  text-right hidden-md hidden-lg">
                              <a href = "/project/info/<?php echo $_smarty_tpl->tpl_vars['project']->value['url'];?>
" class="btn btn-default width100"><span class="fa fa-info-circle"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['show_more'];?>
 <span class="fa fa-angle-double-right"></span></a>
                              <a href = "/project/text/<?php echo $_smarty_tpl->tpl_vars['project']->value['url'];?>
" class="btn btn-primary width100"><span class="fa fa-align-justify"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['show_text'];?>
 <span class="fa fa-angle-double-right"></span></a>
                          </div>
                          
                          <div class="col-md-16 ">
                              <div class="col-md-24 col-xs-24 justify"><span><?php echo $_smarty_tpl->tpl_vars['project']->value['description'];?>
</span></div>
          
                              <div class="col-md-12 col-xs-24 padding-top-10" >
                                  <span><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['translate_to'];?>
: <?php echo implode(' | ',$_smarty_tpl->tpl_vars['project']->value['translateTo']);?>
</span>
                              </div>
                          </div>
                         
                          <div class="col-md-6  text-right hidden-xs hidden-sm">
                              <div class="btn-group">
                                  <a href = "/project/info/<?php echo $_smarty_tpl->tpl_vars['project']->value['url'];?>
" class="btn btn-default width100"><span class="fa fa-info-circle"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['show_more'];?>
 <span class="fa fa-angle-double-right"></span></a>
                                  <a href = "/project/text/<?php echo $_smarty_tpl->tpl_vars['project']->value['url'];?>
" class="btn btn-primary width100"><span class="fa fa-align-justify"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['show_text'];?>
 <span class="fa fa-angle-double-right"></span></a>
                              </div>
                          </div>
                    </div>
                    
                    
                 </div>
            </div>
            <hr />

            <?php
$_smarty_tpl->tpl_vars['project'] = $foreach_project_Sav;
}
if (!$_smarty_tpl->tpl_vars['project']->_loop) {
?>
                <p><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['list']['no_project'];?>
</p>
                <?php
}
?>
                   
                </div><?php }
}
?>