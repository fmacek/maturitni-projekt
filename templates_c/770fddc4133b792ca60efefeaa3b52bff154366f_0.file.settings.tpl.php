<?php /* Smarty version 3.1.25, created on 2017-05-03 18:00:09
         compiled from "/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/settings.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:7087288625909fe891e15f3_84195294%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '770fddc4133b792ca60efefeaa3b52bff154366f' => 
    array (
      0 => '/data/web/virtuals/111611/virtual/www/subdom/preklad2/view/project/settings.tpl',
      1 => 1459033070,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7087288625909fe891e15f3_84195294',
  'variables' => 
  array (
    'project' => 0,
    'lang' => 0,
    'user' => 0,
    'profile' => 0,
    'language' => 0,
    'langs' => 0,
    'search' => 0,
    'can' => 0,
    'manager' => 0,
    'xyz' => 0,
    'id' => 0,
    'file' => 0,
    'url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.25',
  'unifunc' => 'content_5909fe894b30f5_40509138',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5909fe894b30f5_40509138')) {
function content_5909fe894b30f5_40509138 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '7087288625909fe891e15f3_84195294';
?>
 

    <?php echo '<script'; ?>
 type="text/javascript">
        $(document).ready(function () {
            $(".visible-lang").hover(function () {
                if ($(this).hasClass("btn-success")) {
                    $(this).addClass("btn-danger");
                    $(this).removeClass("btn-success");
                }
                else {
                    $(this).addClass("btn-success");
                    $(this).removeClass("btn-danger");
                }
            }, function () {
                if ($(this).hasClass("btn-success")) {
                    $(this).addClass("btn-danger");
                    $(this).removeClass("btn-success");
                }
                else {
                    $(this).addClass("btn-success");
                    $(this).removeClass("btn-danger");
                }
            });
            $(".user-settings").click(function () {
                var name = $(this).data('name');
                var pk = $(this).data('pk');
                if (name == "add-manager") {
                    var value = $("#nick").val();
                    var langId = $("#lang-manager").val();
                    var dataString = {"name": name, "pk": pk, "value": value, "langId": langId}
                }
                else {
                    var value = $(this).data('value');
                    var dataString = {"name": name, "pk": pk, "value": value}
                }
                $.ajax({
                    type: "post",
                    url: "/ajax/project-settings.php",
                    data: dataString,
                    success: function (html) {
                        if (!html.match("^success:")) {//pokud to není úspěšný text
                            if (html) {
                                Messenger().run
                                Messenger().post({
                                    message: html,
                                    type: 'error',
                                    showCloseButton: true
                                });
                            }
                        }
                        else {
                            if (name == "add-manager") {
                                var data = html.substr(8);
                                $('.project-manager').last().html("<img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/" + data + "' class='img-responsive img-thumbnail'><br>" + value + "<div class='project-manager'></div>");
                            }
                            else {
                                $("#manager" + value).remove();
                            }
                        }
                    }
                });
                //*/
            });
        });
    <?php echo '</script'; ?>
>
 
<div class='col-md-24'>
    <h2>
        <span class="fa fa-edit"></span> <a class="xeditable" id="project-name" data-type="text" data-pk="<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
" data-url="/ajax/project-settings.php" data-mode="inline" data-onblur="submit" data-title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['name'];?>
"><?php echo $_smarty_tpl->tpl_vars['project']->value['name'];?>
</a>
    </h2> 
    <div class="row">
        <div class="col-md-8">
            <?php if (!empty($_smarty_tpl->tpl_vars['project']->value['image'])) {?>
                <img src="/img/project/<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['project']->value['image'];?>
" class="img-responsive img-thumbnail">
                <button class="btn btn-xs btn-danger user-settings" id="delete-photo" data-pk="<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
" data-name="delete-photo">
                    <span class="fa fa-times"></span>
                </button> 
            <?php } else { ?> 
                <img src="/img/project/default.jpg" class="img-responsive img-thumbnail">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="file"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['upload_new_photo'];?>
</label>
                        <input type="file" id="file">
                        <p class="help-block"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['project_photo'];?>
</p>
                    </div>
                    <button type="submit" class="btn btn-xs btn-info" name="change_profile_info"><span class="fa fa-send"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['form']['send'];?>
</button>
                </form>
            <?php }?>
            <h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['owner'];?>
</h3>
            <span class="center-text">
                <?php echo $_smarty_tpl->tpl_vars['project']->value['owner']->NICK;?>

            </span>
            <?php if ($_smarty_tpl->tpl_vars['user']->value['photo']) {?>
                <img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/<?php echo $_smarty_tpl->tpl_vars['profile']->value->PHOTO;?>
' alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['your_photo'];?>
" class="img-responsive img-thumbnail center-block">
            <?php } else { ?>
                <img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/img/user-photo/user-default.png' alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['your_photo'];?>
" class="img-responsive img-thumbnail center-block">
            <?php }?>
            <center><?php echo $_smarty_tpl->tpl_vars['user']->value['nick'];?>
</center>
        </div>
        <div class="col-md-16">
            <div class="col-md-24">
                <h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['lang_progress'];?>
</h3>
                <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>

                    <div class='row'>
                        <div class='col-md-2'>
                            <?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>

                        </div>
                        <div class='col-md-19'>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped" style="width: <?php echo $_smarty_tpl->tpl_vars['language']->value['done'];?>
%">
                                    <strong><?php echo $_smarty_tpl->tpl_vars['language']->value['done'];?>
% </strong>
                                </div>
                                <div class="progress-bar progress-bar-danger progress-bar-striped" style="width: <?php echo 100-$_smarty_tpl->tpl_vars['language']->value['done'];?>
%">
                                    <strong><?php echo 100-$_smarty_tpl->tpl_vars['language']->value['done'];?>
%</strong>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <form method="POST">
                                <div class="btn-group" role="group" aria-label="">
                                    <button type="submit" class="btn btn-success btn-xs visible-lang"><span class="fa fa-eye"></span></button>
                                    <button type="submit" class="btn btn-warning btn-xs"><span class="fa fa-times"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
                <div class='row'>

                    <div class='col-md-24'>
                        <form method="POST" action="">
                            <div class="input-group"> 
                                <div class="input-group-btn width100">
                                    <div class="btn-group width100"> 
                                        <select name="lang" class="form-control width100">
                                            <option value=""><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['new_lang'];?>
</option>

                                            <?php
$_from = $_smarty_tpl->tpl_vars['langs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                                                <?php $_smarty_tpl->tpl_vars["can"] = new Smarty_Variable("1", null, 0);?>
                                                <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['search'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['search']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['search']->value) {
$_smarty_tpl->tpl_vars['search']->_loop = true;
$foreach_search_Sav = $_smarty_tpl->tpl_vars['search'];
?>
                                                    <?php if ($_smarty_tpl->tpl_vars['search']->value['id'] == $_smarty_tpl->tpl_vars['language']->value->ID || $_smarty_tpl->tpl_vars['search']->value['id'] == $_smarty_tpl->tpl_vars['project']->value['language']) {?>
                                                        <?php $_smarty_tpl->tpl_vars["can"] = new Smarty_Variable("0", null, 0);?>
                                                    <?php }?>
                                                <?php
$_smarty_tpl->tpl_vars['search'] = $foreach_search_Sav;
}
?>
                                                <?php if ($_smarty_tpl->tpl_vars['language']->value->ID !== $_smarty_tpl->tpl_vars['project']->value['language']) {?>
                                                    <?php if ($_smarty_tpl->tpl_vars['can']->value == 1) {?> 
                                                        <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->ID;?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value->NAME;?>
</option>
                                                    <?php }?>
                                                <?php }?>
                                            <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
                                        </select>
                                    </div>
                                </div>  
                                <span class="input-group-btn">
                                    <button type="submit" name="add-lang" class="btn btn-info" >
                                        <span class="fa fa-send"></span>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-24">
                <h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['managers'];?>
</h3>
                <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['managers'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['manager'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['manager']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['manager']->value) {
$_smarty_tpl->tpl_vars['manager']->_loop = true;
$foreach_manager_Sav = $_smarty_tpl->tpl_vars['manager'];
?>
                    <?php if ($_smarty_tpl->tpl_vars['manager']->value->NICK !== $_smarty_tpl->tpl_vars['user']->value['photo']) {?>
                        <div class="col-md-12" id="manager<?php echo $_smarty_tpl->tpl_vars['manager']->value->ID;?>
">
                            <?php if ($_smarty_tpl->tpl_vars['manager']->value->PHOTO) {?>
                                <img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/<?php echo $_smarty_tpl->tpl_vars['manager']->value->PHOTO;?>
' alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo_user'];?>
" class="img-responsive img-thumbnail center-block">
                            <?php } else { ?>
                                <img src='<?php echo $_smarty_tpl->getConfigVariable('urlroot');?>
/img/user-photo/user-default.png' alt="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['photo_user'];?>
" class="img-responsive img-thumbnail center-block">
                            <?php }?>

                            <center>
                                <button class="btn btn-xs btn-danger user-settings" id="delete-user" data-pk="<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
" data-name="delete-manager" data-value="<?php echo $_smarty_tpl->tpl_vars['manager']->value->ID;?>
">
                                    <span class="fa fa-times"></span>
                                </button> 
                                <?php echo $_smarty_tpl->tpl_vars['manager']->value->NICK;?>
 (<?php echo $_smarty_tpl->tpl_vars['manager']->value->LANG;?>
)
                            </center>
                        </div>
                    <?php }?>
                <?php
$_smarty_tpl->tpl_vars['manager'] = $foreach_manager_Sav;
}
?>
                <div class="project-manager"></div>
                <div class='col-md-24'> 
                    <div class="form-group">
                        <label class="sr-only" for="nick"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['add_new_manager'];?>
</label>
                        <div class="input-group"> 
                            <div class="input-group-btn">
                                <div class="btn-group">

                                    <select id="lang-manager" class="form-control" name="type" style="width: 70px;">
                                        <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value["id"];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value["name"];?>
</option>
                                        <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
                                    </select>
                                </div>
                            </div>  
                            <input class="form-control" type="text" id="nick" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['user']['nick'];?>
">
                            <span class="input-group-btn">
                                <button id="add-user" class="btn btn-info user-settings" data-pk="<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
" data-name="add-manager">
                                    <span class="fa fa-send"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-24 justify">
            <br /><br />
            <h3><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['description'];?>
</h3>
            <span class="fa fa-edit"></span> <a class="xeditable" id="project-description" data-type="textarea" data-pk="<?php echo $_smarty_tpl->tpl_vars['project']->value['id'];?>
" data-url="/ajax/project-settings.php" data-mode="inline" data-onblur="submit" data-title="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['settings']['project_description'];?>
"><?php echo $_smarty_tpl->tpl_vars['project']->value['description'];?>
</a>
            <p class="h2"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['trans'];?>
</p>
            <select id="showFilesByLang" class="form-control">

                <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value["id"];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value["name"];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
            </select>
            <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['xyz'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['xyz']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['xyz']->value) {
$_smarty_tpl->tpl_vars['xyz']->_loop = true;
$foreach_xyz_Sav = $_smarty_tpl->tpl_vars['xyz'];
?>
                <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_Variable($_smarty_tpl->tpl_vars['xyz']->value["id"], null, 0);?>
                <div class="files files-<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
 hidden">
    
                    <p class="h3"><?php echo $_smarty_tpl->tpl_vars['xyz']->value["name"];?>
</p>
                    <?php if ($_smarty_tpl->tpl_vars['project']->value['files'][$_smarty_tpl->tpl_vars['id']->value]) {?>
                        <table class="table table-striped table-responsive">
                            <tr>
                                <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['type'];?>
</th>
                                <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['version'];?>
</th>
                                <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['des'];?>
</th>
                                <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['date'];?>
</th>
                                <th><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['download'];?>
</th>
                            </tr>
                        <?php }?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['files'][$_smarty_tpl->tpl_vars['id']->value];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['file'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['file']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->_loop = true;
$foreach_file_Sav = $_smarty_tpl->tpl_vars['file'];
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['file']->value->TYPE;?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['file']->value->VERSION;?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['file']->value->DESCRIPTION;?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['file']->value->TIME;?>
</td>
                                <td><button class="btn btn-sm btn-info"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['download'];?>
</button></td>
                            </tr>
                            <?php echo $_smarty_tpl->tpl_vars['file']->value->FILEPATH;?>

                            <br>
                        <?php
$_smarty_tpl->tpl_vars['file'] = $foreach_file_Sav;
}
if (!$_smarty_tpl->tpl_vars['file']->_loop) {
?>
                            <p><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['no_file'];?>
</p>
                        <?php
}
?>
                        <?php if ($_smarty_tpl->tpl_vars['project']->value['files'][$_smarty_tpl->tpl_vars['id']->value]) {?>
                        </table>
                    <?php }?>
                    
                </div>
            <?php
$_smarty_tpl->tpl_vars['xyz'] = $foreach_xyz_Sav;
}
?>
            <a class="btn btn-info" href="/project/export-preview/<?php echo $_smarty_tpl->tpl_vars['url']->value[2];?>
/<?php echo $_smarty_tpl->tpl_vars['xyz']->value->LANG_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['preview'];?>
</a>
            <span class="btn btn-info " data-toggle="modal" data-target="#debugBox"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['create'];?>
</span>

        </div>
    </div>
<?php echo '<script'; ?>
>
    
        $(document).ready(function () {
            $("#showFilesByLang").click(function () {//schvalování/zamítnutí textu
                var id = $(this).val();
                $("#showFilesByLang option").each(function ()
                {
                    $(".files-" + $(this).val()).addClass("hidden");
                });
                $(".files-" + id).removeClass("hidden");
            }
            );
            $(".files").first().removeClass("hidden");
        });
    

<?php echo '</script'; ?>
>

    <div id="debugBox" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['new_export'];?>
</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>
">
                        <input type="hidden" class="form-control" name="type" value="<?php if ($_smarty_tpl->tpl_vars['language']->value['done'] == 100) {?>finished<?php } else { ?>beta<?php }?>" />

                        <select name="lang" class="form-control">
                            <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value->LANG_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['export_lang'];?>
</option>
                            <?php
$_from = $_smarty_tpl->tpl_vars['project']->value['translateTo'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['language'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['language']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
$foreach_language_Sav = $_smarty_tpl->tpl_vars['language'];
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['language']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['language']->value['done'];?>
%)</option>
                            <?php
$_smarty_tpl->tpl_vars['language'] = $foreach_language_Sav;
}
?>
                        </select>
                        <input type="text" class="form-control" name="version" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['export_version'];?>
" />
                        <textarea class="form-control" name="description" placeholder="<?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['export_des'];?>
"></textarea>
                        <?php echo $_smarty_tpl->tpl_vars['lang']->value['project']['info']['export_public'];?>
<input type="checkbox" class="checkbox-inline" name="status" />
                        <br><br>
                        <button type="submit" class="btn btn-default center-block width100" name="export"><span class="fa fa-send"></span> <?php echo $_smarty_tpl->tpl_vars['lang']->value['form']['send'];?>
</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div><?php }
}
?>