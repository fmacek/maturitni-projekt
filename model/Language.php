<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Language
 *
 * @author fmacek
 */
class Language {

    public static function get($id = "ALL") {
        if ($id == "ALL") {
            return Database::getTable("lang", "*", "STATUS = :status", ["status" => "visible"]);
        } else {
            return Database::getRow("lang", "*", "ID = :lig AND STATUS = :status", ["lid" => $id, "status" => "visible"]);
        }
    }

}
