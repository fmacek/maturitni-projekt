<?php

// Pomocná třída, poskytující metody pro odeslání emailu
class Email {

    // Odešle email jako HTML, lze tedy používat základní HTML tagy a nové
    // řádky je třeba psát jako <br /> nebo používat odstavce. Kódování je
    // odladěno pro UTF-8.
    public function send($from, $title, $text, $to) {
        $header = "From: " . $from;
        $header .= "\nMIME-Version: 1.0\n";
        $header .= "Content-Type: text/html; charset=\"utf-8\"\n";
        return mb_send_mail($to, $title, $text, $header);
    }

}
