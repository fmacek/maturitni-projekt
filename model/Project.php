<?php

/**
 * Controller for project
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class Project {

    /**
     * Searching if this row with this params exist
     * @param string $filename
     *  name of file
     * @param int $projectId
     *  ID of project
     * @return NULL
     *  */
    public static function import($filename, $projectId) {
        $explode = explode(".", $filename);
        $type = $explode[1];
        switch ($type) {
            case "csv":
                Import::csv($filename, $projectId);
                break;
            case "po":
                Import::po($filename, $projectId);
                break;
            case "xml":
                Import::xml($filename, $projectId);
                break;

            default:
                break;
        }
    }

    public static function export($projectId, $lang, $type, $version, $description, $status) {
        $project = Database::getRow("project", "*", "ID = :pid", ["pid" => $projectId]);
        $filetype = explode(".", $project->FILENAME)[1];

        switch ($filetype) {
            case "csv":
                $export = Export::csv($project->FILENAME, $projectId, $lang);
                break;
            case "po":
                $export = Export::po($project->FILENAME, $projectId, $lang);
                break;
            case "xml":
                $export = Export::xml($project->FILENAME, $projectId, $lang);
                break;
            default:
                
                break;
        }
        if ($export) {
            Database::insert("files", ["PROJECT_ID" => $projectId, "LANG_ID" => $lang, "TYPE" => $type, "VERSION" => $version, "DESCRIPTION" => $description, "FILEPATH" => $export, "STATUS" => $status]);
            Alert::add("info", "Projekt byl vyexportován.");
        }
    }

    /**
     * Get all managers to project
     * @param int $projectId 
     *  ID of project
     * @param array $data [optional=["ID", "NICK", "EMAIL", "ABOUT_ME", "NAME", "PHOTO", "STATUS", "REGISTRED_TIME", "REGISTRED_IP"]]
     *  Data what we want to know
     * @return object $user
     *  */
    public static function getManagers($projectId, $data = array("ID", "NICK", "EMAIL", "ABOUT_ME", "NAME", "PHOTO", "STATUS", "REGISTRED_TIME", "REGISTRED_IP")) {
        $cols = "user." . implode(", user.", $data);
        return Database::getTable("manager", "lang.NAME AS `LANG`, $cols", "manager.PROJECT_ID = :project_id AND manager.STATUS = :status", array("project_id" => $projectId, "status" => "active"), "JOIN user ON manager.USER_ID = user.ID JOIN lang ON manager.LANG_ID = lang.ID");
    }

    /**
     * Add project to favorite to user
     * @param int $projectId 
     *  ID of project
     * @param array $userId [optional = logged user (SESSION)]
     *  To user
     * @return NULL
     *  */
    public static function changeFavoriteUser($projectId, $userId = "SESSION") {
        $uid = ($userId == "SESSION") ? $_SESSION["user"]["id"] : $userId;
        if (!Database::isExist("favorite_project", "USER_ID = :uid AND PROJECT_ID = :pid", ["uid" => $uid, "pid" => $projectId])) {
            //Database::insert("favorite_project", ["USER_ID" => $uid, "PROJECT_ID" => $projectId, "LANG_ID" => $landId]);
        } else {
            
        }
    }

    /**
     * Add manager  to project
     * @param int $userId 
     *  ID of user
     * @param int $projectId 
     *  ID of project
     * @param int $langId 
     *  ID of lang who is he mannaging
     * @param array $status [optional = active]
     *  status of manager
     * @return bool 
     *  */
    public static function addManager($userId, $projectId, $langId, $status = "active") {
        if (!Database::isExist("manager", "USER_ID = :uid AND PROJECT_ID = :pid AND STATUS = :status", array("uid" => $userId, "pid" => $projectId, "status" => "active"))) {
            Database::insert("manager", array("USER_ID" => $userId, "PROJECT_ID" => $projectId, "LANG_ID" => $langId, "STATUS" => $status));
            Database::insert("manager_history", array("USER_ID" => $userId, "TYPE" => "add", "TIME" => date("Y-m-d  H:i:s"), "PARAMETER" => $projectId));
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * deleting manager to project
     * @param int $userId 
     *  ID of user
     * @param int $projectId 
     *  ID of project
     * @param int $langId 
     *  ID of lang who is he mannaging
     * @return NULL
     *  */
    public static function deleteManager($userId, $projectId, $langId) {
        if (Database::isExist("manager", "USER_ID = :uid AND PROJECT_ID = :pid AND LANG_ID = :lid", array("uid" => $userId, "pid" => $projectId, "lid" => $lid))) {
            Database::update("manager", array("STATUS" => "deleted"), "PROJECT_ID = :pid AND USER_ID = :uid", array("uid" => $userId, "pid" => $projectId));
            Database::insert("manager_history", array("USER_ID" => $userId, "TYPE" => "delete", "TIME" => date("Y-m-d  H:i:s"), "PARAMETER" => $projectId));
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get list of languages to witch must be project translated
     * @param int $projectId 
     *  ID of project
     * @param bool $id [optional = TRUE]
     *  if we want ID's of langs
     * @return NULL
     *  */
    public static function getTranslateToLangs($projectId, $id = TRUE) {

        if ($id) {
            return Database::getTable("project_translate_to", "lang.NAME AS `LANG`, lang.ID AS `LANG_ID`", "PROJECT_ID = :project_id", array("project_id" => $projectId), "JOIN lang ON project_translate_to.LANG_ID = lang.ID");
        } else {
            return Database::getValues("project_translate_to", "lang.NAME AS `LANG`", "PROJECT_ID = :project_id", array("project_id" => $projectId), "JOIN lang ON project_translate_to.LANG_ID = lang.ID");
        }
    }

    /**
     * Get list of languages to witch must be project translated
     * @param mixed $id [optional = ALL]
     *  list of project(s). if is not set(value is "ALL") 
     * @return object $projects
     *  */
    public static function get($id = "ALL") {
        if ($id == "ALL") {
            return Database::getTable("project", "project.*, lang.NAME AS `LANG`", "project.STATUS = :status AND VISIBLE = 1", ["status" => "active"], "JOIN lang ON project.LANG_ID = lang.ID");
        } else {
            return Database::getRow("project", "*", "ID = :id", array("id" => $id));
        }
    }

    /**
     * Original text to translate
     * @param int $projectId
     *  ID of project
     * @param int $lang
     *  ID of lang
     * @param string $filter
     *  filtering
     * @param int $page
     *  offset
     * @param int $textPerPage
     *  limit
     * @return object $originals
     *  */
    public static function getOriginalText($projectId, $lang, $filter, $page, $textPerPage) {
        $start = ($page - 1) * $textPerPage;
        switch ($filter) {
            case "all":
                return Database::getTable("original_text", ["ID", "ID_WEB", "DESCRIPTION", "TEXT", "STATUS"], "PROJECT_ID = :project_id AND STATUS != :status LIMIT $start, $textPerPage ", array("project_id" => $projectId, "status" => "hidden"));
                break;
            case "approved":
                return Database::getTable("original_text", ["original_text.ID", "original_text.ID_WEB", "original_text.DESCRIPTION", "original_text.TEXT", "original_text.STATUS"], "original_text.PROJECT_ID = :project_id AND translated_text.STATUS = :status AND translated_text.LANG_ID = :lid LIMIT $start, $textPerPage ", array("project_id" => $projectId, "status" => "approved", "lid" => $lang), "JOIN translated_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID");
                break;
            case "non-approved":
                $data = [];
                $id = 0;
                $c = 1;
                $before = 0;
                for ($i = 0; $i < $textPerPage;) {
                    $row = Database::getRow("original_text", ["original_text.ID", "original_text.ID_WEB", "original_text.DESCRIPTION", "original_text.TEXT", "original_text.STATUS"], "original_text.PROJECT_ID = :pid AND original_text.ID > :before", array("pid" => $projectId, "before" => $before), "LEFT JOIN translated_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID");
                    if (!Database::isExist("translated_text", "ORIGINAL_TEXT_ID = :oid AND LANG_ID = :lid AND STATUS = 'approved'", ["oid" => $row->ID, "lid" => $lang])) {

                        if ($start <= 0) {
                            $data[] = $row;
                            $i ++;
                        } else {
                            $start --;
                        }
                    }
                    $c ++;
                    $before = $row->ID;
                }
                return $data;
                //return Database::getTable("original_text", ["original_text.ID", "original_text.ID_WEB", "original_text.DESCRIPTION", "original_text.TEXT", "original_text.STATUS"], "original_text.PROJECT_ID = :project_id AND translated_text.STATUS = :status AND translated_text.LANG_ID = :lid LIMIT $start, $textPerPage ", array("project_id" => $projectId, "status" => "active", "lid" => $lang), "JOIN translated_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID", TRUE);

                break;
            case "suggestion":
                return Database::getTable("original_text", ["original_text.ID", "original_text.ID_WEB", "original_text.DESCRIPTION", "original_text.TEXT", "original_text.STATUS"], "original_text.PROJECT_ID = :project_id AND original_text.STATUS = :status AND translated_text.STATUS NOT IN ('closed', 'closed_debug') AND translated_text.LANG_ID = :lang GROUP BY original_text.ID LIMIT $start, $textPerPage  ", array("project_id" => $projectId, "status" => "active", "lang" => $lang), "JOIN translated_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID ");
                break;
            case "non-suggestion":
                $count = $offset = 0;
                $dataid = $data = [];
                $ids = Database::getValues("original_text", "ID", "PROJECT_ID = :pid AND STATUS = :status", ["pid" => $projectId, "status" => "active"]);
                foreach ($ids as $id) {
                    if (!Database::isExist("translated_text", "ORIGINAL_TEXT_ID= :oid AND LANG_ID = :lid AND STATUS != :status", ["oid" => $id, "lid" => $lang, "status" => "closed"])) {
                        if ($offset >= $start) {
                            $dataid[] = (int) $id;
                        } else {
                            $offset ++;
                        }
                    }
                    if (count($dataid) == $textPerPage) {
                        break;
                    }
                }
                if ($dataid) {
                    return Database::getTable("original_text", ["original_text.ID", "original_text.ID_WEB", "original_text.DESCRIPTION", "original_text.TEXT", "original_text.STATUS"], "ID IN(" . implode(", ", $dataid) . ")");
                }
                break;
            case "debug":
                return Database::getTable("original_text", ["original_text.ID", "original_text.ID_WEB", "original_text.DESCRIPTION", "original_text.TEXT", "original_text.STATUS"], "original_text.PROJECT_ID = :project_id AND translated_text.STATUS = :status  AND translated_text.STATUS = :status AND translated_text.LANG_ID = :lang GROUP BY original_text.ID LIMIT $start, $textPerPage  ", array("project_id" => $projectId, "status" => "active", "status" => "debug", "lang" => $lang), "JOIN translated_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID ");
                break;
            default:
                return FALSE;
                break;
        }
    }

    public static function countOriginals($projectId, $filter = "all") {
        switch ($filter) {
            case "all":
                return Database::getValue("original_text", "COUNT", "PROJECT_ID = :project_id AND STATUS != :status", array("project_id" => $projectId, "status" => "hidden"));
                break;
            case "approved":
                return Database::getValue("original_text", "COUNT", "PROJECT_ID = :project_id AND STATUS = :status ", array("project_id" => $projectId, "status" => "translated"));
                break;
            case "non-approved":
                return Database::getValue("original_text", "COUNT", "PROJECT_ID = :project_id AND STATUS = :status ", array("project_id" => $projectId, "status" => "active"));
                break;
            case "suggestion":
                return Database::getValue("original_text", "COUNT", "original_text.PROJECT_ID = :project_id AND original_text.STATUS = :status AND translated_text.STATUS NOT IN ('closed', 'closed_debug') AND translated_text.LANG_ID = :lang GROUP BY original_text.ID ", array("project_id" => $projectId, "status" => "active", "lang" => $lang), "JOIN translated_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID ");
                break;
            case "non-suggestion":
                return 0; //Database::multiSelect("SELECT COUNT(ID) FROM `original_text` WHERE (SELECT COUNT(ID) FROM `translated_text` WHERE original_text.ID = translated_text.ORIGINAL_TEXT_ID AND translated_text.LANG_ID = :lang_id AND original_text.PROJECT_ID = :pid) = 0 ", ["lang_id" => $lang, "pid" => $projectId]);
                break;
            case "debug":
                return Database::getValue("original_text", "COUNT", "original_text.PROJECT_ID = :project_id AND translated_text.STATUS = :status  AND translated_text.STATUS = :status AND translated_text.LANG_ID = :lang GROUP BY original_text.ID ", array("project_id" => $projectId, "status" => "active", "status" => "debug", "lang" => $lang), "JOIN translated_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID ");
                break;
            default:
                return FALSE;
                break;
        }
    }

    public static function add($name, $description, $source_lang, $translateTo) {
        debug([$name, $description, $source_lang, $translateTo],"project::add");
        $success = TRUE;
        foreach ($translateTo as $key => $langId) {
            if ($langId == $source_lang) {
                Alert::add("warning", "Projekt nemůže být přeložen do stejného jazyku, v jakým je zdrojový soubor.");
                unset($translateTo[$key]);
            }
        }
        if (empty($name) or empty($description) or empty($source_lang) or empty($translateTo)) {
            Alert::add("warning", "Nejsou vyplněny všechny údaje.");
            $success = FALSE;
        }
        if (Database::isExist("project", "NAME = :name", array("name" => $name))) {
            Alert::add("warning", "Bohužel projekt s tímto jménem už máme.");
            $success = FALSE;
        }
        if ($success) {
            Database::insert("project", array("NAME" => $name, "USER_ID" => $_SESSION["user"]["id"], "DESCRIPTION" => $description, "LANG_ID" => $source_lang, "URL" => textToURI($name)));
            Alert::add("successs", "Projekt byl úspěšně přidán");
            $projectId = Database::getValue("project", "ID", "NAME = :NAME AND USER_ID = :USER_ID AND DESCRIPTION = :DESCRIPTION AND  LANG_ID = :LANG_ID", array("NAME" => $name, "USER_ID" => $_SESSION["user"]["id"], "DESCRIPTION" => $description, "LANG_ID" => $source_lang));
            foreach ($translateTo as $langId) {
                if ($langId !== $source_lang) {
                    Database::insert("project_translate_to", array("PROJECT_ID" => $projectId, "LANG_ID" => $langId));
                }
            }
            Database::insert("manager", array("USER_ID" => $_SESSION["user"]["id"], "PROJECT_ID" => $projectId, "STATUS" => "active"));
            Database::insert("manager_history", array("USER_ID" => $_SESSION["user"]["id"], "TYPE" => "add", "TIME" => date("Y-m-d H:i:s"), "PARAMETER" => $projectId));
            Alert::add("info", "Stal jste se správcem a majitelem tohoto projektu.");

            return $projectId;
        } else {
            return FALSE;
        }
    }

    public static function vote($transId, $type, $userId = "session") {
        if ($userId == "session") {
            $userId = $_SESSION["user"]["id"];
        }
        $success = TRUE;
        if (!in_array($type, array("like", "dislike"))) {
            Alert::add("danger", "Neznámý typ hlasu.");
            $success = FALSE;
        }
        if (!is_numeric($transId)) {
            Alert::add("danger", "Neznámý návrh.");
            $success = FALSE;
        }
        if (!is_numeric($userId)) {
            Alert::add("danger", "Neznámý hlasující.");
            $success = FALSE;
        }
        if ($success) {
            Database::insert("vote", array("TRANSLATED_TEXT_ID" => $transId, "TYPE" => $type, "USER_ID" => $userId));
        }
    }

    public static function addTranslation($originalId, $langId, $text, $description = "", $userId = "session", $status = "active") {
        if ($userId == "session") {
            $userId = $_SESSION["user"]["id"];
        }
        if (!Database::isExist("translated_text", "USER_ID = :uid AND ORIGINAL_TEXT_ID = :oriId AND LANG_ID = :lang", array("uid" => $userId, "oriId" => $originalId, "lang" => $langId))) {
            Database::insert("translated_text", array("ORIGINAL_TEXT_ID" => $originalId, "TEXT" => $text, "USER_ID" => $userId, "LANG_ID" => $langId, "STATUS" => $status));
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function addDebug($tid, $langId, $text, $description, $userId = "session") {
        if ($userId == "session") {
            $userId = $_SESSION["user"]["id"];
        }
        $oid = Database::getValue("translated_text", "ORIGINAL_TEXT_ID", "ID = :tid", ["tid" => $tid]);
        if (Database::isExist("original_text", "ID = :oid AND STATUS = :status", ["oid" => $oid, "status" => "translated"])) {
            //if (!Database::isExist("translated_text", "ORIGINAL_TEXT_ID = :oid AND USER_ID= :uid AND STATUS = 'debug'", ["oid" => $oid, "uid" => $userId])) {
            Database::insert("translated_text", array("ORIGINAL_TEXT_ID" => $oid, "TEXT" => $text, "USER_ID" => $userId, "LANG_ID" => $langId, "STATUS" => "debug", "DESCRIPTION" => $description));
            //}
        } else {
            return FALSE;
        }
    }

    public static function processTranslation($type, $translationId, $originalId, $text, $userId = "SESSION") {
        switch ($type) {
            case "aprove":
                $databaseText = Database::getValue("translated_text", "TEXT", "ID = :tid", ["tid" => $translationId]);
                Database::update("original_text", ["STATUS" => "translated"], "ID = :oid", ["oid" => $originalId]);
                if ($databaseText == $text) {
                    Database::update("translated_text", ["STATUS" => "approved"], "ID = :tid", ["tid" => $translationId]);
                } else {
                    Database::update("translated_text", ["STATUS" => "approved", "TEXT" => $text], "ID = :tid", ["tid" => $translationId]);
                }
                Alert::add("success", "Text byl úspěšně schválen.");
                return TRUE;
                break;
            case "delete":
                Database::update("translated_text", ["STATUS" => "closed"], "ID = :tid", ["tid" => $translationId]);
                Alert::add("success", "Text byl úspěšně zamítnut.");
                return TRUE;
                break;
            default :
                return FALSE;
                break;
        }
    }

    public static function changeOwnSuggestion($suggestionId, $text, $userId = "SESSION") {
        $uid = ($userId == "SESSION") ? $_SESSION["user"]["id"] : $userId;
        $_SESSION["test"]["uid3"] = $uid;
        $_SESSION["test"]["uidfgdf"] = "$userId";
        if (Database::isExist("translated_text", "USER_ID = :uid AND ID = :sid", ["uid" => $uid, "sid" => $suggestionId])) {
            Database::update("translated_text", ["TEXT" => $text], "ID = :sid", ["sid" => $suggestionId]);
            return $uid;
        } else {
            return FALSE;
        }
    }

    public static function allowedToProcess($project, $lang) {
        $manager = Database::getValues("manager", "USER_ID", "PROJECT_ID = :pid AND LANG_ID = :lid AND STATUS = 'active'", ["pid" => $project, "lid" => $lang]);
        $owner = Database::getValue("project", "USER_ID", "ID = :pid", ["pid" => $project]);
        if (!in_array($owner, $manager)) {
            $manager[] = $owner;
        }
        return $manager;
    }

}
