<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Export
 *
 * @author Filip Macek
 */
class Export {

    public static function csv($filename, $projectId, $langId) {
        $filename = "files/$projectId/import/$filename";
        $file = array_map('str_getcsv', file("$filename"));
        $lang = Database::getValue("lang", "NAME", "ID = :lid", ["lid" => $langId]);
        $name = Database::getValue("project", "URL", "ID = :pid", ["pid" => $projectId]);
        $newfilename = "$name-$lang-" . date("Ymd") . ".csv";
        $found = TRUE;
        $i = 1;
        do {
            if (!file_exists($newfilename)) {
                $found = FALSE;
            } else {
                $newfilename = "$name-$lang-" . date("Ymd") . "-$i.csv";
                $i++;
            }
        } while ($found);
        $newfile = fopen("files/$projectId/export/" . $newfilename, 'w');

        foreach ($file as $row) {
            $translation = "";
            $text = $row[1];
            $des = $row[0];
            $oid = Database::getValue("original_text", "ID", "TEXT = :text AND DESCRIPTION = :des AND PROJECT_ID = :pid", ["text" => $text, "des" => $des, "pid" => $projectId]);
            $translation = Database::getValue("translated_text", "TEXT", "ORIGINAL_TEXT_ID = :oid AND LANG_ID = :lid", ["oid" => $oid, "lid" => $langId]);
            fputcsv($newfile, [$row[0], $row[1], $translation]);
        }
        return $newfilename;
        // */
    }

    public static function po($filename, $projectId, $langId) {

        $lang = Database::getValue("lang", "NAME", "ID = :lid", ["lid" => $langId]);
        $name = Database::getValue("project", "URL", "ID = :pid", ["pid" => $projectId]);
        $newfilename = "$name-$lang-" . date("Ymd") . ".po";
        $found = TRUE;
        $i = 1;
        do {
            if (!file_exists($newfilename)) {
                $found = FALSE;
            } else {
                $newfilename = "$name-$lang-" . date("Ymd") . "-$i.po";
                $i++;
            }
        } while ($found);
        $newfile = fopen("files/$projectId/export/" . $newfilename, 'w');

        $myfile = fopen("files/$projectId/import/$filename", "r") or die("Unable to open file!");
// Output one line until end-of-file
        $values = [];
        while (!feof($myfile)) {
            $row = fgets($myfile);
            if ($row[0] == "#") {
                $des = $row;
                $values[] = $row;
            } elseif (strstr($row, "msgid \"")) {
                $prefix = 'msgid "';
                $str = $row;
                $str = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $str);
                $str = substr($str, 0, -2);
                $text = $row;
                $values[] = $text;
            } elseif (strstr($row, "msgstr \"")) {
                $translation = "";
                $oid = Database::getValue("original_text", "ID", "TEXT = :text AND DESCRIPTION = :des AND PROJECT_ID = :pid", ["text" => $str, "des" => $des, "pid" => $projectId]);

                $translation = Database::getValue("translated_text", "TEXT", "ORIGINAL_TEXT_ID = :oid AND LANG_ID = :lid", ["oid" => $oid, "lid" => $langId]);
                $values[] = "msgstr \"$translation\"\n";
            }
        }
        $i = 1;
        foreach ($values as $value) {
            fwrite($newfile, $value);
        }

        fclose($myfile);
        fclose($newfile);


        return $newfilename;
    }

    public static function xml($filename, $projectId, $langId) {

        $lang = Database::getValue("lang", "NAME", "ID = :lid", ["lid" => $langId]);
        $name = Database::getValue("project", "URL", "ID = :pid", ["pid" => $projectId]);
        $newfilename = "$name-$lang-" . date("Ymd") . ".xml";
        $found = TRUE;
        $i = 1;
        do {
            if (!file_exists($newfilename)) {
                $found = FALSE;
            } else {
                $newfilename = "$name-$lang-" . date("Ymd") . "-$i.xml";
                $i++;
            }
        } while ($found);
        $data = fopen("files/$projectId/import/$filename", "r") or die("Unable to open file!");

        $values = $rows = [];
        while (!feof($data)) {
            $row = fgets($data);

            if (strstr($row, "<text")) {
                debug($row, "row");
                $xml = simplexml_load_string($row);
                $des = $xml->attributes()->descripion;

                $oid = Database::getValue("original_text", "ID", "TEXT = :text AND DESCRIPTION = :des AND PROJECT_ID = :pid", ["text" => $xml, "des" => $des, "pid" => $projectId]);
                $translation = Database::getValue("translated_text", "TEXT", "ORIGINAL_TEXT_ID = :oid AND LANG_ID = :lid", ["oid" => $oid, "lid" => $langId]);
                $text = ($translation) ? $translation : $xml;
                $x = [];
                foreach ($xml->attributes() as $a => $b) {
                    $x[] = "$a=\"$b\"";
                }
                $atributes = implode(" ", $x);
                $offset = explode("<text", $xml);
                debug($offset, "offset");
                $rows[] = "$offset<text $atributes>$text</text>\n";
            } else {
                $rows[] = $row;
            }
        }
        $i = 1;
        debug($newfilename);
        $newfile = fopen("files/$projectId/export/" . $newfilename, 'w');

        foreach ($rows as $value) {
            debug($value, "zápis");

            fwrite($newfile, "$value");
        }


        debug($values);
        fclose($data);


        Alert::add("info", "Projekt byl naimportován.");
    }

}
