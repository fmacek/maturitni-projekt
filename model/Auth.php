<?php

class Auth {

    private static $user;

    public static function logIn($email, $password) {
        global $_SESSION;
        $salt = Database::getValue("user", "SALT", "EMAIL = :email", array("email" => $email));
        $login = Database::getRow("user", "user.*", "user.EMAIL = :email AND user.PASS = :pass AND user.`STATUS` IN ('user', 'admin', 'watchdog')", array("email" => $email, "pass" => generateHash("super-long", $password, $salt)));
        if (empty($login)) { //nenalezen
            Alert::add("warning", "Přihlášení proběhlo neúspěšně. Zkuste to prosím znovu.");
        } else {//nalezen
            $session_key = array("ID", "NICK", "EMAIL", "NAME", "BIRTH", "STATUS", "PHOTO", "REGION");
            foreach ($session_key as $key) { //zapsání údajů do SESSION
                $lower = strtolower($key);
                $_SESSION["user"][$lower] = $login->$key;
            }
            Alert::add("success", "Přihlášení proběhlo úspěšně. Vítejte <b>" . $login->NICK . "</b>!");
        }
    }

    public static function logOut() {
        unset($_SESSION["user"]);
        Alert::add("success", "Odhlášení proběho úspěšně.");
    }

    public static function register($data) {
        $email = $data["email"];
        $nick = $data["nick"];
        $pass = $data["pass"];
        $pass2 = $data["pass2"];

        $success = TRUE;
        if (empty($email) or empty($pass) or empty($pass2) or empty($nick)) {
            Alert::add("warning", "Musíš vyplnit všechny požadované údaje!");
            $success = FALSE;
        }

        if (!preg_match('/^[a-z0-9 .\-]+$/i', $nick) or preg_match('/\s/', $nick)) {
            Alert::add("warning", "Nick smí obsahovat pouze anglickou abecedu, čísla, pomlčku (-), tečku(.) a nesmí obsahovat mezery!");
            $success = FALSE;
        } else
            $nick = $data["nick"]; //preg_match z mě neznámého důvodu z toho dělá array, tak na to musím znova šáhnout
        if ($pass !== $pass2) {
            Alert::add("warning", "Hesla se musí shodovat!");
            $success = FALSE;
        }
        if (strlen($pass) < 6) {
            Alert::add("warning", "Heslo je příliš krátké!");
            $success = FALSE;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            Alert::add("warning", "Musíš zadat email ve správném formátu!");
            $success = FALSE;
        }
        if (Database::isExist("user", "EMAIL = :email", array("email" => $email))) { //zjištění zda již není email zabrán
            Alert::add("warning", "Tento email je již obsazený. Zadej prosím jiný.");
            $success = FALSE;
        }
        if (Database::isExist("user", "NICK = :nick", array("nick" => $nick))) {//zjištění zda již není nick zabrán
            Alert::add("warning", "Tento nick je již obsazený. Zadej prosím jiný.");
            $success = FALSE;
        }
        if ($success) {
            $salt = sha1(md5(time()));
            Database::insert("user", array("EMAIL" => $email, "NICK" => $nick, "PASS" => generateHash("super-long", $pass, $salt), "SALT" => $salt, "STATUS" => "registred", "REGISTRED_IP" => $_SERVER['REMOTE_ADDR'], "REGISTRED_TIME" => date("Y-m-d H:i:s"), "SPECIAL_CODE" => generateHash("long", $salt, $email)));
            Alert::add("success", "Registrace proběhla úspěšně! Nyní vyčkej na doručení aktivačního emailu s potvrzením.");
            sendMail($email, "DMP - registrace uživatele", "Dobrý den,<br \><br \>děkujeme za registraci na našem webu, zde máš <a href='http://mvc.filipmacek.cz/user/authorize/$email/" . generateHash("long", $salt, $email) . "'>aktivační odkaz</a>. Doufáme, že budeš na našem webu spokojen. ");
        }
    }

    public static function authorize($email, $hash) {
        $success = TRUE;
        if (empty($email) or empty($hash)) {
            Alert::add("warning", "Musíš zadat email i potvrzovací kód!");
            $success = FALSE;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            Alert::add("warning", "Musíš zadat správně email!");
            $success = FALSE;
        }
        if (strlen($hash) !== 72) {
            Alert::add("warning", "Musíš zadat hash ve správném formátu!");
            $success = FALSE;
        }
        if ($success) {
            
            if (Database::isExist("user", "EMAIL = :email AND SPECIAL_CODE = :hash", array("email" => $email, "hash" => $hash))) {//pokud byl takový výskledek nalezen (odpovídající mail a hash)
                Database::update("user", array("STATUS" => "user", "SPECIAL_CODE" => ""), "EMAIL = :email", array("email" => $email));
                Alert::add("success", "Tvůj účet byl úspěšně aktvivován, nyní se můžeš přihlásit a doplnit si celý profil!");
            } else {
                Alert::add("warning", "Bohužel, ale k tomuto emailu tento hash nemáme. Účet máš již aktivovaný nebo neexistuje.");
            }
        }
    }

    public static function isAdmin() {
        return (self::isLogged() && $_SESSION['role'] === "admin");
    }

    private static function hash($password, $salt) {
        $hash = sha1($salt . $password) . sha1(md5($password) . "123-789-456");
        return $hash;
    }

    public static function getLoggedUser() {
        self::forceAuthentication();
        if (is_null(self::$user)) {
            self::$user = User::find($_SESSION['id']);
        }
        return self::$user;
    }

    public static function forceAuthentication() {
        if (!self::isLogged()) {
            throw new UnauthorizedAccessException();
        }
    }

    public static function isLogged() {
        return array_key_exists('role', $_SESSION);
    }

}
