<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Inport
 *
 * @author Filip Macek
 */
class Import {

    //put your code here
    public static function csv($filename, $projectId) {
        $file = array_map('str_getcsv', file($filename));
        $values = array();
        $i = 1;
        $params = array("project_id" => $projectId, "status" => "active");
        foreach ($file as $row) {
            $values[] = "(:id_web$i ,:project_id, :des$i, :text$i, :status)";
            $params["id_web$i"] = $i;
            $params["des$i"] = $row[0];
            $params["text$i"] = $row[1];
            $i ++;
        }
        $value = implode(", ", $values);
        Database::query("INSERT INTO original_text (ID_WEB, PROJECT_ID, DESCRIPTION, TEXT, STATUS) VALUES $value", $params);
        Alert::add("info", "Projekt byl naimportován.");
    }

    //put your code here
    public static function po($filename, $projectId) {
        $myfile = fopen("$filename", "r") or die("Unable to open file!");
// Output one line until end-of-file
        $values = [];
        $description = $original = FALSE;
        $des = $text = "";

        while (!feof($myfile)) {
            $row = fgets($myfile);


            if ($row[0] == "#") {
                $description = TRUE;
                $des .= "$row\n";
            } elseif (strstr($row, "msgid \"") or ($row[0] == '"' and $original) ) {
                $original = TRUE;
                if (strstr($row, "msgid \"")) {
                    $prefix = 'msgid "';
                    $str = $row;
                    $str = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $str);
                    $text = substr($str, 0, -2);
                } else {
                    $text = substr($row, 1, -1) . "\n";
                }
            } elseif (strstr($row, "msgstr \"")) {
                $values[] = ["DESCRIPTION" => $des, "TEXT" => $text];
                $des = $text = "";
                $description = $original = FALSE;
            }
        }
        debug($values, "FINAL VALUES");
        $i = 1;
        foreach ($values as $value) {
            $data = array_merge($value, ["PROJECT_ID" => $projectId, "STATUS" => 'active', "ID_WEB" => $i]);
            debug($data);
            Database::insert("original_text", $data);
            $i++;
        }

        debug($values);
        fclose($myfile);


        Alert::add("info", "Projekt byl naimportován.");
    }

    public static function xml($filename, $projectId) {

        $data = fopen($filename, "r");
        $values = [];
        while (!feof($data)) {

            $row = fgets($data);

            if (strstr($row, "<text")) {
                //  debug($row, "row");
                $xml = simplexml_load_string($row);

                $descripion = $xml->attributes()->descripion;
                echo $xml;
                debug(["TEXT" => $xml, "DESCRIPTION" => $descripion], "ABC");
                $values[] = ["TEXT" => $xml, "DESCRIPTION" => $descripion];
            }
        }
        $i = 1;
        foreach ($values as $value) {
            $row = array_merge($value, ["PROJECT_ID" => $projectId, "STATUS" => 'active', "ID_WEB" => $i]);
            debug($row);
            Database::insert("original_text", $row);
            $i++;
        }

        debug($values);
        fclose($data);


        Alert::add("info", "Projekt byl naimportován.");
    }

}
