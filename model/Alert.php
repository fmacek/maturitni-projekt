<?php

/**
 * Objekt na správu hlášek webu ("Byl jste přihlášen", "Váš hlas byl uložen",..)
 *
 * @author Filip Macek
 */
class Alert {

    public static $types = array("primary", "success", "info", "warning", "danger");
    public static $alert = array();

    public static function add($type, $text) {
        if (in_array($type, self::$types)) {
            self::$alert[] = array("type" => $type, "text" => $text);
        }
    }

    public static function get() {
        return self::$alert;
    }

}
