<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pager
 *
 * @author fmacek
 */
class Pager {

    public $page;
    public $offset;
    public $max;
    private $toFirst = "<span class='fa fa-angle-double-left'></span>";
    private $toLast = "<span class='fa fa-angle-double-right'></span>";
    private $prev = "<span class='fa fa-angle-left'></span>";
    private $next = "<span class='fa fa-angle-right'></span>";
    private $data = [];

    public function __construct($page, $numberOfItemsPerPage, $countOfItems) {
        $page = ($page != 0) ? $page : 1;

        $this->page = (int) abs(round($page)); //page number XY
        $this->offset = (int) $numberOfItemsPerPage;
        $this->max = (int) ceil($countOfItems / $numberOfItemsPerPage);
    }

    public function generate() {
        if ($this->page > 1) {
            $this->data[] = $this->generateOne(1, $this->toFirst);
            $this->data[] = $this->generateOne($this->page - 1, $this->prev);
        }
        if ($this->page - 2 >= 1) {
            $this->data[] = $this->generateOne($this->page - 2);
        }
        if ($this->page - 1 >= 1) {
            $this->data[] = $this->generateOne($this->page - 1);
        }
        $this->data[] = $this->generateOne($this->page);
        if ($this->page + 1 <= $this->max) {
            $this->data[] = $this->generateOne($this->page + 1);
        }
        if ($this->page + 2 <= $this->max) {
            $this->data[] = $this->generateOne($this->page + 2);
        }
        if ($this->page < $this->max) {
            $this->data[] = $this->generateOne($this->page + 1, $this->next);
            $this->data[] = $this->generateOne($this->max, $this->toLast);
        }
        return $this->data;
    }

    public function limit() {
        $limit = ($this->page - 1) * $this->offset;
        return (object) ["start" => $limit, "end" => $this->offset];
    }

    function generateOne($page, $text = FALSE) {
        if (!$text) {
            $text = $page;
        }
        if ($page == $this->page) { //if it is same page, it has active class
            $active = TRUE;
        } else {
            $active = FALSE;
        }
        return (object) ["active" => $active, "page" => $page, "text" => $text];
    }

}
