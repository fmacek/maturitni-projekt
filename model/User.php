<?php

/**
 * User model
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class User {

    public $id;
    public $name;
    public $email;

    public function __construct($id, $nick, $email, $name, $about_me, $photo, $registred_time, $status) {
        $this->id = $id;
        $this->nick = $nick;
        $this->email = $email;
        $this->name = $name;
        $this->about_me = $about_me;
        $this->photo = $photo;
        $this->registred_time = $registred_time;
        $this->status = $status;
    }

    /**
     * Najde uživatele podle primárního klíče
     * @param int $id
     * @return null|User
     */
    public static function find($id, $data = array("ID", "NICK", "EMAIL", "ABOUT_ME", "NAME", "PHOTO", "STATUS", "REGISTRED_TIME", "REGISTRED_IP")) {
        $cols = "user." . implode(", user.", $data);
        $user = Database::getRow("user", $cols, "ID = :id", array(':id' => $id));
        if (!empty($user)) {
            unset($user->SALT);
            unset($user->PASS);

            return $user;
        }
        return null;
    }

    public static function getAll() {
        $users = Database::getTable("user");
        return $users;
    }

    public static function changeData($data) {
        $name = $data["name"];
        $aboutMe = $data["about_me"];
        Database::update("user", array("name" => $name, "about_me" => $aboutMe), "ID = :user_id", array("user_id" => $_SESSION["user"]["id"]));
                        Alert::add("success", "Vaše údaje byly změněny.");

        $_SESSION["user"]["name"] = $name;
        $_SESSION["user"]["about_me"] = $aboutMe;
        if (isset($data["delete_photo"])) {
            self::deletePhoto($_SESSION["user"]["id"]);
                            Alert::add("success", "Vaše fotka byla smazána.");

        }
    }

    public static function changePassword($old, $new, $new2, $user = "SESSION") {
        global $_SESSION;
        $user = ($user = "SESSION") ? $_SESSION["user"]["id"] : $user;
        if ($new != $new2) {
            Alert::add("warning", "Nová hesla se neshodují");
            $success = FALSE;
        } else {
            $salt = Database::getValue("user", "SALT", "ID = :uid", array("uid" => $user));
            if (!Database::isExist("user", "user.ID = :uid AND user.PASS = :pass AND user.`STATUS` IN ('user', 'admin', 'watchdog')", array("uid" => $user, "pass" => generateHash("super-long", $old, $salt)))) { //nenalezen
                Alert::add("warning", "Aktuální heslo je špatně.");
            } else {//nalezen
                Database::update("user", ["PASS" => generateHash("super-long", $new, $salt)], "ID = :uid", ["uid" => $user]);
                Alert::add("success", "Vaše heslo bylo změněno.");
            }
        }
    }

    /**
     * Zjistí, zda je email použitý
     * @param $email
     * @return bool
     */
    public static function deletePhoto($user) {
        $photo = Database::getValue("user", "PHOTO", "ID = :uid", array("uid" => $user));
        //unlink("".$photo);
        Database::update("user", array("PHOTO" => ""), "ID = :user_id", array("user_id" => $user));
        if ($user == $_SESSION["user"]["id"]) {
            $_SESSION["user"]["photo"] = "";
        }
    }

}
