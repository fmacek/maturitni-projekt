<?php

/**
 * Description of Vocabulary
 *
 * @author fmacek
 */
class Vocabulary {
    /*
     * Get one value from DB
     * @param string $table 
     *  name of table in database
     * @param string $col 
     *  name of col intable 
     * @param string $where [optional]
     *  WHERE ....
     * @param array $params [optional] 
     *  parameters
     * @param string $join  [optional]
     *  Joins
     * @return string $value
     *  */ 

    public static function getUserVocabulary($sourceLang, $toLang, $user = "SESSION") {
        $user = ($user == "SESSION") ? $_SESSION["user"]["id"] : $user;
        return Database::getTable("translated_vocabulary", ["original_vocabulary.TEXT AS 'ORIGINAL', translated_vocabulary.TEXT AS 'TRANSLATED' "], "original_vocabulary.LANG_ID = :olang AND translated_vocabulary.LANG_ID = :tlang ORDER BY original_vocabulary.TEXT ASC", ["olang" => $sourceLang, "tlang" => $toLang], "JOIN original_vocabulary ON translated_vocabulary.ORIGINAL_VOCABULARY_ID = original_vocabulary.ID");
    }

    public static function addToUserVocabulary($originalText, $translatedText, $user = "SESSION") {
        $user = ($user == "SESSION") ? $_SESSION["user"]["id"] : $user;
        foreach ($originalText as $original) {
            if (!Database::isExist("original_vocabulary", "TEXT = :text AND LANG_ID = :lid", ["text" => $original["text"], "lid" => $original["lang"]])) {
                Database::insert("original_vocabulary", ["TEXT" => $original["text"], "LANG_ID" => $original["lang"]]);
            }
            $id = Database::getValue("original_vocabulary", "ID", "TEXT = :text AND LANG_ID = :lid", ["text" => $original["text"], "lid" => $original["lang"]]);
            foreach ($translatedText as $translated) {
                foreach ($translated as $value) {
                    Database::insert("translated_vocabulary", ["TEXT" => $value["text"], "LANG_ID" => $value["lang"], "ORIGINAL_VOCABUlARY_ID" => $id, "USER_ID" => $user, "TYPE" => "user"]);
                }
            }
            return Database::getTable("translated_vocabulary", ["original_vocabulary.TEXT AS 'ORIGINAL', translated_vocabulary.TEXT AS 'TRANSLATED' "], "original_vocabulary.LANG_ID = :olang AND translated_vocabulary.LANG_ID = :tlang", ["olang" => $sourceLang, "tlang" => $toLang], "JOIN original_vocabulary ON translated_vocabulary.ORIGINAL_VOCABULARY_ID = original_vocabulary.ID");
        }
    }

}
