<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Project
 *
 * @author fmacek
 */
class File {

    public static function upload($file, $projectId) {
        //($projectId);
        mkdir("files/$projectId");
        mkdir("files/$projectId/import");
        mkdir("files/$projectId/export");
        $target_dir = "files/$projectId/import/";
        $suffix = explode(".", $file["name"]);
        $fileName = md5_file($file["tmp_name"]);
        $target_file = $target_dir . $fileName . "." . end($suffix);
        $uploadOk = TRUE;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

// Check if file already exists
        if (file_exists($target_file)) {
            Alert::add("warning", "Soubor již existuje.");
            $uploadOk = FALSE;
        }
// Check file size
        if ($file["size"] > 500000) {
            Alert::add("danger", "Soubor je příliš veliký.");
            $uploadOk = FALSE;
        }
// Allow certain file formats
        $types = array("text/csv", "text/x-gettext-translation", "text/xml");
        debug($file);
        if (!in_array($file["type"], $types)) {
            Alert::add("danger", "Tento formát souboru není bohužel podporován.");
            $uploadOk = FALSE;
        }
        // */
// Check if $uploadOk OK
        if ($uploadOk) {
            if (move_uploaded_file($file["tmp_name"], $target_file)) {
                Alert::add("success", "Soubor " . basename($file["name"]) . " byl úspěšně nahrán.");
            } else {
                Alert::add("warning", "Při nahrávání souboru vznikly nějaké potíže.");
            }
        }
        return $fileName . "." . end($suffix); //Name of new uploaded file
    }

    public static function import($filepath, $projectId, $version) {
        
    }

}
