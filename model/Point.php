<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Users point managment
 *
 * @author fmacek
 */
class Point {

    public static function add($point, $type, $parameter, $user = "session") {
        if ($user == "session") {
            // global $_SESSION;
            $user = $_SESSION["user"]["id"];
        }
        Database::insert("point", ["USER_ID" => $user, "TYPE" => $type, "PARAMETER" => $parameter, "VALUE" => $point]);
    }

    public static function get($user = "session") {
        if ($user == "session") {
            // global $_SESSION;
            $user = $_SESSION["user"]["id"];
        }
        return Database::getValue("point", "SUM(VALUE)", "USER_ID = :uid", ["uid" => $user]);
    }

}
