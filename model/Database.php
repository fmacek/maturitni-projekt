<?php
/**
 * Database model
 * @author Filip Macek <macek97@gmail.com>
 * @version 3.0
 */
class Database {

    /** @var  PDO */
    private static $connection;
    private $server = "xxx";
    private $database = "xxx";
    private $user = "xxx";
    private $pass = "xxx";
    private $port = "3306";

    /**
     * Creating instance to DB
     * @param NULL
     * @return object PDO
     *  */
    private static function getInstance() {

        if (!(self::$connection instanceof PDO)) {
            self::$connection = new PDO("mysql:host=" . $this->server . ";dbname=" . $this->database . ";port=" . $this->port . ";charset=utf8", $this->user, $this->pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            
            self::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }
        return self::$connection;
    }

    /**
     * Get one value from DB
     * @param string $table 
     *  name of table in database
     * @param string $col 
     *  name of col intable 
     * @param string $where [optional = "1"]
     *  WHERE ....
     * @param array $params [optional = array]
     *  parameters
     * @param string $join  [optional = NULL]
     *  Joins
     * @return string $value
     *  */
    public static function getValue($table, $col, $where = "1", array $params = array(), $join = "", $debug = FALSE) {
        $colum = ($col == "COUNT") ? "COUNT(*) AS `COUNT`" : $col;
        $sql = "SELECT $colum FROM $table $join WHERE $where";

        if ($debug) {
            debug($sql, 'SQL');
            debug($params, 'params');
        }
        try {
            $statement = self::getInstance()->prepare($sql);
            $statement->execute($params);
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
        $data = $statement->fetch(PDO::FETCH_OBJ);
        return $data->$col;
    }

    /**
     * Get values from col in DB
     * @param string $table 
     *  name of table in database
     * @param string $col 
     *  name of col intable 
     * @param string $where [optional = "1"]
     *  WHERE ....
     * @param array $params [optional = array]
     *  parameters
     * @param string $join  [optional = NULL]
     *  Joins
     * @return array $values
     *  */
    public static function getValues($table, $col, $where = "1", array $params = array(), $join = "") {
        $sql = "SELECT $col FROM $table $join WHERE $where";
        $statement = self::getInstance()->prepare($sql);
        $statement->execute($params);
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);
        if (strpos($col, ' AS ') == true) {
            $name = substr(explode("AS `", $col)[1], 0, -1);
            $a = array_column($data, $name);
        } else {
            $a = array_column($data, $col);
        }
        return $a;
    }

    /**
     * Get table in DB
     * @param string $table 
     *  name of table in database
     * @param array $col  [optional = "*"]
     *  name of col in table 
     * @param string $where [optional = "1"]
     *  WHERE ....
     * @param array $params [optional = array]
     *  parameters
     * @param string $join  [optional = NULL]
     *  Joins
     * @param string $debug [optional = FALSE]
     *  If you want to se debug for this SQL
     * @return array $values
     *  */
    public static function getTable($table, $col = "*", $where = "1", array $params = array(), $join = "", $debug = FALSE) {
        if (is_array($col)) {
            $col = implode(", ", $col);
        }
        $sql = "SELECT $col FROM $table $join WHERE $where";
        if ($debug) {
            debug($sql, 'SQL');
            debug($params, 'params');
        }
        try {
            $statement = self::getInstance()->prepare($sql);
            $statement->execute($params);
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
        return $statement->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Get one row in DB
     * @param string $table 
     *  name of table in database
     * @param array $col [optional = "*"]
     *  name of col in table 
     * @param string $where [optional = "1"]
     *  WHERE ....
     * @param array $params [optional = array]
     *  parameters
     * @param string $join  [optional = NULL]
     *  Joins
     * @return array $values
     *  */
    public static function getRow($table, $col = "*", $where = "1", array $params = array(), $join = "", $debug = FALSE) {
        if (is_array($col)) {
            $col = implode(", ", $col);
        }
        $sql = "SELECT $col FROM $table $join WHERE $where";
        if ($debug) {
            debug($sql, 'SQL');
            debug($params, 'params');
        }
        try {
            $statement = self::getInstance()->prepare($sql);
            $statement->execute($params);
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
        return $statement->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Searching if this row with this params exist
     * @param string $table 
     *  name of table in database
     * @param string $where [optional = "1"]
     *  WHERE ....
     * @param array $params [optional = array]
     *  parametrs
     * @param string $join  [optional = NULL]
     *  Joins
     * @return bool
     *  */
    public static function isExist($table, $where = "1", array $params = array(), $join = "") {
        $sql = "SELECT `$table`.`ID` FROM $table $join WHERE $where";
// debug($sql, '$sql');
// debug($params, '$params');
        try {
            $statement = self::getInstance()->prepare($sql);
            $statement->execute($params);
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
        if (empty($statement->fetch(PDO::FETCH_OBJ))) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Old clasic SELECT with returning one row
     * @param string $query
     *  SQL
     * @param array $params [optional = array]
     *  parameters
     * @return array $values
     *  */
    public static function singleSelect($query, array $params = array()) {
        try {
            $statement = self::getInstance()->prepare($query);
            $statement->execute($params);
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
        return $statement->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Old clasic SELECT with returning all rows
     * @param string $query
     *  SQL
     * @param array $params [optional = array]
     *  parameters
     * @return array $values
     *  */
    public static function multiSelect($query, array $params = array()) {
        try {
            $statement = self::getInstance()->prepare($query);
            $statement->execute($params);
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
        debug($query, "SQL");
        debug($params, "params");
        return $statement->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Inserting to table
     * @param string $table 
     *  name of table in database
     * @param array $params [optional = array]
     *  key is a name of cols and values is.... values?
     * @return NULL
     *  */
    public static function insert($table, array $cols = array()) {
        $value = ":" . implode(", :", array_keys($cols)) . "";
        $col = "`" . implode("`, `", array_keys($cols)) . "`";
        $sql = "INSERT INTO `$table` ($col) VALUES ($value)";
        $statement = self::query($sql, $cols);
    }

    /**
     * Inserting to table
     * @param string $table 
     *  name of table in database
     * @param int $id
     *  ID of row
     * @return NULL
     *  */
    public static function delete($table, int $id) {
        $statement = self::query("DELETE FROM `$table` WHERE ID = $id");
    }

    /**
     * Update row(s) in table
     * @param string $table 
     *  name of table in database
     * @param array $cols [optional = array]
     *  key is a name of cols and values is.... values?
     * @param string $where [optional = "1"]
     *  WHERE ....
     * @param array $params [optional = array]
     *  parametrs
     * @return NULL
     *  */
    public static function update($table, array $cols = array(), $where = "1", array $params = array()) {
        $keys = array_keys($params);
        $slopec = array_keys($cols);
        $set = array();
        for ($i = 0; $i < count($cols); $i++) {
            $sets[] = "`$slopec[$i]`= :$slopec[$i] ";
        }
        $super = array_merge($cols, $params);
        $set = implode(", ", $sets);
        $parametry = array_merge($cols, $params);
        $sql = "UPDATE `$table` SET $set WHERE $where";
        try {
            $statement = self::getInstance()->prepare($sql);
            $statement->execute(array_merge($params, $cols));
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
    }

    /**
     * Old clasic INSERT, UPDATE, DELETE,...
     * @param string $query
     *  SQL
     * @param array $params [optional = array]
     *  parameters
     * @return NULL
     *  */
    public static function query($query, array $params = array()) {
        try {
            $statement = self::getInstance()->prepare($query);
            $statement->execute($params);
        } catch (PDOException $e) {
            debug($e->getMessage(), "PDO error", "no-args", "bg-danger");
            debug($sql, 'SQL');
            debug($params, 'params');
            echo "<hr>";
        }
    }

}
