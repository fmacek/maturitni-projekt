<?php
// Nastavení interního kódování pro funkce pro práci s řetězci
mb_internal_encoding("UTF-8");


require 'inc/define.php';
require 'inc/autoloader.php';
require 'inc/libs/Smarty/libs/Smarty.class.php';
if (isset($_POST["selectlang"])) {
    $_SESSION["lang"] = $_POST["selectlang"];
}

$langFile = (!empty($_SESSION["lang"])) ? $_SESSION["lang"] : "cz";
global $lang;
require_once "inc/lang/$langFile.php";

// Vytvoření routeru a zpracování parametrů od uživatele z URL
$router = new Router();
$router->exec($_SERVER['REQUEST_URI']);

// Vyrenderování šablony
$router->view();