{assign "colors" ["FFD700", "C0C0C0", "CD7F32"]}

<section>
    <header>
        <h2 class="padding-left-13">{$head}</h2>
    </header>
    <div class="col-md-16">
        {$content}
    </div>
    <div class="col-md-8">
        <span class="h3">{$lang.home.top5project}</span>        

        <table class="table table-striped table-bordered margin-top-10">
            <tr><th>{$lang.home.project}</th></tr>
                    {foreach from=$topProjects key=key item=topProject}
                <tr {if $colors.$key}style="background-color:#{$colors.$key};"{/if}>
                    <td >{$topProject.project->NAME}</td>
                </tr>
            {/foreach}
        </table>
        <span class="h3">{$lang.home.top5user}</span>
        <table class="table table-striped table-bordered margin-top-10">
            <tr><th>{$lang.home.user}</th><th>{$lang.home.points}</th></tr>
                    {foreach from=$topUsers key=key item=topUser}
                <tr {if $colors.$key}style="background-color:#{$colors.$key};"{/if}>
                    <td >{$topUser->NICK}</td><td>{$topUser->POINT}</td>
                </tr>
            {/foreach}
        </table>
        <span class="h3">{$lang.home.stats}</span>
        <table class="table table-striped table-bordered margin-top-10">
            <tr><th>{$lang.home.s1}</th><td>{$statistics.projectCount}</td></tr>
            <tr><th>{$lang.home.s2}</th><td>{$statistics.textCount}</td></tr>
            <tr><th>{$lang.home.s3}</th><td>{$statistics.translateCount}</td></tr>
            <tr><th>{$lang.home.s4}</th><td>{$statistics.userCount}</td></tr>
            <tr><th>{$lang.home.s5}</th><td>{$statistics.pointCount}</td></tr>
        </table>

    </div> 

</section>