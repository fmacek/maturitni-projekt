<div class="col-md-24">
    <a href="/user/vocabulary/add" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> {$lang.vocabulary.add}</a>
    <hr>
    <form role="form" method="GET" action="">

        <div class="row" style="padding-bottom: 10px;">

            <div class="col-md-10">
                <select class="form-control sourcelang width100" name="sourcelang" >
                    <option value="">{$lang.project.list.source_lang}</option>
                    {foreach from=$languages item=language}   
                        {if $_GET.sourcelang == $language->ID}
                            <option value="{$language->ID}" selected="selected">{$language->NAME}</option>
                        {else}
                            <option value="{$language->ID}">{$language->NAME}</option>
                        {/if}
                    {/foreach}
                </select>
            </div>
            <div class="col-md-10">
                <select class="form-control sourcelang width100" name="tolang" >
                    <option value="">{$lang.project.list.to_lang}</option>
                    {foreach from=$languages item=language}      
                        {if $_GET.tolang == $language->ID}
                            <option value="{$language->ID}" selected="selected">{$language->NAME}</option>
                        {else}
                            <option value="{$language->ID}">{$language->NAME}</option>
                        {/if}
                    {/foreach}
                </select>
            </div>
            <div class="col-md-4"> 
                <button type="submit" class="btn btn-info center-block width100" ><i class="fa fa-filter"></i> {$lang.project.list.button_filter}</button>

            </div>
        </div>
    </form>
    {assign "previousLetter" ""}
    {assign "previousWord" ""}
    {foreach from=$vocabularys item=vocabulary}
        {assign "letter" $vocabulary->ORIGINAL|substr:0:1|upper}
        {assign "word" $vocabulary->ORIGINAL}
        {if $letter !== $previousLetter}
            <h1>{$letter}</h1>
        {/if}

        {if $word == $previousWord}
            <div class="row">
                <div class="col-md-11 col-md-offset-11">
                    {$vocabulary->TRANSLATED}<br />
                </div>
            </div>
        {else}
            <div class="row padding-top-3 margin-top-10" style="border-top: 1px solid #ccc;">
                <div class="col-md-11">
                    {$vocabulary->ORIGINAL}
                </div>
                <div class="col-md-11">
                    {$vocabulary->TRANSLATED}<br />
                </div>
            </div>
        {/if}

        {assign "previousLetter" $vocabulary->ORIGINAL|substr:0:1|upper}
        {assign "previousWord" $vocabulary->ORIGINAL}
    {foreachelse}
        {$lang.vocabulary.no_text}
    {/foreach}

</div>