<a href="/user/vocabulary" class="btn btn-success btn-sm" ><i class="fa fa-list"></i> {$lang.vocabulary.list}</a>


<h2>{$lang.vocabulary.add}</h2>

<form role="form" method="POST" action="">
    <div class="input-group" style='padding-bottom: 10px;'>
        <span class="input-group-btn">
            <select class="form-control sourcelang" name="sourcelang" style="width: 200px;">
                <option value="">Vyberte jazyk</option>

                {foreach from=$languages item=language}      
                    <option value="{$language->ID}">{$language->NAME}</option>
                {/foreach}
            </select>
        </span>
        <input type="text" class="form-control" name="sourcetext" value="" placeholder="{$lang.vocabulary.original}" required="required">

    </div>

    {foreach from=$languages item=language}      
        <div class="col-md-22 input{$language->ID}">
            <div class="input-group" style="padding-bottom: 10px;">
                <div class="input-group-addon">{$language->NAME}</div>
                <input type="text" class="form-control lang-{$language->ID}" name="translatedtext[{$language->ID}][]">
            </div>
        </div>
        <div class="col-md-2">
            <a class="btn btn-success center-block add-option" data-lang-id ="{$language->ID}" ><i class="fa fa-plus-circle"></i></a>
        </div>
        <div class="col-md-offset-1 newinput{$language->ID}"></div>
    {/foreach}
    <div class="col-md-24">
        <button type="submit" class="btn btn-default center-block" name="add_vocabulary"><i class="fa fa-send"></i> {$lang.form.send}</button>
    </div>

</form>



<script>
    $(document).ready(function () {

        $(".add-option").click(function () {

            var lang = $(this).data("lang-id");
            //$(".input"+lang).clone().first().prependTo(".newinput"+lang);
             $( ".newinput"+lang ).append($(".input"+lang).clone().first());
        });
        
        
        
        
        
        
        $(".sourcelang").change(function () {
            $(".sourcelang option").each(function () {
                var lang = $(this).val();
                $(".lang-" + lang).prop('disabled', false);
                $(".lang-" + lang).prop('placeholder', "{$lang.vocabulary.your_trans}");
            });
            var lang = $(".sourcelang").val();
            $(".lang-" + lang).prop('disabled', true);
            $(".lang-" + lang).prop('value', "");
            $(".lang-" + lang).prop('placeholder', "{$lang.vocabulary.cant_same_lang}");
        }).change();
    });
</script>