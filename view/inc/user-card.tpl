<ul class="nav navbar-nav navbar-right margin-top-35">
    <li class="dropdown">
        {if !$user}
            <div class="btn-group" role="group" aria-label="">
                <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#loginBox">Login</button>
                <a href="/user/register" class="btn btn-default btn-lg">Register</a>
            </div>
            <!-- Modal -->
            <div id="loginBox" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Login</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" method="POST" action="{$smarty.server.REQUEST_URI}">
                                <div class="form-group">
                                    <div class="col-md-24">

                                        <div class="input-group">
                                            <label class="sr-only" for="email">{$lang.login_form.email}</label>
                                            <div class="input-group-addon"><span class="fa fa-at fa-fw"></span></div>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="{$lang.login_form.email}" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-24">

                                        <div class="input-group">
                                            <label class="sr-only" for="pass">{$lang.login_form.pass}</label>
                                            <div class="input-group-addon"><span class="fa fa-key fa-fw"></span></div>
                                            <input type="password" class="form-control" id="pass" name="pass" placeholder="{$lang.login_form.pass}" required="required">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <button type="submit" class="btn btn-default center-block width100" name="login"><span class="fa fa-sign-in"></span> {$lang.login_form.send}</button>
                                <button type="submit" class="btn btn-danger center-block width100" name="login"><span class="fa fa-google-plus"></span> G+ login</button>

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        {else}
            <form class="form-inline" method="POST" action="{$smarty.server.REQUEST_URI}">
                <div class="col-md-24 ">
                    <div class="btn-group btn-group-justified">
                        <a href="/project/my-projects" class="btn btn-default center-block width100"><span class="fa fa-file"></span> {$lang.user_card.my_projects}</a>
                        <a href="/user/profile" class="btn btn-default center-block width100"><span class="fa fa-user"></span> {$user.nick}</a>
                        <a href="/user/vocabulary" class="btn btn-default center-block width100"><span class="fa fa-list"></span> {$lang.user_card.vocabulary}</a>
                    </div>
                </div>
                <div class="col-md-24">
                    <div class="btn-group btn-group-justified">
                        <a href="/user/profile"  class="btn btn-default center-block"><span class="fa fa-list-alt"></span> {$lang.user_card.show_profile}</a>
                        <a href="/user/logout"  class="btn btn-warning center-block"><span class="fa fa-sign-out"></span> {$lang.account.logout}</a>
                        <a href="/user/settings" class="btn btn-default center-block"><span class="fa fa-cogs"></span> {$lang.user_card.settings}</a>
                    </div>
                </div>
            </form>

            <!--
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class='fa fa-user'></span> {$user.nick} <span class='fa fa-caret-down'></span></a>
            <ul class="dropdown-menu">
                <a href="/project/my-projects" class="btn btn-default center-block"><span class="fa fa-file"></span> {$lang.user_card.my_projects}</a>
                <a href="/user/profile" class="btn btn-default center-block"><span class="fa fa-list-alt"></span> {$lang.user_card.show_profile}</a>
                <a href="/user/settings" class="btn btn-default center-block"><span class="fa fa-cogs"></span> {$lang.user_card.settings}</a>
                <a href="/user/vocabulary" class="btn btn-default center-block"><span class="fa fa-list"></span> {$lang.user_card.vocabulary}</a>
                <form class="form-inline" method="POST" action="">
                    <button type="submit" class="btn btn-warning center-block width100" name="logout"><span class="fa fa-sign-out"></span> {$lang.account.logout}</button>
                </form>
            </ul>
            -->
        {/if}
    </li>
</ul>
                
