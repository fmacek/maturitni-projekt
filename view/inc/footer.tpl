<!-- FOOTER -->

</div>
<div class="visible-xs">
    <hr />
    <p class="text-center">&copy; 2014 - {$smarty.now|date_format:"%Y"} {$lang["webname"]}</p>
</div>

<div class="hidden-xs">

    <footer class="footer">
        <div class="col-md-8 col-md-offset-8">
            <p class="text-center">&copy; 2015 - {$smarty.now|date_format:"%Y"} Filip Macek</p>
        </div>
        <div class="col-md-8 text-right">
            <form method="POST" id="changelang">
                <select name="selectlang" id="changelangselect" class="form-control selectpicker" data-mobile="true">
                    <option>{$lang.lang.select}</option>
                    {foreach from=$webLangVersion item=weblang}
                        <option value="{$weblang->CODE}">{$weblang->NAME}</option>
                    {/foreach}
                </select>
            </form>
        </div>

    </footer>
</div>
<script src="/inc/js/script.js"></script>
<script src="/inc/bootstrap/plugin/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="/inc/bootstrap/plugin/messenger/build/js/messenger.min.js"></script>
<script src="/inc/bootstrap/plugin/messenger/build/js/messenger-theme-future.js"></script>
<script src="/inc/bootstrap/plugin/select/dist/js/bootstrap-select.min.js"></script>
<script src="/inc/bootstrap/plugin/select/dist/js/i18n/defaults-cs_CZ.min.js"></script>