<!DOCTYPE html>
<html lang="{$lang.html.lang}">
    <head>
        <meta charset="UTF-8" />
        <title>{if $title}{$title|ucfirst} - {$lang.html.title}{else}{$lang.html.title}{/if}</title>
        <!--
==========================================================================================
==========================================================================================
   _____ _____   ____ _____ _      ______ _____             _      ______ _____ _______ _ 
  / ____|  __ \ / __ \_   _| |    |  ____|  __ \      /\   | |    |  ____|  __ \__   __| |
 | (___ | |__) | |  | || | | |    | |__  | |__) |    /  \  | |    | |__  | |__) | | |  | |
  \___ \|  ___/| |  | || | | |    |  __| |  _  /    / /\ \ | |    |  __| |  _  /  | |  | |
  ____) | |    | |__| || |_| |____| |____| | \ \   / ____ \| |____| |____| | \ \  | |  |_|
 |_____/|_|     \____/_____|______|______|_|  \_\ /_/    \_\______|______|_|  \_\ |_|  (_)
                                                                                          
                       This part of code show how the webpage (don't) work. 
                       So skip this part if you don't know that information.                                                                      
===========================================================================================
===========================================================================================

        -->
        {*meta tagy*}
        <meta name="description" content="{$lang.meta.description}" />
        <meta name="keywords" content="{$lang.meta.keyword}" />
        <meta name="robots" content="noindex, nofollow">
        <meta name="author" content="Filip Macek" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="theme-color" content="#da4848"> {*Chrome, Firefox OS and Opera*}
        <meta name="msapplication-navbutton-color" content="#da4848"> {* WP *}
        <meta name="apple-mobile-web-app-status-bar-style" content="#da4848"> {* iOS *}

        {*jQuery*}
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js'></script>
        {*Font*}
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        {*Bootstrap*}
        <link href="/inc/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <script src="/inc/bootstrap/js/bootstrap.min.js"></script>
        {*FontAwesome*}
        <link href="/inc/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen" />
        {*flags CSS*}
        <link href="/inc/css/flags.css" rel="stylesheet" />
        {*Messenger*}
        <link href="/inc/bootstrap/plugin/messenger/build/css/messenger.css" rel="stylesheet">
        <link href="/inc/bootstrap/plugin/messenger/build/css/messenger-theme-future.css" rel="stylesheet">
        {*x-editable*}
        <link href="/inc/bootstrap/plugin/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
        {*bootstrap select*}
        <link href="/inc/bootstrap/plugin/select/dist/css/bootstrap-select.min.css" rel="stylesheet"/>
        {*My CSS*}
        <link href="/inc/css/style.css" rel="stylesheet" />

    </head>
    <body>
        {include file="view/inc/menu.tpl"}
