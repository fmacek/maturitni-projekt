<!-- MENU -->
{assign var=menu value=
[
{$lang.menu.home}=>["icon" => "home", "url" => "home"],
{$lang.menu.projects}=>["icon" => "file", "url" => "project"],
{$lang.menu.princip}=>["icon" => "question", "url" => "error"],
{$lang.menu.statictics} => ["optgroup" =>[
{$lang.menu.statictics_users}=>["icon" => "users", "url" => "error", "title" => {$lang.menu.statictics_users_title}],
{$lang.menu.statictics_projects}=>["icon" => "gamepad", "url" => "error", "title" => {$lang.menu.statictics_projects_title}]
                    ]],
{$lang.menu.contact}=>["icon" => "envelope", "url" => "user/register"]
]}

{* Na těchto stránkách používáme soubory cookies. Léta to nikomu nevadilo, nyní ouřadové z Bruselu žádají, abychom Vás o tom informovali :-) *}
<div class="visible-xs visible-sm mobile-menu red-background ">
    <a href="/" class="">
        <img class="center-block" src="/img/logo-dark-mini.png" />
    </a>

    {assign var="urlmenu" value="/"|explode:$smarty.server.REQUEST_URI}

    <select id="mobile-menu" class="selectpicker shadow" data-mobile="true" data-width='100%' data-style="btn-warning">
        {foreach from=$menu key=name item=row}
            {if !$row["optgroup"]}
                <option data-icon="fa-{$row["icon"]}" value='/{$row["url"]}' {if $row["url"] == {$urlmenu[1]}}selected="selected"{/if}>{$name}</option>
            {else}
                <optgroup label="{$name}">
                    {foreach from=$row["optgroup"] key=key item=item}
                        <option  value='/{$item["url"]}' title="<span class='fa fa-{$item["icon"]}'></span> {$item["title"]}" {if $item["url"] == {$urlmenu[1]}}selected="selected"{/if}>{$key}</option>
                    {/foreach}
                </optgroup>
            {/if}
        {/foreach}
        {if !$user}
            <optgroup label="{$lang.account.account}">
                <option  value='/user/login' title="<span class='fa fa-sign-in'></span> {$lang.account.login}" {if "/user/login" == {$smarty.server.REQUEST_URI}}selected="selected"{/if}>{$lang.account.login}</option>
                <option  value='/user/register' title="<span class='fa fa-list'></span> {$lang.account.register}" {if "/user/register" == {$smarty.server.REQUEST_URI}}selected="selected"{/if}>{$lang.account.register}</option>
            </optgroup>
        {else}
            <optgroup label="{$user.name}">
                <option value="/project/my-projects"  {if "/project/my-projects" == {$smarty.server.REQUEST_URI}}selected="selected"{/if} title='<span class="fa fa-file"></span> {$lang.user_card.my_projects}'>{$lang.user_card.my_projects}</option>
                <option value="/user/profile"  {if "/user/profile" == {$smarty.server.REQUEST_URI}}selected="selected"{/if} title='<span class="fa fa-list-alt"></span> {$lang.user_card.show_profile}'>{$lang.user_card.show_profile}</option>
                <option value="/user/settings"  {if "/user/settings" == {$smarty.server.REQUEST_URI}}selected="selected"{/if} title='<span class="fa fa-cogs"></span> {$lang.user_card.settings}'>{$lang.user_card.settings}</option>
                <option value="/user/vocabulary"  {if "/user/vocabulary" == {$smarty.server.REQUEST_URI}}selected="selected"{/if} title='<span class="fa fa-list"></span> {$lang.user_card.vocabulary}'>{$lang.user_card.vocabulary}</option>
                <option value="/user/logout"  {if "/user/logout" == {$smarty.server.REQUEST_URI}}selected="selected"{/if} title='<span class="fa fa-sign-out"></span> {$lang.account.logout}'>{$lang.account.logout}</option>
            </optgroup>
        {/if}
    </select>
</div>

<div class="hidden-xs hidden-sm">
    <div class="navbar header navbar-default">
        <div class="container">
            <div class="col-md-12">
                <div class="navbar-header">
                    <a href="/" class="navbar-brand logo hidden-xs"><h1>{$lang.webname|lower}</h1></a>
                </div>
            </div>
            <div class="col-md-12">
                {include file="view/inc/user-card.tpl"}
            </div>



            <div class="col-md-24 padding-0">
                <div class="navbar-collapse collapse">
                    <div class="masthead ">
                        <nav class="">
                            <ul class="nav nav-justified">
                                <li><a class="menu-item" href='/project'><span class='fa fa-file'></span> {$lang.menu.projects}</a></li>
                                <li><a class="menu-item" href='/home'><span class='fa fa-question'></span> {$lang.menu.princip}</a></li>
                                <li><a class="menu-item" href='/statistic'><span class='fa fa-bar-chart'></span> {$lang.menu.statictics}</a></li>
                                <li><a class="menu-item" href='/contact'><span class='fa fa-envelope'></span> {$lang.menu.contact}</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>    
            </div>

        </div>
    </div>
</div>
<!-- /MENU -->
<nav class="navbar-fixed-top red-background shadow padding-0 hidden my-menu">
    <div class="container">
        <div class="col-md-24">

            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav nav-justified ">
                    <li><a class="menu-item" href='/project'><span class='fa fa-file'></span> {$lang.menu.projects}</a></li>
                    <li><a class="menu-item" href='/home'><span class='fa fa-question'></span> {$lang.menu.princip}</a></li>
                    <li><a class="menu-item" href='/home'><span class='fa fa-bar-chart'></span> {$lang.menu.statictics}</a></li>
                    <li><a class="menu-item" href='/home'><span class='fa fa-envelope'></span> {$lang.menu.contact}</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</nav>



<div class="container">

    <div id="alert">
        {foreach from=$alerts item=alert}
            <p class='bg-{$alert.type} padding-10'>
                {$alert.text}<br />                                            
            </p>
        {/foreach}
    </div>