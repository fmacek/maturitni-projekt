<div class='col-md-24'>
    <h2>{$lang.account.register}</h2>
    <p>{$lang.user.register.text.main}</p>
    <ul>
        <li>{$lang.user.register.text.name}</li>        
        <li>{$lang.user.register.text.birth}</li>        
        <li>{$lang.user.register.text.region}</li>        
        <li>{$lang.user.register.text.photo}</li>        
    </ul>
    <p>{$lang.user.register.text.thanks}</p>
    <!--<div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>-->
    <script>
        function onSignIn(googleUser) {
            // Useful data for your client-side scripts:
            var profile = googleUser.getBasicProfile();
            console.log("ID: " + profile.getId()); // Don't send this directly to your server!
            console.log("Name: " + profile.getName());
            console.log("Image URL: " + profile.getImageUrl());
            console.log("Email: " + profile.getEmail());

            // The ID token you need to pass to your backend:
            var id_token = googleUser.getAuthResponse().id_token;
            console.log("ID Token: " + id_token);
        }
        ;
    </script>
    <!--
        <button class="btn btn-danger"><span class="fa fa-google-plus-square"></span> {$lang.user.register.register_via.google}</button>
        <button class="btn btn-info"><span class="fa fa-facebook-square"></span> {$lang.user.register.register_via.facebook}</button>
        <button class="btn btn-info"><span class="fa fa-twitter-square"></span> {$lang.user.register.register_via.twitter}</button>
    -->  
    <h3>{$lang.user.register.register_form}</h3>
    <form role="form" method="POST" action="">
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-at"></i></div>
            <input type="email" class="form-control" name="email" value="" id="Email" placeholder="{$lang.user.email}" required="required">
        </div><br>  
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-user"></i></div>
            <input type="text" class="form-control" name="nick" value="" id="Nick" placeholder="{$lang.user.nick}" required="required">
        </div><br>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-key"></i></div>
            <input type="password" class="form-control" name="pass" id="Pass" placeholder="{$lang.user.pass}" required="required">
        </div><br>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-key"></i></div>
            <input type="password" class="form-control" name="pass2" id="Pass2" placeholder="{$lang.user.register.pass_again}" required="required">
        </div><br>

        <button type="submit" class="btn btn-default center-block" name="register"><i class="fa fa-check"></i> {$lang.user.register.button}</button>
    </form>

</div>