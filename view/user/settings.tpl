<script>
    $(document).ready(function () {
        $(".controlpass").keypress(function () {
            if ($(this).val().length >= 6) {
                alert("gut");
            } else {
                // Disable submit button
            }
        });
    });

</script>
<div class="col-md-24">
    <h2>Změna hesla</h2>
    <form class="form-horizontal" method="POST"">
        <div class="form-group">
            <div class="col-md-24">
                <label for="oldpass">{$lang.user.settings.pass.old_pass}</label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="fa fa-key"></span></div>
                    <input type="password" class="form-control" id="oldpass" name="oldpass">
                </div>
                <p class="help-block">{$lang.user.settings.pass.text1}</p>
            </div>
            <div class="col-md-24">

                <label for="newpass">{$lang.user.settings.pass.new_pass}</label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="fa fa-key"></span></div>
                    <input type="password" class="form-control controlpass" id="newpass" name="newpass">
                </div>
                <p class="help-block">{$lang.user.settings.pass.text2}</p>
            </div>
            <div class="col-md-24">

                <label for="newpass2">{$lang.user.settings.pass.new_pass_again}</label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="fa fa-key"></span></div>
                    <input type="password" class="form-control controlpass" id="newpass2" name="newpass2" >
                </div>
                <p class="help-block">{$lang.user.settings.pass.text3}</p>

            </div>

            <button type="submit" class="btn btn-lg btn-info center-block" name="change_profile_password"><span class="fa fa-send"></span> {$lang.user.settings.pass.button}</button>
        </div>

    </form>


    <h2>{$lang.user.settings.head}</h2>

    <form class="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="email">{$lang.user.email}</label>
            <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-envelope-o"></span></div>
                <input type="email" class="form-control" id="email" value="{$user.email}" disabled>
            </div>
            <p class="help-block">{$lang.user.settings.email.description}</p>


            <label for="nick">{$lang.user.nick}</label>
            <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-user"></span></div>
                <input type="text" class="form-control" id="nick" value="{$user.nick}" disabled>
            </div>
            <p class="help-block">{$lang.user.settings.nick.description}</p>


            <label for="name">{$lang.user.name}</label>
            <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-smile-o"></span></div>
                <input type="text" name="name" class="form-control" id="name" placeholder="{$lang.user.name}" value="{$user.name}">
            </div>
            <p class="help-block">{$lang.user.settings.name.description}</p>


            <label for="about_me">{$lang.user.about_me}</label>
            <textarea name="about_me" class="form-control" placeholder="{$lang.user.about_me}">{$user.about_me}</textarea>
        </div>
        <p class="help-block">{$lang.user.settings.about_me.description}</p>

        <h3 class="center-text">{$lang.user.photo}</h3>
        <img src='{$smarty.config.urlroot}/{$user.photo}'  alt="{$lang.user.photo}" title="{$lang.user.your_photo}" class="img-responsive img-thumbnail center-block">

        <div class="form-group">
            <label for="file">{$lang.user.settings.photo.upload_new}</label>
            <input type="file" id="file">
            <p class="help-block">{$lang.user.settings.photo.description}</p>
        </div>
        {if !empty($user.photo)} 
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="delete_photo"> {$lang.user.settings.photo.delete}
                </label>
            </div>
        {/if}

        <button type="submit" class="btn btn-lg btn-info center-block" name="change_profile_info"><span class="fa fa-send"></span> {$lang.form.send}</button>
    </form>
</div>