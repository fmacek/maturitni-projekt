<div class='col-md-24'>
    <div class='row'>
        <div class='col-md-24'>

            <h2>{$profile->NICK}</h2>
        </div>
        <div class='col-md-10'>

            <div class='thumbnail'>
                {if $profile->PHOTO}
                    <img src='{$smarty.config.urlroot}/{$profile->PHOTO}' alt="{$lang.user.photo}" title="{$lang.user.your_photo}" class="img-responsive img-thumbnail center-block">
                {else}
                    <img src='{$smarty.config.urlroot}/img/user-photo/user-default.png' alt="{$lang.user.photo}" title="{$lang.user.your_photo}" class="img-responsive img-thumbnail center-block">
                {/if}
            </div>
        </div>
        <div class='col-md-14 '>
            <b>{$lang.user.name}:</b> {$profile->NAME}
        </div>
        {*
        <div class='col-md-14'>
        <b>{$lang.user.rank}:</b> Admirál překladů (<i>zatím staický</i>)
        </div>
        *}
        <div class='col-md-14'>
            <h3>{$lang.user.about_me}</h3>
            <p>{$profile->ABOUT_ME}</p>
        </div>
    </div>
</div>