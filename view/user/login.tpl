<h2>{$lang.account.login}</h2>
{if $user}
    <meta http-equiv="refresh" content="1; URL=/">      
{/if}
<form class="form-horizontal" role="form" method="POST" action="">
    <div class="form-group">
        <div class="input-group">
            <label class="sr-only" for="email">{$lang.login_form.email}</label>
            <div class="input-group-addon"><span class="fa fa-at"></span></div>
            <input type="email" class="form-control" id="email" name="email" placeholder="{$lang.login_form.email}" required="required">
        </div>
        <div class="input-group">
            <label class="sr-only" for="pass">{$lang.login_form.pass}</label>
            <div class="input-group-addon"><span class="fa fa-key"></span></div>
            <input type="password" class="form-control" id="pass" name="pass" placeholder="{$lang.login_form.pass}" required="required">
        </div>
    </div>
    <br><br>
    <button type="submit" class="btn btn-default center-block width100" name="login"><span class="fa fa-sign-in"></span> {$lang.login_form.send}</button>
        <a href="user/register" class="btn btn-default width100"><span class="fa fa-list"></span> {$lang.account.register}</a>

</form>