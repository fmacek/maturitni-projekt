<form method="POST" action="" >

    <div class="col-md-18">
        <select name="lang[]" class="selectpicker show-menu-arrow" multiple data-live-search="true" title="{$lang.project.list.source_lang}" data-width="100%">
            {foreach from=$langs item=language}
                {assign "selected" ""}
                {if $_POST["lang"]}
                    {if in_array($language->ID,$_POST["lang"])}
                        {assign "selected" "selected='selected'"}
                    {/if}
                {else }
                    {assign "selected" "selected='selected'"}
                {/if}

                <option value="{$language->ID}" {$selected} >{$language->NAME}</option>
            {/foreach}
        </select>
    </div>
    <div class="col-md-4">
        <button type="submit" name="filter" class="btn btn-primary width100"><span class="fa fa-filter"></span> {$lang.project.list.button_filter}</button>
    </div>
</form>
<div class="col-md-24">
    <div class="row visible-lg visible-md">
        <div class="col-md-1 center-text">
            #
        </div>
        <div class="col-md-3 center-text">
            Uživatel
        </div>
        <div class="col-md-5 center-text">
            Body
        </div>
        <div class="col-md-5 center-text">
            Počet návrhů
        </div>
        <div class="col-md-5 center-text">
            Počet schválených
        </div>
        <div class="col-md-5 center-text">
            Počet hlasování
        </div>
    </div>
    <hr>
    {assign "position" 1}

    {foreach from=$users key=key item=user}
        <div class="row lichy padding-top-10">
            <div class="col-md-1 center-text visible-lg visible-md">
                {$position}
            </div>
            <div class="col-md-3 center-text">
                <div class="col-md-24">
                    <span class="visible-sm visible-xs"><a href="/user/profile/{$user["user"]->NICK}" class="h3">#{$position} {$user["user"]->NICK}</a></span> 
                    <span class="visible-md visible-lg"><a href="/user/profile/{$user["user"]->NICK}" class="h3"> {$user["user"]->NICK}</a></span> 

                </div>
                <div class="col-md-24">

                    <a href="/user/profile/{$user["user"]->NICK}" class="h3"> 
                        {if !empty($user["user"]->PHOTO)}
                            <img src="/{$user["user"]->PHOTO}" class="img-responsive img-thumbnail" alt="">
                        {else} 
                            <img src="/img/user-photo/user-default.png" class="img-responsive img-thumbnail" alt="">
                        {/if}
                    </a>
                </div>
            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Body:</span> {$user["point"]}
            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Počet návrhů:</span> {$user["sug"]}
            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Počet schválených</span> {$user["app"]}
            </div>
            <div class="col-md-5 center-text">
                <span class="visible-sm visible-xs">Počet hlasování:</span> {$user["vote"]}
            </div>
        </div>
        {assign "position" $position +1}


    {foreachelse}
        <p>Nenalezen žádný uživatel.</p>
    {/foreach}
</div>