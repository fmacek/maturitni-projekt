<script>
    $(document).ready(function () {

        $('#change-lang-project-translate').on('change', function () {
            var pathname = window.location.pathname.split('/');
            pathname[4] = this.value;
            var url = pathname.join('/');
            window.location.href = url;
        });
    });
</script>

{assign idCol 1}
{assign origCol 10}
{assign suggCol 13}
{if empty($url.3)}
    {append var='url' value=$project.translateTo.0->LANG_ID index='3'}
{/if}


<div class="col-md-24">
    <div class="col-md-24">
        <div class="row h2">
            {$project.name}
        </div>

        {if $texts}
            <div class="row lichy vcenter" >
                <div class="col-md-{$idCol} bold">{$lang.project.text.id}</div>{*sloupec s web ID*}
                <div class="col-md-{$origCol-1} bold">{$lang.project.text.original_text}</div>{*sloupec s originálem*}
                <div class="col-md-{$suggCol} bold">
                    <select name="selectlang" id="change-lang-project-translate" class="form-control">
                        {foreach from=$project.translateTo item=language}
                            {if $language.id == $url.3}
                                <option value="{$language.id}" selected>{$language.lang} ({$language.progress}%)</option>
                            {else}
                                <option value="{$language.id}">{$language.lang} ({$language.progress}%)</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>{*sloupec s návrhy*}
            </div>
            {foreach from=$texts item=text}
                <div class="row lichy vcenter margin-bottom-and-up-10px" >
                    <div class="col-md-{$idCol}">{$text.id_web}</div>{*sloupec s web ID*}
                    <div class="col-md-{$origCol}">{*sloupec s originálem*}
                        <small>
                            <i style="word-break: break-all;">{$text.description}</i>
                        </small><br />
                        <b>{$text.text}</b>
                    </div>
                    <div class="col-md-{$suggCol}">{*sloupec s návrhy*}
                        <div class="col-md-15">
                            {$text.translate->TEXT}
                        </div>

                    </div>

                </div>

            {/foreach}



        {else}
            <p class="h2">{$lang.project.text.no_text}</p>
        {/if}
    </div>
</div>