 
<script>

    $(document).ready(function () {


        $("#source_lang").change(function () {
            $("#source_lang option").each(function () {
                var lang = $(this).val();
                $(".lang-" + lang).prop('disabled', false);
            });
            var lang = $("#source_lang").val();
            $(".lang-" + lang).prop('disabled', true);
            $(".lang-" + lang).prop('checked', false);

        }).change();
    });
    //*/
</script>
<div class="col-md-24">

    <h2 class="h1">{$lang.project.import.new.head}</h2>
    <form class="" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">{$lang.project.import.new.name.text}</label>
            <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-file-o"></span></div>
                <input type="text" name="name" class="form-control" id="name" placeholder="{$lang.project.import.new.name.text}">
            </div>
            <p class="help-block">{$lang.project.import.new.name.description}</p>
        </div>

        <div class="form-group">
            <label for="description">{$lang.project.import.new.description.text}</label>
            <div class="input-group">
                <textarea class="form-control" id="description" name="description" placeholder="{$lang.project.import.new.description.text}"></textarea>
            </div>
            <p class="help-block">{$lang.project.import.new.description.description}</p>
        </div>

        <div class="form-group">
            <label for="source_file">{$lang.project.import.new.source_file.text}</label>
            <div class="input-group">
                <input type="file" name="source_file" id="source_file">
            </div>
            <p class="help-block">{$lang.project.import.new.source_file.description}</p>
        </div>

        <div class="form-group">
            <label for="email">{$lang.project.import.new.source_lang.text}</label>
            <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-comments"></span></div>
                <select id="source_lang" name="source_lang" class="form-control">
                    {foreach from=$langs item=language}
                        <option value="{$language->ID}">{$language->NAME}</option>
                    {/foreach}

                </select>
            </div>
            <p class="help-block">{$lang.project.import.new.source_lang.description}</p>
        </div>
        <script>
            function toggle(source) {
                //checkboxes = document.getElementsByName('translateTo[]');
                for (var i = 0, n = $('.' + source).length; i < n; i++) {
                    console.log("a = " + checkboxes[i] + "b = " + source);
                    if ($("." + source + "-" + i).hasClass("lang-" + $("#source_lang").val(); )) {
                        checkboxes[i].checked = source.checked;
                    }
                }
            }
        </script>
        <label>{$lang.project.import.new.to_lang.text}</label>

        <div class="checkbox">
            <label><input type="checkbox" onClick="toggle('translateTo')" /> {$lang.form.select_all}</label><br/><br />
                {append "i" 0}
                {foreach from=$langs item=language}
                <label><input disabled="disabled" type="checkbox" name="translateTo[]" value="{$language->ID}" class="translateTo lang-{$language->ID}" > {$language->NAME}</label><br /> 
                {/foreach}
        </div>
        <p class="help-block">{$lang.project.import.new.to_lang.description}</p>
        <label>{$lang.project.import.new.to_lang.text}</label>

        <div class="checkbox">
            <label><input type="checkbox" name="visible" /> Viditelný</label><br/><br />
             
        </div>
        <p class="help-block">{$lang.project.import.new.to_lang.description}</p>

        <button type="submit" name="add_project" class="btn btn-lg btn-info center-block"><span class="fa fa-send"></span> {$lang.form.send}</button>
    </form>

</div>