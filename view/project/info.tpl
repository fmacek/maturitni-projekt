<div class="col-md-8">
    <h2>{$project.name}</h2> 
    {if !empty($project.image)}
        <img src="/img/project/{$project.id}/{$project.image}" class="img-responsive img-thumbnail">
    {else} 
        <img src="/img/project/default.jpg" class="img-responsive img-thumbnail">
    {/if}
    <div class="col-md-24">
        <h3>{$lang.project.info.owner}</h3>
        {if $profile->PHOTO}
            <img src='{$smarty.config.urlroot}/{$project.owner->PHOTO}' alt="{$lang.user.photo}" title="{$lang.project.info.user_photo.owner}" class="img-responsive img-thumbnail center-block"><br />
        {else}
            <img src='{$smarty.config.urlroot}/img/user-photo/user-default.png' alt="{$lang.user.photo}" title="{$lang.project.info.user_photo.owner}" class="img-responsive img-thumbnail center-block"><br />
        {/if}
        <p class="text-center h4">
            {$project.owner->NICK}
        </p>
    </div>
    <div class="col-md-24">
        <h3>Správci</h3>
        {foreach from=$project.managers item=manager}
            {if $manager->NICK !== $project.owner->NICK}
                <div class="col-md-12">
                    <span class="center-block">
                        {$manager->NICK}
                    </span>
                    <img src='{$smarty.config.urlroot}/{$manager->PHOTO}' alt="{$lang.user.photo}" title="{$lang.project.info.user_photo.manager}" class="img-responsive img-thumbnail center-block"><br />
                </div>
            {/if}
        {/foreach}
    </div>
</div>
<div class="col-md-16">
    <h3 class="h2">{$lang.project.info.lang_prog}</h3>
    {foreach from=$project.translateTo item=language}
        {if $user and 1==2}
            <div class='col-md-2'>


                <form method="POST">
                    <input type="hidden" name="project_id" value="{$project.id}" />
                    {if $language.favorite}
                        <button type="submit" name="favorite_project" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="{$lang.project.favorite_project.remove}"><span class="fa fa-star"></span></button>
                        {else}
                        <button type="submit" name="favorite_project" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="{$lang.project.favorite_project.add}"><span class="fa fa-star-o"></span></button>
                        {/if}
                </form>


            </div>
        {/if}
        <div class='col-md-2'>
            {$language.name}
        </div>
        <div class='col-md-22'>
            <div class="progress">
                <div class="progress-bar progress-bar-success progress-bar-striped" style="width: {$language.done}%">
                    <strong>{$language.done}% </strong>
                </div>
                <div class="progress-bar progress-bar-danger progress-bar-striped" style="width: {$language.not_done}%">
                    <strong>{$language.not_done}%</strong>
                </div>
            </div>
        </div>
    {/foreach}
    <p class="h2">{$lang.project.info.about}</p>
    {$project.description}
    <p class="h2">{$lang.project.info.downloadable}</p>
    <select id="showFilesByLang" class="form-control">
        {foreach from=$project.translateTo item=language}
            <option value="{$language.id}">{$language.name}</option>
        {/foreach}
    </select>
    {foreach from=$project.translateTo item=xyz}
        {assign "id" $xyz.id}
        <div class="files files-{$id} hidden">
            <p class="h3">{$xyz.name}</p>
            {if $project.files[$id]}
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>{$lang.project.info.type}</th>
                        <th>{$lang.project.info.version}</th>
                        <th>{$lang.project.info.des}</th>
                        <th>{$lang.project.info.date}</th>
                        <th>{$lang.project.info.download}</th>
                    </tr>
                {/if}
                {foreach from=$project.files[$id] item=file}
                    <tr>
                        <td>{$file->TYPE}</td>
                        <td>{$file->VERSION}</td>
                        <td>{$file->DESCRIPTION}</td>
                        <td>{$file->TIME}</td>
                        <td><button class="btn btn-sm btn-info">{$lang.project.info.download}</button></td>
                    </tr>
                    {$file->FILEPATH}
                    <br>
                {foreachelse}
                    <p>{$lang.project.info.no_file}</p>
                {/foreach}
                {if $project.files[$id]}
                </table>
            {/if}
        </div>
    {/foreach}

</div>


<script>
    {literal}
        $(document).ready(function () {
            $("#showFilesByLang").click(function () {//schvalování/zamítnutí textu
                var id = $(this).val();
                $("#showFilesByLang option").each(function ()
                {
                    $(".files-" + $(this).val()).addClass("hidden");
                });
                $(".files-" + id).removeClass("hidden");
            }
            );
            $(".files").first().removeClass("hidden");
        });
    {/literal}

</script>