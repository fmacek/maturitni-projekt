{literal} 

    <script type="text/javascript">
        $(document).ready(function () {
            $(".visible-lang").hover(function () {
                if ($(this).hasClass("btn-success")) {
                    $(this).addClass("btn-danger");
                    $(this).removeClass("btn-success");
                }
                else {
                    $(this).addClass("btn-success");
                    $(this).removeClass("btn-danger");
                }
            }, function () {
                if ($(this).hasClass("btn-success")) {
                    $(this).addClass("btn-danger");
                    $(this).removeClass("btn-success");
                }
                else {
                    $(this).addClass("btn-success");
                    $(this).removeClass("btn-danger");
                }
            });
            $(".user-settings").click(function () {
                var name = $(this).data('name');
                var pk = $(this).data('pk');
                if (name == "add-manager") {
                    var value = $("#nick").val();
                    var langId = $("#lang-manager").val();
                    var dataString = {"name": name, "pk": pk, "value": value, "langId": langId}
                }
                else {
                    var value = $(this).data('value');
                    var dataString = {"name": name, "pk": pk, "value": value}
                }
                $.ajax({
                    type: "post",
                    url: "/ajax/project-settings",
                    data: dataString,
                    success: function (html) {
                        if (!html.match("^success:")) {//pokud to není úspěšný text
                            if (html) {
                                Messenger().run
                                Messenger().post({
                                    message: html,
                                    type: 'error',
                                    showCloseButton: true
                                });
                            }
                        }
                        else {
                            if (name == "add-manager") {
                                var data = html.substr(8);
                                $('.project-manager').last().html("<img src='{/literal}{$smarty.config.urlroot}{literal}/" + data + "' class='img-responsive img-thumbnail'><br>" + value + "<div class='project-manager'></div>");
                            }
                            else {
                                $("#manager" + value).remove();
                            }
                        }
                    }
                });
                //*/
            });
        });
    </script>
{/literal} 
<div class='col-md-24'>
    <h2>
        <span class="fa fa-edit"></span> <a class="xeditable" id="project-name" data-type="text" data-pk="{$project.id}" data-url="/ajax/project-settings" data-mode="inline" data-onblur="submit" data-title="{$lang.project.settings.name}">{$project.name}</a>
    </h2> 
    <div class="row">
        <div class="col-md-8">
            {if !empty($project.image)}
                <img src="/img/project/{$project.id}/{$project.image}" class="img-responsive img-thumbnail">
                <button class="btn btn-xs btn-danger user-settings" id="delete-photo" data-pk="{$project.id}" data-name="delete-photo">
                    <span class="fa fa-times"></span>
                </button> 
            {else} 
                <img src="/img/project/default.jpg" class="img-responsive img-thumbnail">
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="file">{$lang.project.settings.upload_new_photo}</label>
                        <input type="file" id="file">
                        <p class="help-block">{$lang.project.settings.project_photo}</p>
                    </div>
                    <button type="submit" class="btn btn-xs btn-info" name="change_profile_info"><span class="fa fa-send"></span> {$lang.form.send}</button>
                </form>
            {/if}
            <h3>{$lang.project.settings.owner}</h3>
            <span class="center-text">
                {$project.owner->NICK}
            </span>
            {if $user.photo}
                <img src='{$smarty.config.urlroot}/{$profile->PHOTO}' alt="{$lang.user.photo}" title="{$lang.user.your_photo}" class="img-responsive img-thumbnail center-block">
            {else}
                <img src='{$smarty.config.urlroot}/img/user-photo/user-default.png' alt="{$lang.user.photo}" title="{$lang.user.your_photo}" class="img-responsive img-thumbnail center-block">
            {/if}
            <center>{$user.nick}</center>
        </div>
        <div class="col-md-16">
            <div class="col-md-24">
                <h3>{$lang.project.settings.lang_progress}</h3>
                {foreach from=$project.translateTo item=language}

                    <div class='row'>
                        <div class='col-md-2'>
                            {$language.name}
                        </div>
                        <div class='col-md-19'>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped" style="width: {$language.done}%">
                                    <strong>{$language.done}% </strong>
                                </div>
                                <div class="progress-bar progress-bar-danger progress-bar-striped" style="width: {100 - $language.done}%">
                                    <strong>{100 - $language.done}%</strong>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <form method="POST">
                                <div class="btn-group" role="group" aria-label="">
                                    <button type="submit" class="btn btn-success btn-xs visible-lang"><span class="fa fa-eye"></span></button>
                                    <button type="submit" class="btn btn-warning btn-xs"><span class="fa fa-times"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                {/foreach}
                <div class='row'>

                    <div class='col-md-24'>
                        <form method="POST" action="">
                            <div class="input-group"> 
                                <div class="input-group-btn width100">
                                    <div class="btn-group width100"> 
                                        <select name="lang" class="form-control width100">
                                            <option value="">{$lang.project.settings.new_lang}</option>

                                            {foreach from=$langs item=language}
                                                {assign "can" "1"}
                                                {foreach from=$project.translateTo item=search}
                                                    {if $search.id == $language->ID or $search.id == $project.language}
                                                        {assign "can" "0"}
                                                    {/if}
                                                {/foreach}
                                                {if $language->ID !== $project.language}
                                                    {if $can == 1} 
                                                        <option value="{$language->ID}">{$language->NAME}</option>
                                                    {/if}
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>  
                                <span class="input-group-btn">
                                    <button type="submit" name="add-lang" class="btn btn-info" >
                                        <span class="fa fa-send"></span>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-24">
                <h3>{$lang.project.settings.managers}</h3>
                {foreach from=$project.managers item=manager}
                    {if $manager->NICK !== $user.photo}
                        <div class="col-md-12" id="manager{$manager->ID}">
                            {if $manager->PHOTO}
                                <img src='{$smarty.config.urlroot}/{$manager->PHOTO}' alt="{$lang.user.photo}" title="{$lang.user.photo_user}" class="img-responsive img-thumbnail center-block">
                            {else}
                                <img src='{$smarty.config.urlroot}/img/user-photo/user-default.png' alt="{$lang.user.photo}" title="{$lang.user.photo_user}" class="img-responsive img-thumbnail center-block">
                            {/if}

                            <center>
                                <button class="btn btn-xs btn-danger user-settings" id="delete-user" data-pk="{$project.id}" data-name="delete-manager" data-value="{$manager->ID}">
                                    <span class="fa fa-times"></span>
                                </button> 
                                {$manager->NICK} ({$manager->LANG})
                            </center>
                        </div>
                    {/if}
                {/foreach}
                <div class="project-manager"></div>
                <div class='col-md-24'> 
                    <div class="form-group">
                        <label class="sr-only" for="nick">{$lang.project.settings.add_new_manager}</label>
                        <div class="input-group"> 
                            <div class="input-group-btn">
                                <div class="btn-group">

                                    <select id="lang-manager" class="form-control" name="type" style="width: 70px;">
                                        {foreach from=$project.translateTo item=language}
                                            <option value="{$language["id"]}">{$language["name"]}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>  
                            <input class="form-control" type="text" id="nick" placeholder="{$lang.user.nick}">
                            <span class="input-group-btn">
                                <button id="add-user" class="btn btn-info user-settings" data-pk="{$project.id}" data-name="add-manager">
                                    <span class="fa fa-send"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-24 justify">
            <br /><br />
            <h3>{$lang.project.settings.description}</h3>
            <span class="fa fa-edit"></span> <a class="xeditable" id="project-description" data-type="textarea" data-pk="{$project.id}" data-url="/ajax/project-settings" data-mode="inline" data-onblur="submit" data-title="{$lang.project.settings.project_description}">{$project.description}</a>
            <p class="h2">{$lang.project.info.trans}</p>
            <select id="showFilesByLang" class="form-control">

                {foreach from=$project.translateTo item=language}
                    <option value="{$language["id"]}">{$language["name"]}</option>
                {/foreach}
            </select>
            {foreach from=$project.translateTo item=xyz}
                {assign "id" $xyz["id"]}
                <div class="files files-{$id} hidden">
    
                    <p class="h3">{$xyz["name"]}</p>
                    {if $project.files[$id]}
                        <table class="table table-striped table-responsive">
                            <tr>
                                <th>{$lang.project.info.type}</th>
                                <th>{$lang.project.info.version}</th>
                                <th>{$lang.project.info.des}</th>
                                <th>{$lang.project.info.date}</th>
                                <th>{$lang.project.info.download}</th>
                            </tr>
                        {/if}
                        {foreach from=$project.files[$id] item=file}
                            <tr>
                                <td>{$file->TYPE}</td>
                                <td>{$file->VERSION}</td>
                                <td>{$file->DESCRIPTION}</td>
                                <td>{$file->TIME}</td>
                                <td><button class="btn btn-sm btn-info">{$lang.project.info.download}</button></td>
                            </tr>
                            {$file->FILEPATH}
                            <br>
                        {foreachelse}
                            <p>{$lang.project.info.no_file}</p>
                        {/foreach}
                        {if $project.files[$id]}
                        </table>
                    {/if}
                    
                </div>
            {/foreach}
            <a class="btn btn-info" href="/project/export-preview/{$url[2]}/{$xyz->LANG_ID}">{$lang.project.info.preview}</a>
            <span class="btn btn-info " data-toggle="modal" data-target="#debugBox">{$lang.project.info.create}</span>

        </div>
    </div>
<script>
    {literal}
        $(document).ready(function () {
            $("#showFilesByLang").click(function () {//schvalování/zamítnutí textu
                var id = $(this).val();
                $("#showFilesByLang option").each(function ()
                {
                    $(".files-" + $(this).val()).addClass("hidden");
                });
                $(".files-" + id).removeClass("hidden");
            }
            );
            $(".files").first().removeClass("hidden");
        });
    {/literal}

</script>

    <div id="debugBox" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{$lang.project.info.new_export}</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="POST" action="{$smarty.server.REQUEST_URI}">
                        <input type="hidden" class="form-control" name="type" value="{if $language.done == 100}finished{else}beta{/if}" />

                        <select name="lang" class="form-control">
                            <option value="{$language->LANG_ID}">{$lang.project.info.export_lang}</option>
                            {foreach from=$project.translateTo item=language}
                                <option value="{$language.id}">{$language.name} ({$language.done}%)</option>
                            {/foreach}
                        </select>
                        <input type="text" class="form-control" name="version" placeholder="{$lang.project.info.export_version}" />
                        <textarea class="form-control" name="description" placeholder="{$lang.project.info.export_des}"></textarea>
                        {$lang.project.info.export_public}<input type="checkbox" class="checkbox-inline" name="status" />
                        <br><br>
                        <button type="submit" class="btn btn-default center-block width100" name="export"><span class="fa fa-send"></span> {$lang.form.send}</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>