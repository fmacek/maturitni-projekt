<div class="visible-xs visible-sm">
    <center>{$lang.project.list.filter}</center>
</div>
<form method="POST" action="" >
    <div class="col-md-8">
        <select name="fromlang[]" class="selectpicker show-menu-arrow" multiple data-live-search="true" title="{$lang.project.list.source_lang}" data-width="100%">
            {foreach from=$langs item=language}
                {assign "selected" ""}
                {if $_POST["fromlang"]}
                    {if in_array($language->ID,$_POST["fromlang"])}
                        {assign "selected" "selected='selected'"}
                    {/if}
                {/if}

                <option value="{$language->ID}" {$selected}>{$language->NAME}</option>
            {/foreach}
        </select>
    </div>
    <div class="col-md-8">
        <select name="tolang[]" class="selectpicker show-menu-arrow" multiple data-live-search="true" title="{$lang.project.list.to_lang}" data-width="100%">
            {foreach from=$langs item=language}
                {assign "selected" ""}
                {if $_POST["tolang"]}
                    {if in_array($language->ID,$_POST["tolang"])}
                        {assign "selected" "selected='selected'"}
                    {/if}
                {/if}

                <option value="{$language->ID}" {$selected}>{$language->NAME}</option>

            {/foreach}
        </select>
    </div>
    <div class="col-md-8">
        <button type="submit" name="filter_project" class="btn btn-primary width100"><span class="fa fa-filter"></span> {$lang.project.list.button_filter}</button>
    </div>
</form>

<div class="col-md-24">

    <div class="page-header">
        <h2 class="padding-top-10 margin-top-10">{$lang.project.list.head}</h2>
        {* <a href="/project/add" class="btn btn-bg btn-info">{$lang.project.list.new}</a> *}
    </div>


    {foreach from=$projects item=project}

            


            <div class="row">
            
                <div class="col-md-3 col-xs-12">
                    <a href="/project/info/{$project.url}" alt="{$lang.project.list.img}">
                        {if !empty($project.image)}
                            <img src="/img/project/{$project.id}/{$project.image}" class="img-responsive img-thumbnail" alt="{$lang.project.list.image}{$project.name}">
                        {else} 
                            <img src="/img/project/default.jpg" class="img-responsive img-thumbnail" alt="{$lang.project.list.image} {$project.name}">
                        {/if}
                    </a>
                </div>
                
                <div class="col-md-21 col-xs-12">
                    <div class="row">
                         <h3 style='margin: 0 0 10px 0'>   
                            {if $project.user_id == $user.id}{*Pokud je to projekt uživatele, zobraz mu tlačítko nastavení*}
                                    <a href="/project/settings/{$project.url}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="{$lang.button.settings}"><span class="fa fa-gear"></span></a>
                                    {/if}
                                <a href="/project/info/{$project.url}" alt="{$lang.project.list.img}">
                                    {$project.name} ({$project.language})
                                </a>
                          </h3>
                    </div>
                    <div class="row">
                          <div class="col-md-7 col-xs-12  text-right hidden-md hidden-lg">
                              <a href = "/project/info/{$project.url}" class="btn btn-default width100"><span class="fa fa-info-circle"></span> {$lang.project.list.show_more} <span class="fa fa-angle-double-right"></span></a>
                              <a href = "/project/text/{$project.url}" class="btn btn-primary width100"><span class="fa fa-align-justify"></span> {$lang.project.list.show_text} <span class="fa fa-angle-double-right"></span></a>
                          </div>
                          
                          <div class="col-md-16 ">
                              <div class="col-md-24 col-xs-24 justify"><span>{$project.description}</span></div>
          
                              <div class="col-md-12 col-xs-24 padding-top-10" >
                                  <span>{$lang.project.list.translate_to}: {' | '|implode:$project.translateTo}</span>
                              </div>
                          </div>
                         
                          <div class="col-md-6  text-right hidden-xs hidden-sm">
                              <div class="btn-group">
                                  <a href = "/project/info/{$project.url}" class="btn btn-default width100"><span class="fa fa-info-circle"></span> {$lang.project.list.show_more} <span class="fa fa-angle-double-right"></span></a>
                                  <a href = "/project/text/{$project.url}" class="btn btn-primary width100"><span class="fa fa-align-justify"></span> {$lang.project.list.show_text} <span class="fa fa-angle-double-right"></span></a>
                              </div>
                          </div>
                    </div>
                    
                    {*
                    
                    <div class="row">
                          <div class="col-md-7 col-xs-12  text-right hidden-md hidden-lg">
                              <a href = "/project/info/{$project.url}" class="btn btn-default width100"><span class="fa fa-info-circle"></span> {$lang.project.list.show_more} <span class="fa fa-angle-double-right"></span></a>
                              <a href = "/project/text/{$project.url}" class="btn btn-primary width100"><span class="fa fa-align-justify"></span> {$lang.project.list.show_text} <span class="fa fa-angle-double-right"></span></a>
                          </div>
                          
                          <div class="col-md-14 ">
                              <div class="col-md-24 col-xs-24 justify"><span>{$project.description}</span></div>
          
                              <div class="col-md-12 col-xs-24 padding-top-10" >
                                  <span>{$lang.project.list.translate_to}: {' | '|implode:$project.translateTo}</span>
                              </div>
                          </div>
                         
                          <div class="col-md-7  text-right hidden-xs hidden-sm">
                              <div class="col-md-24 col-lg-12 margin-0 padding-0">
                                  <a href = "/project/info/{$project.url}" class="btn btn-default width100"><span class="fa fa-info-circle"></span> {$lang.project.list.show_more} <span class="fa fa-angle-double-right"></span></a>
                              </div>
                              <div class="col-md-24 col-lg-12 margin-0 padding-0">
                                  <a href = "/project/text/{$project.url}" class="btn btn-primary width100"><span class="fa fa-align-justify"></span> {$lang.project.list.show_text} <span class="fa fa-angle-double-right"></span></a>
                              </div>
                          </div>
                    </div>
                    
                    *}
                 </div>
            </div>
            <hr />

            {foreachelse}
                <p>{$lang.project.list.no_project}</p>
                {/foreach}
                   
                </div>