
{assign idCol 1}
{assign origCol 10}
{assign suggCol 13}
{if empty($url.3)}
    {append var='url' value=$project.translateTo.0->LANG_ID index='3'}
{/if}
{if empty($url.4)}
    {append var='url' value='non-approved' index='4'}
{/if}

{if $user}
    {literal} 
        <script type="text/javascript">


            function disableVote(params) {
                console.log(JSON.stringify(params));
                var transId = params.idt;
                var type = params.type;
                var count = params.count;
                $(".like-button" + transId).attr("disabled", true);
                $(".dislike-button" + transId).attr("disabled", true);
                $(".like-button" + transId).removeClass("ajax");
                $(".dislike-button" + transId).removeClass("ajax");
                $("#" + type + transId).html(count + 1);
                console.log("disableVote");
            }

            function addText(params) {
                $("#form" + params.idt).remove();
                $("#new-added-text" + params.idt).html("<span class='fa fa-edit'></span> <a class='xeditable' id='changeOwnSuggestion' data-type='textarea' data-pk='" + newidt + "' data-url='/ajax/project-text' data-mode='inline' data-onblur='submit' data-title='Změna textu'>" + params.text + "</a>");
                $('.xeditable').editable();
            }

            $(document).ready(function () {

                $(".ajax").click(function () {//hlasování pro text
                    var ido = $(this).data('ido') ? $(this).data('ido') : "";
                    var idt = $(this).data('idt') ? $(this).data('idt') : "";
                    var ajaxData = {"ido": ido, "idt": idt, "switch": $(this).data('switch')};
                    var vars = $(this).data('vars') ? $(this).data('vars').split(" ") : 0;
                    var vals = $(this).data('vals') ? $(this).data('vals').split(" ") : 0;
                    var fcetrue = $(this).data('fcet') ? $(this).data('fcet').split(" ") : 0;
                    var fcefalse = $(this).data('fcef') ? $(this).data('fcef').split(" ") : 0;
                    var fceall = $(this).data('fce') ? $(this).data('fce').split(" ") : 0;
                    console.log("fcet " + $(this).data('fcet') + " - " + fcetrue);
                    for (var i = 0; i < vars.length; i++) {
                        ajaxData[vars[i]] = $(this).data(vars[i]);
                    }
                    for (var i = 0; i < vals.length; i++) {
                        ajaxData[vals[i]] = $(this).data(vals[i]);
                    }
                    console.log(JSON.stringify(ajaxData));
                    $.ajax({
                        type: "post",
                        url: "/ajax/project-text",
                        data: ajaxData,
                        success: function (html) {
                            if (html) {//pokud je v tom nějaký text
                                console.log("html");
                                Messenger().run;
                                if (html.match("^success:")) {//pokud to není úspěšný text                             console.log(html);
                                    Messenger().post({
                                        message: html.substr(8),
                                        type: 'success',
                                        showCloseButton: true
                                    });
                                    for (var i = 0; i < fcetrue.length; i++) {
                                        console.log(fcetrue[i]);
                                        window[fcetrue[i]](ajaxData);
                                    }
                                } else if (html.match("^error:")) {
                                    console.log(html);
                                    Messenger().post({
                                        message: html.substr(6),
                                        type: 'error',
                                        showCloseButton: true
                                    });
                                    for (var i = 0; i < fcefalse.length; i++) {
                                        console.log(fcefalse[i]);
                                        window[fcefalse[i]](ajaxData);
                                    }

                                } else {
                                    console.log("fail:" + html);
                                }
                            }

                            for (var i = 0; i < fceall.length; i++) {
                                    console.log(fceall[i]);
                                    window[fceall[i]](ajaxData);
                                }

                            }
                        });
                    });

                    ////////////////////////////////////////////////////////////////////////////////
                    
                $(".add-text").click(function () { // přidávání návrhu textu

                    var id = $(this).data('ido');
                    var newText = ".new-text-val" + id;
                    var langId = $(this).data('lang');
                    var vocabulary = $(newText).data('vocabulary');
                    var text = $(newText).val();
                    if (!text || (text == "NULL" && vocabulary == true)) {
                        Messenger().run;
                        Messenger().post({
                            message: "Musíš vyplnit textové pole.",
                            type: 'error',
                            showCloseButton: true});
                    } else {

                        var dataString = {"ido": id, "text": text, "switch": "addText", "langId": langId, "vocabulary": vocabulary};
                        console.log(JSON.stringify(dataString));

                        $.ajax({
                            type: "post",
                            url: "/ajax/project-text",
                            data: dataString,
                            success: function (html) {
                                console.log(html);
                                if (html.match("^id:")) {//pokud to není úspěšný text
                                    if (vocabulary == true) {
                                        console.log("select text");
                                        var text = $(newText + " option:selected").text();
                                    } else {
                                        var text = $(newText).val();
                                        console.log("no change" + text);
                                    }
                                    $("#form" + id).remove();
                                    $("#new-added-text" + id).html("<span class='fa fa-edit'></span> <a class='xeditable' id='changeOwnSuggestion' data-type='textarea' data-pk='" + html.substr(3) + "' data-url='/ajax/project-text' data-mode='inline' data-onblur='submit' data-title='{/literal}{$lang.project.text.change}{literal}'>" + text + "</a>");
                                    $('.xeditable').editable();
                                } else {
                                    Messenger().run;
                                    Messenger().post({
                                        message: html,
                                        type: 'error',
                                        showCloseButton: true
                                    });
                                }
                            }
                        });
                        // END AJAX */
                    }
                });

                $(".vocabulary")
                        .change(function () {
                            var val = $(this).val();
                            var ido = $(this).data('ido');
                            console.log(val);
                            console.log(ido);
                            if (val == "OWN") {
                                console.log("lol");
                                $(".vocabulary" + ido).addClass("hidden");
                                $(".new-text" + ido).removeClass("hidden");
                                $(".back-vocabulary" + ido).removeClass("hidden");
                                $(".new-text" + ido).addClass("new-text-val" + ido);
                                $(".vocabulary" + ido).removeClass("new-text-val" + ido);

                            }
                        })
                        .change();
                $(".back-vocabulary").click(function () {
                    var ido = $(this).data('ido');
                    $(".vocabulary" + ido).removeClass("hidden");
                    $(".new-text" + ido).addClass("hidden");
                    $(".back-vocabulary" + ido).addClass("hidden");
                    $(".vocabulary" + ido).val('NULL');
                    $(".new-text" + ido).removeClass("new-text-val" + ido);
                    $(".vocabulary" + ido).addClass("new-text-val" + ido);

                });
        {/literal} 

        {if $user.status == "admin" or in_array($user.id, $allowedToProcess)}
            {literal} 

                $(".process-text").click(function () {//schvalování/zamítnutí textu
                    var idt = $(this).data('idt');
                    var ido = $(this).data('ido');
                    var type = $(this).data('type');
                    var newText = "#processed-text" + idt;
                    var text = $(newText).val();
                    if (!text) {
                        Messenger().run;
                        Messenger().post({
                            message: "{/literal}{$project.text.must_fill}{literal}",
                            type: 'error',
                            showCloseButton: true
                        });
                    } else {
                        var dataString = {"idt": idt, "ido": ido, "text": text, "switch": "processText", "type": type};
                        console.log(JSON.stringify(dataString));
                        $.ajax({
                            type: "post",
                            url: "/ajax/project-text",
                            data: dataString, success: function (html) {
                                if (html) {//pokud je v tom nějaký text
                                    console.log(html);
                                    Messenger().post({
                                        message: html,
                                        type: 'success',
                                        showCloseButton: true
                                    });
                                    $(".text-row-" + ido).remove();
                                }
                            }
                        });
                    }
                });
                //*/

            {/literal} 


        {/if}
//*/


            }); //document ready END

    </script>

{/if}

<div id="debugBox" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nahlásit špatný překlad</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="POST" action="{$smarty.server.REQUEST_URI}">
                    <input type="hidden" id="idTransDebug" name="tid" >
                    <input type='hidden' class="lang-id" value='{$url.3}'>

                    Původní text:<br />
                    <span id="debugOriginalText"></span>
                    <textarea class="form-control debug-new-translation" placeholder="{$lang.project.text.new_trans}"></textarea>
                    <textarea class="form-control debug-description" placeholder="{$lang.project.text.new_trans_reason}"></textarea>
                    <br><br>
                    <button type="button" class="btn btn-default center-block width100 add-debug" data-lang-id="{$url.3}"  ><span class="fa fa-send"></span> {$lang.form.send}</button>

                </form>

            </div>
        </div>

    </div>
</div>



<div class="padding-left-20 padding-right-20">
    <div class="row">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-btn ">
                    <span class="h2 padding-right-20">{$project.name}</span> 

                </div>
            </div>
            {assign filter ["all" => {$lang.project.text.filter.all}, "approved" => {$lang.project.text.filter.approved}, "non-approved" => {$lang.project.text.filter.non_approved}, "suggestion" => {$lang.project.text.filter.suggestion}, "non-suggestion" => {$lang.project.text.filter.non_suggestion}, "debug" => {$lang.project.text.filter.debug}]}

            <div class="btn-group btn-group-justified width100" role="group" aria-label="">
                {foreach from=$filter  key=key item=name}
                    {if $key == $url.4}
                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$key}/1" class="btn btn-info btn-sm center-block">{$name}</a>
                    {else}
                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$key}/1" class="btn btn-default btn-sm center-block">{$name}</a>
                    {/if}
                {/foreach}
            </div>

        </div>

        {if $texts}
            <div class="row lichy vcenter" >
                <div class="col-md-{$idCol} bold">{$lang.project.text.id}</div>{*sloupec s web ID*}
                <div class="col-md-{$origCol-1} bold">{$lang.project.text.original_text}</div>{*sloupec s originálem*}
                <div class="col-md-{$suggCol} bold">
                    <select name="selectlang" id="change-lang-project-translate" class="form-control">
                        {foreach from=$project.translateTo item=language}
                            {if $language->LANG_ID == $url.3}
                                <option value="{$language->LANG_ID}" selected>{$language->LANG}</option>
                            {else}
                                <option value="{$language->LANG_ID}">{$language->LANG}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>{*sloupec s návrhy*}
            </div>
            {foreach from=$texts item=text}
                {assign canAddText "TRUE"}
                {if !$user}
                    <div class="row lichy vcenter margin-bottom-and-up-10px" >
                        <div class="col-md-{$idCol}">{$text.id_web}</div>{*sloupec s web ID*}
                        <div class="col-md-{$origCol}">{*sloupec s originálem*}
                            <small>
                                <i style="word-break: break-all;">{$text.description}</i>
                            </small><br />
                            <b>{$text.text}</b>
                        </div>
                        <div class="col-md-{$suggCol}">{*sloupec s návrhy*}
                            {foreach from=$text.suggestions item=suggestion}
                                <div class="col-md-15">
                                    {$suggestion.text}
                                </div>
                                <div class="col-md-9 margin-bottom-2 margin-top-2"> 
                                    <div class="btn-group btn-group-justified width100" role="group" aria-label="">
                                        <span class="btn btn-success btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="{$lang.project.text.you_voted}">
                                            <i class="fa fa-thumbs-up fa-flip-horizontal"></i> {$suggestion.like|@count}</span>
                                        <span class="btn btn-danger btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="{$lang.project.text.you_voted}">
                                            <i class="fa fa-thumbs-up fa-rotate-180"></i> {$suggestion.dislike|@count}
                                        </span>
                                    </div>
                                </div>
                            {/foreach}
                        </div>

                    </div>
                {else}
                    <div class="row lichy vcenter margin-bottom-10 margin-top-10 text-row-{$text.id}" >
                        <div class="col-md-1">{$text.id_web}</div>{*sloupec s web ID*}
                        <div class="col-md-{$origCol}">{*sloupec s originálem*}
                            <b>{$text.text}</b><br />
                            <small>
                                <i style="word-break: break-all;">{$text.description}</i>
                            </small>
                        </div>
                        <div class="col-md-{$suggCol}">{*sloupec s návrhy*}
                            {foreach from=$text.suggestions item=suggestion}
                                {if $suggestion.author == $user.nick}
                                    {assign canAddText "FALSE"}
                                {/if}

                                <form method="POST" action="">
                                    <div class="row">
                                        <input type="hidden" name="id_o" value="{$text.id}">
                                        <input type="hidden" name="id_t" value="{$suggestion.id}">
                                        <div class="col-md-16"> 

                                            {if ($user.status == "admin" or in_array($user.id, $allowedToProcess))}{*zpracování návrhů (schvalování/zamítání)*}
                                                    <div class="input-group {if $suggestion.status == "debug"} has-warning{/if}">
                                                        <span class="input-group-addon btn btn-success process-text" data-type="aprove" data-idt="{$suggestion.id}" data-ido="{$text.id}" data-lang-id="{$url.3}"   data-toggle="tooltip" data-placement="left" title="{$lang.project.text.aprove_trans}"><i class="fa fa-check"></i></span>
                                                        <textarea class="form-control suggestion{$suggestion.id} " name="trans" id="processed-text{$suggestion.id}">{$suggestion.text}</textarea>
                                                        <span class="input-group-addon btn btn-danger process-text" data-type="delete" data-idt="{$suggestion.id}" data-ido="{$text.id}" data-lang-id="{$url.3}"  data-toggle="tooltip" data-placement="right" title="{$lang.project.text.delete_trans}"><i class="fa fa-times"></i></span>
                                                    </div>
                                                    {$suggestion.author} 
                                                {elseif $suggestion.author == $user.nick and $suggestion.like|@count == 0 and $suggestion.dislike|@count == 0 }

                                                    <span class='fa fa-edit'></span> 
                                                    <a class='xeditable' id='changeOwnSuggestion' data-type='textarea' data-pk='{$suggestion.id}' data-url='/ajax/project-text' data-mode='inline' data-onblur='submit' data-title='Změna textu'>{$suggestion.text}</a>
                                                {else}
                                                    <p class="suggestion{$suggestion.id}">{$suggestion.text}</p>

                                                {/if}
                                                <p class="description{$suggestion.id} italic h6"><span class='margin-left-20'>{$suggestion.description}</span></p>
                                            </div>
                                            <div class="col-md-8">
                                                {if !in_array($user.nick, $suggestion.like) and !in_array($user.nick, $suggestion.dislike) and $suggestion.author !== $user.nick} {* user co nehlasoval nebo není majitelem *}
                                                        <div class="col-md-24 margin-bottom-2 margin-top-2"> 
                                                            <div class="btn-group btn-group-justified width100" role="group" aria-label="">
                                                                <div class="ajax btn btn-success btn-sm center-block like-button{$suggestion.id}" data-ido="{$text.id}" data-idt="{$suggestion.id}" data-vars="type count" data-fcet="disableVote"  data-type="like" data-switch="vote" data-count="{$suggestion.like|@count}" title="{$lang.project.text.like}">
                                                                    <i class="fa fa-thumbs-up fa-flip-horizontal"></i> <span id="like{$suggestion.id}">{$suggestion.like|@count}</span>
                                                                </div>
                                                                <div class="ajax btn btn-danger btn-sm center-block dislike-button{$suggestion.id}" data-ido="{$text.id}" data-idt="{$suggestion.id}" data-vars="type count"  data-fcet="disableVote" data-type="dislike" data-switch="vote" data-count="{$suggestion.dislike|@count}" title="{$lang.project.text.dislike}">
                                                                    <i class="fa fa-thumbs-up fa-rotate-180"></i> <span id="dislike{$suggestion.id}">{$suggestion.dislike|@count}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                {else}{* user co hlasoval *}
                                                    {assign canAddText "FALSE"}

                                                    <div class="col-md-24 margin-bottom-2 margin-top-2"> 

                                                        <div class="btn-group btn-group-justified width100" role="group" aria-label="">
                                                            {if $suggestion.status == "approved"}                                    
                                                                <span data-tid="{$suggestion.id}" type="button" class="debugOpenButton btn btn-warning btn-sm" data-toggle="modal" data-target="#debugBox" data-toggle="tooltip" data-placement="top" title="Nahlásit špatný překlad"><i class="fa fa-warning"></i></span>
                                                                {/if}
                                                            <span class="btn btn-success btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="{$lang.project.text.you_voted}">
                                                                <i class="fa fa-thumbs-up fa-flip-horizontal"></i> {$suggestion.like|@count}
                                                            </span>
                                                            <span class="btn btn-danger btn-sm center-block " disabled data-toggle="tooltip" data-placement="top" title="{$lang.project.text.you_voted}">
                                                                <i class="fa fa-thumbs-up fa-rotate-180"></i> {$suggestion.dislike|@count}
                                                            </span>
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>
                                        </div>
                                    </form>
                                    {foreachelse}
                                        {assign canAddText "TRUE"}
                                        {/foreach}
                                            {if $canAddText == "TRUE"}
                                                <div id="new-added-text{$text.id}"></div>
                                                <div class="row" id="form{$text.id}">
                                                    <form method="POST" action="">
                                                        <div class="input-group">
                                                            {if $text.vocabulary}
                                                                <select class="form-control vocabulary vocabulary{$text.id} new-text-val{$text.id}" data-ido="{$text.id}" data-vocabulary="true">
                                                                    <option value="NULL" >{$lang.project.text.vocabulary.select}</option>
                                                                    {foreach from=$text.vocabulary item=vocabulary}
                                                                        <option value="{$vocabulary->ID}" >{$vocabulary->TEXT}</option>
                                                                    {/foreach}
                                                                    <option value="OWN" >{$lang.project.text.vocabulary.own}</option>

                                                                </select>
                                                                <span class="input-group-addon btn btn-info back-vocabulary back-vocabulary{$text.id} hidden" data-ido="{$text.id}" data-toggle="tooltip" data-placement="left" title="{$lang.project.text.vocabulary.back}"><i class="fa fa-backward"></i></span>
                                                                <input placeholder="{$lang.project.text.add_new}" class="hidden form-control custom-control new-text{$text.id}" required="required" data-vocabulary="false">
                                                            {else}
                                                                <input placeholder="{$lang.project.text.add_new}" class="form-control custom-control new-text-val{$text.id}" required="required" data-vocabulary="false">
                                                            {/if}
                                                            <span class="input-group-addon btn btn-info add-text" data-switch="addText" data-ido="{$text.id}" data-lang="{$url.3}"  data-toggle="tooltip" data-placement="right" title="{$lang.form.send}"><i class="fa fa-send"></i></span>

                                                        </div>
                                                    </form>

                                                </div>
                                            {/if}
                                        </div>
                                        {if $text.suggestions|@count gt 1}
                                            <div class="col-md-1">
                                                {*<a href="" class="show" data-trans="" data-toggle="tooltip" data-placement="right" title="{$lang.button.show_more}"></span></a>*}
                                            </div>
                                        {else}
                                            <div class="col-md-1"></div>
                                        {/if}
                                    </div>
                                    {/if}
                                        {/foreach}


                                            <center>
                                                <div class="btn-group" role="group" aria-label="">
                                                    {if $page != 1}
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/1" class="btn btn-default"><span class="fa fa-angle-double-left"></span</a>
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$page-1}" class="btn btn-default"><span class="fa fa-angle-left"></span></a>
                                                        {/if}
                                                        {if $page-2 >= 1}
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$page-2}" class="btn btn-default">{$page-2}</a>
                                                    {/if}
                                                    {if $page-1 >= 1}
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$page-1}" class="btn btn-default">{$page-1}</a>
                                                    {/if}
                                                    <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$page}" class="btn btn-info" disabled>{$page}</a>
                                                    {if $page+1 <= $maxPage}
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$page+1}" class="btn btn-default">{$page+1}</a>
                                                    {/if}
                                                    {if $page+2 <= $maxPage}
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$page+2}" class="btn btn-default">{$page+2}</a>
                                                    {/if}
                                                    {if $page != $maxPage}
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$page+1}" class="btn btn-default"><span class="fa fa-angle-right"></span></a>
                                                        <a href="/{$url.0}/{$url.1}/{$url.2}/{$url.3}/{$url.4}/{$maxPage}" class="btn btn-default"><span class="fa fa-angle-double-right"></span></a>
                                                        {/if}
                                                </div>
                                            </center>
                                            {else}
                                                <p class="h2">{$lang.project.text.no_text}</p>
                                                {/if}
                                                </div>