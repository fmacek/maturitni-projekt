<?php

/**
 * Functions
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
/*
 * =============================================================================
 *                         Generátor hashů
 * Výstup: String
 * =============================================================================
 */
function lang($key = NULL) {
    global $lang;
    if (is_null($key)) {
        return $lang;
    } else {
        return $key;
    }
}

function generateHash($type = "long", $param1 = "", $param2 = "", $param3 = "", $param4 = "") {
    switch ($type) {
        case "short":
            return md5($param1 . $param2 . $param3) . md5(md5($param2 . $param4) . "super_text");
            break;

        case "long":
            return sha1($param4 . $param2 . $param2) . md5(md5($param1) . "qwertzuiop" . $param3);
            break;

        case "super-long":
            return sha1($param2 . $param3 . $param1) . sha1(md5($param1) . $param4 . "123-789-456");
            break;

        default:
            return NULL;
            break;
    }
}

/*
 * =============================================================================
 *                       Převedení stojového data na lidské
 * Vstup: String (YYYY-MM-DD)
 * Výstup: String (DD. MM. YYYY)
 * =============================================================================
 */

function convertDatetime($date) {
    $year = substr($date, 0, 4);
    $month = substr($date, 5, 2);
    $day = substr($date, 8, 2);
    $time = substr($date, -8);

    $humanReadable = "$day. $month. $year $time";
    return $humanReadable;
}

/*
 * =============================================================================
 *                               Výpočet věku
 *  Vstup: String (YYYY-MM-DD)
 *  Výstup: Int 
 * =============================================================================
 */

function age($date) {
//převedení do formátu mm/dd/yyyy, protože jsme tu funkci sehnal takhle a nevim jak to převést
    $birthDate = substr($date, 5, 2) . "/" . substr($date, -2) . "/" . substr($date, 0, 4);
//rozkouskování
    $birthDate = explode("/", $birthDate);
//záhadné dopočítání
    if ($date != "0000-00-00") {
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
    } else {
        $age = "<i>nevyplněno</span>";
    }
    return $age;
}

function humanDate($date, $format) {
    $newDate = date($format, strtotime($date));
}

/*
 * =============================================================================
 *                                  Debugging
 * Výstup: String
 * =============================================================================
 */

function debug($debug = "", $name = "Debug", $backtrace = FALSE, $class = "") {

    echo "<H2>$name</H2>";
    echo "<pre class='$class'>";


    var_dump($debug);
    if (!empty($backtrace) or $backtrace) {
        echo "<hr>";

        echo "Backtrace<br />";
        foreach (debug_backtrace() as $key => $value) {
            echo "<h3><strong>#$key</strong> - " . $value['file'] . ":" . $value["line"] . " - " . $value["function"] . "</h3>";
            if ($value["args"] and $backtrace == "args") {
                echo "Args:";
                var_dump($value["args"]);
            }
        }
    }
    echo "</pre>";
//*/
}

/*
 * =============================================================================
 *                              Odesílání mailů
 * Výstup: NULL
 * =============================================================================
 */

function sendMail($to, $head, $text) {

    $message = wordwrap($text, 70, "\r\n");
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'From: DMP <info@filipmacek.cz>' . "\r\n";
    mail($to, $head, $message, $headers);
}

/*
 * =============================================================================
 *                          Generátor lipsum
 * Výstup: String
 * =============================================================================
 */

function lipsum($p = 1) {
    $lipsum = array("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id eleifend mauris. Quisque tempor justo ut eleifend malesuada. Mauris ac auctor nibh. In eleifend magna et ex varius, sit amet imperdiet nulla commodo. Aliquam dignissim sapien velit, ut rhoncus nulla mollis eu. Suspendisse vel neque vitae purus pulvinar volutpat. Nam mollis porttitor cursus. Integer sodales bibendum urna, sed blandit ipsum sagittis sed. Proin tempus magna ut lorem malesuada, non rhoncus ante iaculis. Cras eget ipsum mauris. Cras urna diam, accumsan at orci ac, accumsan interdum nisl. Sed et felis dolor. Nunc rhoncus sollicitudin mi, ac pulvinar lacus tincidunt sed.",
        "Nullam diam eros, porttitor tincidunt massa quis, dignissim interdum sem. Morbi rhoncus arcu non erat sagittis finibus. Sed lobortis sit amet ex sed pretium. Maecenas risus odio, scelerisque id lorem nec, volutpat congue eros. Ut egestas pharetra nibh, a elementum justo. Aenean facilisis maximus consectetur. Vestibulum molestie odio a leo imperdiet euismod. Maecenas a eros eleifend, rutrum ipsum quis, eleifend quam. Morbi nec maximus ante. Fusce mi augue, auctor eget enim at, placerat convallis tellus. Maecenas auctor lorem at nisl aliquet, nec sodales magna venenatis. Fusce at faucibus risus. Cras congue, dolor tincidunt volutpat interdum, est magna egestas leo, nec mollis tellus metus efficitur ipsum. Aenean gravida leo nec dolor pharetra, nec vulputate nisl pellentesque.",
        "Duis sit amet efficitur purus, in pretium mauris. Quisque pharetra arcu metus, at ultricies tortor aliquet eu. Nunc pretium eu felis sit amet viverra. Donec tincidunt ac sem in dapibus. Nulla mattis velit pharetra odio posuere maximus. Vestibulum iaculis enim ex, eget posuere nibh tempus ut. Integer cursus egestas metus. Duis in tristique mi. Ut malesuada, erat nec congue maximus, neque massa mollis mauris, ut molestie nibh sem ut eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas quis neque sed sapien blandit gravida. Etiam in sapien ultricies, dapibus libero non, tincidunt justo.",
        "Suspendisse potenti. Pellentesque in magna dignissim, sollicitudin nulla nec, aliquam risus. Praesent mi nulla, convallis quis dui id, laoreet auctor nibh. Cras id posuere lacus, vel blandit ligula. In id nibh non leo viverra egestas. Aenean consequat ultricies tellus id fermentum. Vivamus tortor nulla, luctus nec ultricies vel, molestie eu justo.",
        "Etiam at lacinia diam. Nulla commodo nulla sed arcu posuere venenatis. Nulla lacinia efficitur euismod. Phasellus a velit magna. Phasellus risus orci, porta sed augue sit amet, sollicitudin ullamcorper sapien. Suspendisse sagittis turpis tristique augue eleifend, at faucibus nibh rutrum. Sed vitae egestas turpis, bibendum dictum lectus. Aenean vitae malesuada risus. Etiam consectetur ligula nec tortor congue, at blandit nunc faucibus.");
    if (is_numeric($p)) {
        return $lipsum[$p];
    } else {
        $string = "";
        foreach ($lipsum as $row) {
            $string .= "<p>$row</p>";
        }
        return $string;
    }
}

/*
 * =============================================================================
 *                          Text převést na URI
 * Výstup: String
 * =============================================================================
 */

function textToURI($title) {
    static $convertTable = array(
        'á' => 'a', 'Á' => 'A', 'ä' => 'a', 'Ä' => 'A', 'č' => 'c',
        'Č' => 'C', 'ď' => 'd', 'Ď' => 'D', 'é' => 'e', 'É' => 'E',
        'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'í' => 'i',
        'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ľ' => 'l', 'Ľ' => 'L',
        'ĺ' => 'l', 'Ĺ' => 'L', 'ň' => 'n', 'Ň' => 'N', 'ń' => 'n',
        'Ń' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O',
        'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R', 'š' => 's',
        'Š' => 'S', 'ś' => 's', 'Ś' => 'S', 'ť' => 't', 'Ť' => 'T',
        'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u',
        'Ü' => 'U', 'ý' => 'y', 'Ý' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y',
        'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z',
    );
    $title = strtolower(strtr($title, $convertTable));
    $title = preg_replace('/[^a-zA-Z0-9]+/u', '-', $title);

    do {
        $title = str_replace('--', '-', $title);
        $old = $title;
    } while ($old != $title);

    $title = trim($title, '-');

    return $title;
}
