<?php

session_start();

/**
 * Autoloader of objects
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
function autoloadFunction($class) {
    $file = NULL;
    $classArray = explode("\\", $class);
    $classname = end($classArray);
    if (file_exists("controller/$classname.php")) {//Clasic controller
        $file = "controller/" . $classname . ".php";
    } elseif (file_exists("model/$classname.php")) {//Clasic model
        $file = "model/" . $classname . ".php";
    } elseif (file_exists("enum/$classname.php")) {//Enum list
        $file = "enum/" . $classname . ".php";
    } elseif (is_dir("inc/libs/$classname")) {//Libary
        if (file_exists("inc/libs/$classname/$classname-autoloader.php")) {//next autoload (for ex. next internal autoload for lib. )
            $file = "inc/libs/$classname/$classname-autoloader.php";
        } elseif (file_exists("inc/libs/$classname/$classname.php")) {//clasic class name 
            $file = "inc/libs/$classname/$classname.php";
        }
    }
    if ($file) {
        require $file;
    }
}

spl_autoload_register("autoloadFunction");
