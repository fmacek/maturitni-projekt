(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
ga('create', 'UA-54606218-1', 'auto');
ga('send', 'pageview');
$(document).ready(function () {
    $('#changelangselect').change(function ()
    {
        $('#changelang').submit();
    })
    $('.selectpicker').selectpicker();
    $('.xeditable').editable();
    $('[data-toggle="tooltip"]').tooltip();

    $('#mobile-menu').on('change', function () {
        window.location.href = this.value;
    })
   
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 120) {
            $('.my-menu').removeClass('hidden');
        } else {
            $('.my-menu').addClass('hidden');
        }
    });

    /*
     $(window).on("scroll", function () {
     var xclass = "fixed top-0 z-index-800 center-block";
     var scroll = $(window).scrollTop();
     if (scroll > 120) {
     $(".header-bottom").addClass(xclass);
     } else {
     $(".header-bottom").removeClass(xclass);
     }
     });
     //*/
});
        