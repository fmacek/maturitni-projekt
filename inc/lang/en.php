<?php

$lang["html"]["lang"] = "en"; //
$lang["html"]["title"] = "Comunites translations"; //
$lang["webname"] = "comunitestranslations.com"; //

$lang["meta"]["description"] = "Comunites translations - Translations of games & aps by comunity. Even your projects!"; //
$lang["meta"]["keywords"] = "translations, games translations, apps translations, comunity"; //

$lang["menu"]["home"] = "Home"; //
$lang["menu"]["projects"] = "Projects"; //
$lang["menu"]["princip"] = "Principe"; //
$lang["menu"]["statictics"] = "Statisitics"; //
$lang["menu"]["statictics_users"] = "Users"; //
$lang["menu"]["statictics_users_title"] = "Statistics/users"; //
$lang["menu"]["statictics_projects"] = "Projects"; //
$lang["menu"]["statictics_projects_title"] = "Statistics/projects"; //
$lang["menu"]["contact"] = "Contact"; //

$lang["account"]["account"] = "Account"; //
$lang["account"]["login"] = "Login"; //
$lang["account"]["logout"] = "Logout"; //
$lang["account"]["register"] = "Register"; //

$lang["user"]["name"] = "Name"; //
$lang["user"]["nick"] = "Nick"; //
$lang["user"]["email"] = "Email"; //
$lang["user"]["pass"] = "Password"; //
$lang["user"]["photo"] = "User photo"; //
$lang["user"]["photo_user"] = "Photo of user"; //
$lang["user"]["your_photo"] = "Your photo"; //
$lang["user"]["rank"] = "Rank"; //
$lang["user"]["birth"] = "Date of birth"; //
$lang["user"]["region"] = "Kraj bydliště"; //
$lang["user"]["about_me"] = "About me"; //

$lang["user"]["settings"]["head"] = "Change user data"; //
$lang["user"]["settings"]["email"]["description"] = "Your registred email. For login purposes. For other user is not published."; //
$lang["user"]["settings"]["nick"]["description"] = "Your nick, the name of your acconut."; //
$lang["user"]["settings"]["name"]["description"] = "Your name. Other users can show it in your profile."; //
$lang["user"]["settings"]["about_me"]["description"] = "Text, witch will be shown in your profile. It is on you mind waht you will say."; //
$lang["user"]["settings"]["photo"]["upload_new"] = "Upload new photo"; //
$lang["user"]["settings"]["photo"]["description"] = "Your profile photo."; //
$lang["user"]["settings"]["photo"]["delete"] = "Delete photo"; //
$lang["user"]["settings"]["pass"]["text1"] = "Write old pass (check for unauthorized change)"; //
$lang["user"]["settings"]["pass"]["text2"] = "Your new pass."; //
$lang["user"]["settings"]["pass"]["text3"] = "Your new pass again (check for missclicks)."; //
$lang["user"]["settings"]["pass"]["old_pass"] = "Old pass"; //
$lang["user"]["settings"]["pass"]["new_pass"] = "New pass"; //
$lang["user"]["settings"]["pass"]["new_pass_again"] = "New pass again"; //
$lang["user"]["settings"]["pass"]["button"] = "Change pass"; //

$lang["user"]["register"]["text"]["main"] = "Here is a register formular, contains the most important data for registration. After registration we will send you a validation email for activation. Then you can set another data like a:"; //
$lang["user"]["register"]["text"]["name"] = "Name"; //
$lang["user"]["register"]["text"]["birth"] = "Date of birth"; //
$lang["user"]["register"]["text"]["region"] = "Region"; //
$lang["user"]["register"]["text"]["photo"] = "User photo"; //
$lang["user"]["register"]["text"]["thanks"] = "Thank you for choosing to use our site fully and that approached you so much that you want to create an account."; //
$lang["user"]["register"]["register_via"]["google"] = "Login via Google+"; //
$lang["user"]["register"]["register_via"]["facebook"] = "Login via Facebook"; //
$lang["user"]["register"]["register_via"]["twitter"] = "Login via Twitter"; //
$lang["user"]["register"]["register_form"] = "Registration form"; //
$lang["user"]["register"]["pass_again"] = "Password again"; //
$lang["user"]["register"]["button"] = "Register"; //

$lang["user_card"]["my_projects"] = "My projects"; //
$lang["user_card"]["show_profile"] = "Show profile"; //
$lang["user_card"]["settings"] = "Settings"; //
$lang["user_card"]["vocabulary"] = "Vocabulary"; //

$lang["login_form"]["email"] = "Email"; //
$lang["login_form"]["pass"] = "Password"; //
$lang["login_form"]["send"] = "Login"; //

$lang["project"]["list"]["head"] = "Projects"; //
$lang["project"]["list"]["filter"] = "Filter"; //
$lang["project"]["list"]["source_lang"] = "Source language"; //
$lang["project"]["list"]["to_lang"] = "Target language"; //
$lang["project"]["list"]["button_filter"] = "Filter"; //
$lang["project"]["list"]["show_more"] = "Show more"; //
$lang["project"]["list"]["show_text"] = "Show texts"; //
$lang["project"]["list"]["translate_to"] = "Translate to"; //
$lang["project"]["list"]["image"] = "Image of project"; //
$lang["project"]["list"]["no_project"] = "No project found."; //
$lang["project"]["list"]["img"] = "Picture of project"; //
$lang["project"]["list"]["new"] = "Create new project"; //

$lang["project"]["text"]["id"] = "ID"; //
$lang["project"]["text"]["original_text"] = "Original text"; //
$lang["project"]["text"]["translated_text"] = "Translated text"; //
$lang["project"]["text"]["aprove_trans"] = "Aprove text"; //
$lang["project"]["text"]["delete_trans"] = "Decline text"; //
$lang["project"]["text"]["like"] = "Text is correct"; //
$lang["project"]["text"]["dislike"] = "Text is wrong"; //
$lang["project"]["text"]["you_voted"] = "You allready voted for this text"; //
$lang["project"]["text"]["add_new"] = "Suggest a new translation..."; //
$lang["project"]["text"]["add_new"] = "Text change"; //
$lang["project"]["text"]["must_fill"] = "Must fill text field."; //
$lang["project"]["text"]["new_trans"] = "New translate"; //
$lang["project"]["text"]["new_trans_reason"] = "Reason to change (context, missclick, another word,...)"; //
$lang["project"]["text"]["no_text"] = "No text founded."; //

$lang["project"]["text"]["filter"]["all"] = "All texts"; //
$lang["project"]["text"]["filter"]["approved"] = "Approved texts"; //
$lang["project"]["text"]["filter"]["non_approved"] = "Not approved texts"; //
$lang["project"]["text"]["filter"]["suggestion"] = "With suggestion"; //
$lang["project"]["text"]["filter"]["non_suggestion"] = "Without suggestion"; //
$lang["project"]["text"]["filter"]["debug"] = "Debug"; //

$lang["project"]["text"]["vocabulary"]["select"] = "Choose vocabulary translate"; //
$lang["project"]["text"]["vocabulary"]["own"] = "--- New own translate ---"; //
$lang["project"]["text"]["vocabulary"]["back"] = "Back to vocabulary"; //

$lang["project"]["info"]["user_photo"]["owner"] = "Profile photo of owner"; //
$lang["project"]["info"]["user_photo"]["manager"] = "Profile photo of manager"; //
$lang["project"]["info"]["owner"] = "Owner"; //
$lang["project"]["info"]["managers"] = "Managers"; //
$lang["project"]["info"]["about"] = "About project"; //
$lang["project"]["info"]["downloadable"] = "Download translations"; //
$lang["project"]["info"]["trans"] = "Translate manager"; //
$lang["project"]["info"]["type"] = "Type"; //
$lang["project"]["info"]["version"] = "Version"; //
$lang["project"]["info"]["des"] = "Description"; //
$lang["project"]["info"]["date"] = "Date"; //
$lang["project"]["info"]["download"] = "Download"; //
$lang["project"]["info"]["no_file"] = "No file in this language."; //
$lang["project"]["info"]["preview"] = "Preview of actual translate"; //
$lang["project"]["info"]["create"] = "Create new translate"; //
$lang["project"]["info"]["new_export"] = "New export"; //
$lang["project"]["info"]["export_lang"] = "Translated langugage"; //
$lang["project"]["info"]["export_version"] = "Vesion of translate"; //
$lang["project"]["info"]["export_des"] = "Description file translation"; //
$lang["project"]["info"]["export_public"] = "Public"; //
$lang["project"]["info"]["lang_prog"] = "Languages progress"; //

$lang["project"]["settings"]["name"] = "Project name"; //
$lang["project"]["settings"]["add_new_manager"] = "Add new manager of project"; //
$lang["project"]["settings"]["upload_new_photo"] = "Upload new photo"; //
$lang["project"]["settings"]["project_photo"] = "Image of project."; //
$lang["project"]["settings"]["owner"] = "Owner"; //
$lang["project"]["settings"]["managers"] = "Managers"; //
$lang["project"]["settings"]["lang_progress"] = "Progress of each language"; //
$lang["project"]["settings"]["description"] = "Description"; //
$lang["project"]["settings"]["project_description"] = "Desription of project"; //
$lang["project"]["settings"]["new_lang"] = "New langugage to translate"; //

$lang["project"]["import"]["new"]["head"] = "New project"; //
$lang["project"]["import"]["new"]["name"]["text"] = "Name of project"; //
$lang["project"]["import"]["new"]["name"]["description"] = "Name of you project. With this name will be presented. Name should not be long."; //
$lang["project"]["import"]["new"]["description"]["text"] = "Desription of project"; //
$lang["project"]["import"]["new"]["description"]["description"] = "Description of project, For details."; //
$lang["project"]["import"]["new"]["source_file"]["text"] = "Source file"; //
$lang["project"]["import"]["new"]["source_file"]["description"] = "File with text to translate. It is supported only files with specific format. For detail info please read a <a href='/article/import_documentation'>documentation</a>."; //
$lang["project"]["import"]["new"]["source_lang"]["text"] = "Source lang"; //
$lang["project"]["import"]["new"]["source_lang"]["description"] = "In witch language is source file."; //
$lang["project"]["import"]["new"]["to_lang"]["text"] = "Target language(s)"; //
$lang["project"]["import"]["new"]["to_lang"]["description"] = "You set only language what to translate, becouse we wan't to waste the time to translate to unused languages."; //

$lang["project"]["import"]["exist"]["head"] = "Update project"; //

$lang["project"]["favorite_project"]["add"] = "Add to favorite"; //
$lang["project"]["favorite_project"]["remove"] = "Remove from favorite"; //

$lang["error"]["head"] = "Error 404"; //Head of 404 error page
$lang["error"]["text"] = "Page not found, pleas check the URL"; //Text of 404 error page

$lang["form"]["send"] = "Send"; //
$lang["form"]["select_all"] = "Select all"; //

$lang["button"]["settings"] = "Settings"; //
$lang["button"]["show_more"] = "Show more"; //

$lang["home"]["top5project"] = "Top 5 projects"; //
$lang["home"]["top5user"] = "Top 5 users"; //
$lang["home"]["stats"] = "Statistics"; //

$lang["home"]["project"] = "Project";//
$lang["home"]["user"] = "User";//
$lang["home"]["points"] = "Points";//

$lang["home"]["s1"] = "Project count";//
$lang["home"]["s2"] = "Text count";//
$lang["home"]["s3"] = "Approved text count";//
$lang["home"]["s4"] = "User count";//
$lang["home"]["s5"] = "Points count";//

$lang["vocabulary"]["list"] = "Recrds";//
$lang["vocabulary"]["your_trans"] = "Your translate.";//
$lang["vocabulary"]["cant_same_lang"] = "Can't make translate to same language.";//
$lang["vocabulary"]["add"] = "Add record";//
$lang["vocabulary"]["no_text"] = "No record :'(";//

$lang["lang"]["select"] = "Language selection";//