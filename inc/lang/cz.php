<?php

$lang["html"]["lang"] = "cs-cz"; //
$lang["html"]["title"] = "Dlouhodobá maturitní práce"; //
$lang["webname"] = "DMP"; //

$lang["meta"]["description"] = "DMP - dlouhodbá maturitní práce pro komunitní překlad textů."; //
$lang["meta"]["keywords"] = "překlad,komunitní překlad , překlad her, překlad aplikací, překlad zdarma"; //

$lang["menu"]["home"] = "Domů"; //
$lang["menu"]["projects"] = "Projekty"; //
$lang["menu"]["princip"] = "Princip"; //
$lang["menu"]["statictics"] = "Statistiky"; //
$lang["menu"]["statictics_users"] = "Uživatelé"; //
$lang["menu"]["statictics_users_title"] = "Statistiky/uživatelé"; //
$lang["menu"]["statictics_projects"] = "Projekty"; //
$lang["menu"]["statictics_projects_title"] = "Statistiky/hry"; //
$lang["menu"]["contact"] = "Kontakt"; //

$lang["account"]["account"] = "Účet"; //
$lang["account"]["login"] = "Přihlásit"; //
$lang["account"]["logout"] = "Odhlásit"; //
$lang["account"]["register"] = "Registrovat"; //

$lang["user"]["name"] = "Jméno"; //
$lang["user"]["nick"] = "Přezdívka"; //
$lang["user"]["email"] = "Email"; //
$lang["user"]["pass"] = "Heslo"; //
$lang["user"]["photo"] = "Profilová forografie"; //
$lang["user"]["photo_user"] = "Profilová forografie uživatele"; //
$lang["user"]["your_photo"] = "Vaše profilová fotografie"; //
$lang["user"]["rank"] = "Hodnost"; //
$lang["user"]["birth"] = "Datum narození"; //
$lang["user"]["region"] = "Kraj bydliště"; //
$lang["user"]["about_me"] = "O mně"; //

$lang["user"]["settings"]["head"] = "Změna profilových údajů"; //
$lang["user"]["settings"]["email"]["description"] = "Vaše registrovaná emailová adresa. Slouží pro oznámení a přihlašování. ostatním uživatelům se nezveřejňuje."; //
$lang["user"]["settings"]["nick"]["description"] = "Vaše přezdívka pod kterým bude účet pojmenován."; //
$lang["user"]["settings"]["name"]["description"] = "Vaše jméno. Ostatní uživatelé si jej mohou zobrazit ve Vašem profilu."; //
$lang["user"]["settings"]["about_me"]["description"] = "Text který se bude zobrazovat u profilu. Je jen na Vás co jsme napíšete."; //
$lang["user"]["settings"]["photo"]["upload_new"] = "Nahrát novou fotografii"; //
$lang["user"]["settings"]["photo"]["description"] = "Vaše profilová fotografie."; //
$lang["user"]["settings"]["photo"]["delete"] = "Smazat fotografii"; //
$lang["user"]["settings"]["pass"]["text1"] = "Staré heslou slouží jako kontrola pro neautorizovanou změnu hesla."; //
$lang["user"]["settings"]["pass"]["text2"] = "Nové heslo na váš účet."; //
$lang["user"]["settings"]["pass"]["text3"] = "Zadání znovu nového hesla je z důvodu kontroly překlepů."; //
$lang["user"]["settings"]["pass"]["old_pass"] = "Staré heslo"; //
$lang["user"]["settings"]["pass"]["new_pass"] = "Nové heslo"; //
$lang["user"]["settings"]["pass"]["new_pass_again"] = "Nové heslo znovu"; //
$lang["user"]["settings"]["pass"]["button"] = "Změnit heslo"; //

$lang["user"]["register"]["text"]["main"] = "Zde je registrační formulář, obsahující nejdůležitější údaje pro registraci. Poté co se zaregistruješ ti přijde potvrzovací email, kterým svůj účet aktivuješ. Následně si budeš moct vyplnit další údaje jako:"; //
$lang["user"]["register"]["text"]["name"] = "Jméno"; //
$lang["user"]["register"]["text"]["birth"] = "Datum narození"; //
$lang["user"]["register"]["text"]["region"] = "Kraj bydliště"; //
$lang["user"]["register"]["text"]["photo"] = "Profilový obrázek"; //
$lang["user"]["register"]["text"]["thanks"] = "Děkujeme že jste se rozhodli využívat náš web naplno a že vás oslovil natolik, že si chcete založit účet."; //
$lang["user"]["register"]["register_via"]["google"] = "Přihlásit se přes Google+"; //
$lang["user"]["register"]["register_via"]["facebook"] = "Přihlásit se přes Facebook"; //
$lang["user"]["register"]["register_via"]["twitter"] = "Přihlásit se přes Twitter"; //
$lang["user"]["register"]["register_form"] = "Registrační formulář"; //
$lang["user"]["register"]["pass_again"] = "Zopakujte heslo"; //
$lang["user"]["register"]["button"] = "Registrovat"; //

$lang["user_card"]["my_projects"] = "Moje projekty"; //
$lang["user_card"]["show_profile"] = "Zobrazit profil"; //
$lang["user_card"]["settings"] = "Nastavení"; //
$lang["user_card"]["vocabulary"] = "Slovník"; //

$lang["login_form"]["email"] = "Email"; //
$lang["login_form"]["pass"] = "Heslo"; //
$lang["login_form"]["send"] = "Přihlásit"; //

$lang["project"]["list"]["head"] = "Projekty"; //
$lang["project"]["list"]["filter"] = "Filtr"; //
$lang["project"]["list"]["source_lang"] = "Zdrojový jazyk"; //
$lang["project"]["list"]["to_lang"] = "Cílový jazyk"; //
$lang["project"]["list"]["button_filter"] = "Filtrovat"; //
$lang["project"]["list"]["show_more"] = "Zobrazit více"; //
$lang["project"]["list"]["show_text"] = "Zobrazit texty"; //
$lang["project"]["list"]["translate_to"] = "Přeložit do"; //
$lang["project"]["list"]["image"] = "Obrázek projektu"; //
$lang["project"]["list"]["no_project"] = "Nebyl nalezen žádný projekt."; //
$lang["project"]["list"]["img"] = "Obrázek projektu"; //
$lang["project"]["list"]["new"] = "Vytořit nový projekt"; //

$lang["project"]["text"]["id"] = "ID"; //
$lang["project"]["text"]["original_text"] = "Originální text"; //
$lang["project"]["text"]["translated_text"] = "Přeložený text"; //
$lang["project"]["text"]["aprove_trans"] = "Schválit text"; //
$lang["project"]["text"]["delete_trans"] = "Zamítnout text"; //
$lang["project"]["text"]["like"] = "Text je správně"; //
$lang["project"]["text"]["dislike"] = "Text je špatně"; //
$lang["project"]["text"]["you_voted"] = "Pro tento text jste již hlasoval"; //
$lang["project"]["text"]["add_new"] = "Navrhněte nový překlad..."; //
$lang["project"]["text"]["change"] = "Změna textu"; //
$lang["project"]["text"]["must_fill"] = "Musíš vyplnit textové pole."; //
$lang["project"]["text"]["new_trans"] = "Nový překlad"; //
$lang["project"]["text"]["new_trans_reason"] = "Důvod změny (jiný kontext, překlep, špatný rod,...)"; //
$lang["project"]["text"]["no_text"] = "Nebyl nalezen žádný text."; //

$lang["project"]["text"]["filter"]["all"] = "Všechny text"; //
$lang["project"]["text"]["filter"]["approved"] = "Schválené texty"; //
$lang["project"]["text"]["filter"]["non_approved"] = "Neschválené texty"; //
$lang["project"]["text"]["filter"]["suggestion"] = "S návrhy"; //
$lang["project"]["text"]["filter"]["non_suggestion"] = "Bez návrhu"; //
$lang["project"]["text"]["filter"]["debug"] = "Debug"; //

$lang["project"]["text"]["vocabulary"]["select"] = "Vyberte slovníkový překlad"; //
$lang["project"]["text"]["vocabulary"]["own"] = "--- Nový vlastní překlad ---"; //
$lang["project"]["text"]["vocabulary"]["back"] = "Zpět na slovník"; //

$lang["project"]["info"]["user_photo"]["owner"] = "Profilová fotografie majitele"; //
$lang["project"]["info"]["user_photo"]["manager"] = "Profilová fotografie správce"; //
$lang["project"]["info"]["owner"] = "Majitel"; //
$lang["project"]["info"]["managers"] = "Správci"; //
$lang["project"]["info"]["about"] = "O projektu"; //
$lang["project"]["info"]["downloadable"] = "Stáhnutelné překlady"; //
$lang["project"]["info"]["trans"] = "Správa překladů"; //
$lang["project"]["info"]["type"] = "Typ"; //
$lang["project"]["info"]["version"] = "Verze"; //
$lang["project"]["info"]["des"] = "Popis"; //
$lang["project"]["info"]["date"] = "Datum"; //
$lang["project"]["info"]["download"] = "Stáhnout"; //
$lang["project"]["info"]["no_file"] = "Žádný soubor v tomto jazyku."; //
$lang["project"]["info"]["preview"] = "Náhled aktuálního překladu"; //
$lang["project"]["info"]["create"] = "Vytvořit nový překlad"; //
$lang["project"]["info"]["new_export"] = "Nový export"; //
$lang["project"]["info"]["export_lang"] = "Překládaný jazyk"; //
$lang["project"]["info"]["export_version"] = "Verze překladu"; //
$lang["project"]["info"]["export_des"] = "Popis souboru překladu"; //
$lang["project"]["info"]["export_public"] = "Veřejný"; //
$lang["project"]["info"]["lang_prog"] = "Postup jednotlivých jazyků"; //

$lang["project"]["settings"]["name"] = "Název projektu"; //
$lang["project"]["settings"]["add_new_manager"] = "Přidat nového správce projektu"; //
$lang["project"]["settings"]["upload_new_photo"] = "Nahrát novou fotografii"; //
$lang["project"]["settings"]["project_photo"] = "Obrázek k projektu."; //
$lang["project"]["settings"]["owner"] = "Majitel"; //
$lang["project"]["settings"]["managers"] = "Správci"; //
$lang["project"]["settings"]["lang_progress"] = "Postup jednotlivých jazyků"; //
$lang["project"]["settings"]["description"] = "Popisek"; //
$lang["project"]["settings"]["project_description"] = "Popis projektu"; //
$lang["project"]["settings"]["new_lang"] = "Nový jazyk k přeložení"; //

$lang["project"]["import"]["new"]["head"] = "Nový projekt"; //
$lang["project"]["import"]["new"]["name"]["text"] = "Název projektu"; //
$lang["project"]["import"]["new"]["name"]["description"] = "Jak se váš projekt jmenuje. Pod tímto názvém bude prezentován. Text by neměl být příliš dlouhý."; //
$lang["project"]["import"]["new"]["description"]["text"] = "Popis projektu"; //
$lang["project"]["import"]["new"]["description"]["description"] = "Popis projektu, k detailnímu popisu."; //
$lang["project"]["import"]["new"]["source_file"]["text"] = "Zdrojový soubor"; //
$lang["project"]["import"]["new"]["source_file"]["description"] = "Soubor s texty k přeložení. Jsou podporované pouze určité soubory pouze s určitou syntaxí. Pro detailnější informace si přečtěte <a href='/article/import_documentation'>dokumentaci</a>."; //
$lang["project"]["import"]["new"]["source_lang"]["text"] = "Zdrojový jazyk"; //
$lang["project"]["import"]["new"]["source_lang"]["description"] = "V jakém jazykce je zdrojový soubor."; //
$lang["project"]["import"]["new"]["to_lang"]["text"] = "Cílový jazyk(y)"; //
$lang["project"]["import"]["new"]["to_lang"]["description"] = "Do jakých jazyků chcete soubor přeložit. Slouží pokud chcete překlad pouze do určitých jazyků a aby komunita nepřekládala zbytečně."; //

$lang["project"]["import"]["exist"]["head"] = "Update projektu"; //

$lang["project"]["favorite_project"]["add"] = "Přidat do oblíbených"; //
$lang["project"]["favorite_project"]["remove"] = "Odebrat z oblíbených"; //

$lang["error"]["head"] = "Error 404"; //Head of 404 error page
$lang["error"]["text"] = "Požadovaná stránka nebyla nalezena, zkontrolujte prosím URL adresu"; //Text of 404 error page

$lang["form"]["send"] = "Odeslat"; //
$lang["form"]["select_all"] = "Vybrat vše"; //

$lang["button"]["settings"] = "Nastavení"; //
$lang["button"]["show_more"] = "Zobrazit více"; //

$lang["home"]["top5project"] = "Top 5 projektů";//
$lang["home"]["top5user"] = "Top 5 uživatelů";//
$lang["home"]["stats"] = "Statistiky";//

$lang["home"]["project"] = "Projekt";//
$lang["home"]["user"] = "Uživatel";//
$lang["home"]["points"] = "Body";//

$lang["home"]["s1"] = "Počet projektů";//
$lang["home"]["s2"] = "Počet textů";//
$lang["home"]["s3"] = "Počet schválených textů";//
$lang["home"]["s4"] = "Počet uživatelů";//
$lang["home"]["s5"] = "Počet rozdaných bodů";//

$lang["vocabulary"]["list"] = "Výpis záznamů";//
$lang["vocabulary"]["your_trans"] = "Váš navrhovaný překlad.";//
$lang["vocabulary"]["cant_same_lang"] = "Nelze udělat překlad pro stejný jazyk.";//
$lang["vocabulary"]["add"] = "Přidat záznam";//
$lang["vocabulary"]["no_text"] = "Žádný textík :'(";//

$lang["lang"]["select"] = "Výběr jazyku";//