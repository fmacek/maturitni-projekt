<?php
/**
 * Definitions
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */


define("LIBS_DIR", "inc/libs/");

define("SMARTY_DIR", LIBS_DIR . "Smarty/libs/");
define("FACEBOOK_DIR", LIBS_DIR . "Facebook/src/");

require_once 'fce.php';