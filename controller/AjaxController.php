<?php

/**
 * Controller for articles
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
// Controller pro výpis článků

class AjaxController extends Controller {

    public function project_text($params) {

        $switch = (!empty($_POST["switch"])) ? $_POST["switch"] : $_POST["name"];
        $ido = (!empty($_POST["ido"])) ? $_POST["ido"] : NULL;
        $idt = (!empty($_POST["idt"])) ? $_POST["idt"] : NULL;
        switch ($switch) {
            case "vote":
                $type = $_POST["type"];
                $success = TRUE;
                if (!in_array($type, array("like", "dislike"))) {
                    echo "Neznámý typ hlasu.";
                    $success = FALSE;
                }
                if (Database::isExist("vote", "USER_ID = :userId AND TRANSLATED_TEXT_ID = :transId", array("userId" => $_SESSION["user"]["id"], "transId" => $idt))) {
                    echo "Pro tento text si již hlasoval.";
                    $success = FALSE;
                }
                if ($success) {
                    Project::vote($idt, $type);
                    Point::add(10, "vote", $idt);
                    echo "success:Tvůj hlas byl uložen.";
                }
                break;
            case "addText":
                $text = ($_POST["vocabulary"] == "false") ? $_POST["text"] : Database::getValue("translated_vocabulary", "TEXT", "ID = :id", ["id" => $_POST["text"]]);
                $langId = $_POST["langId"];

                if (!Project::addTranslation($ido, $langId, $text)) {
                    echo "Pro tento text si již navrhoval.";
                } else {
                    $id = Database::getValue("translated_text", "ID", "ORIGINAL_TEXT_ID = :oid AND TEXT = :text AND USER_ID = :uid", ["oid" => $ido, "text" => $text, "uid" => $_SESSION["user"]["id"]]);
                    echo "id:" . $id;
                    Point::add(30, "add_translate", $id);
                }
                break;
            case "processText":
                Database::update("translated_text", ["STATUS" => "active"], "ORIGINAL_TEXT_ID = :oid AND STATUS = 'approved'", ["oid" => $_POST["id_o"]]);
                if (Project::processTranslation($_POST["type"], $idt, $ido, $_POST["text"])) {
                    if ($_POST["type"] == "aprove") {
                        echo "Text byl úspěšně schválen.";
                    } elseif ($_POST["type"] == "delete") {
                        echo "Text byl úspěšně zamítnut.";
                    }
                }
                break;
            case "changeOwnSuggestion":
                Project::changeOwnSuggestion($_POST["pk"], $_POST["value"]);
                break;

            case "addDebug":
                $id = $_POST["id"];
                $text = $_POST["text"];
                $description = $_POST["description"];
                $lang = $_POST["lang"];
                Project::addDebug($id, $lang, $text, $description);

                break;

            default :

                break;
        }
        $this->rawOutput = TRUE;
    }

    public function vote($params) {
        $type = $_POST["type"];
        $transId = $_POST["transId"];
        $success = TRUE;
        if (!in_array($type, array("like", "dislike"))) {
            echo "Neznámý typ hlasu.";
            $success = FALSE;
        }
        if (Database::isExist("vote", "USER_ID = :userId AND TRANSLATED_TEXT_ID = :transId", array("userId" => $_SESSION["user"]["id"], "transId" => $transId))) {
            echo "Pro tento text si již hlasoval.";
            $success = FALSE;
        }
        if ($success) {
            Project::vote($transId, $type);
        }
        $this->rawOutput = TRUE;
    }

    public function project_settings($param) {
        $id = $_POST['pk'];
        $name = $_POST['name'];
        $value = $_POST['value'];
        switch ($name) {
            case "project-name":
                if (Database::isExist("project", "ID = :id AND USER_ID = :uid", array("id" => $id, "uid" => $_SESSION["user"]["id"]))) {
                    Database::update("project", array("NAME" => $value, "URL" => textToURI($value)), "ID = :id AND USER_ID = :uid", array("id" => $id, "uid" => $_SESSION["user"]["id"]));
                }

                break;
            case "project-description":
                if (Database::isExist("project", "ID = :id AND USER_ID = :uid", array("id" => $id, "uid" => $_SESSION["user"]["id"]))) {
                    Database::update("project", array("DESCRIPTION" => $value), "ID = :id AND USER_ID = :uid", array("id" => $id, "uid" => $_SESSION["user"]["id"]));
                }
                break;

            case "add-manager":
                $langId = $_POST["langId"];
                if (Database::isExist("project", "ID = :id AND USER_ID = :uid", array("id" => $id, "uid" => $_SESSION["user"]["id"]))) {
                    $userId = Database::getValue("user", "ID", "NICK = :nick", array("nick" => $value));
                    if (!empty($userId)) {
                        $x = Project::addManager($userId, $id, $langId);
                        if ($x) {
                            $photo = Database::getValue("user", "PHOTO", "NICK = :nick", array("nick" => $value));
                            echo "success:$photo";
                        } else {
                            echo "Uživatel již je správcem nebo majitelem.";
                        }
                    } else {
                        echo "Uživatel nenalezen.";
                    }
                }
                break;

            case "delete-manager":
                if (Database::isExist("project", "ID = :id AND USER_ID = :uid", array("id" => $id, "uid" => $_SESSION["user"]["id"]))) {
                    Project::deleteManager($value, $id);
                    echo "success:";
                }
                break;
            case "delete-photo":
                if (Database::isExist("project", "ID = :id AND USER_ID = :uid", array("id" => $id, "uid" => $_SESSION["user"]["id"]))) {
                    Database::update("project", array("PHOTO" => ""), "ID = :pid", array("pid" => $id));
                    echo "success:";
                }
                break;
        }
    }

    public function preexec($params) {
        $this->rawOutput = TRUE;
    }

    public function exec($params) {
        $this->rawOutput = TRUE;
    }

}
