<?php

/**
 * Controller for error page
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class ErrorController extends Controller {

    public function exec($params) {
        
        $this->data["title"] = "Chyba";
        $this->view = "error";
    }

}
