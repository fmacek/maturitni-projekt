<?php

/**
 * Main controller
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
// Výchozí kontroler pro
abstract class Controller {

    // Pole, jehož indexy jsou poté viditelné v šabloně jako běžné proměnné
    protected $data = array();
    // Název šablony bez přípony
    protected $view = "";
    protected $rawOutput = FALSE;

    // Ošetří proměnnou pro výpis do HTML stránky
    public static function treat($x = null) {
        if (!isset($x))
            return null;
        elseif (is_string($x))
            return htmlspecialchars($x, ENT_QUOTES);
        elseif (is_array($x)) {
            foreach ($x as $k => $v) {
                $x[$k] = self::treat($v);
            }
            return $x;
        } else
            return $x;
    }

    // Vyrenderuje pohled
    public function view() {
        if (isset($_POST["login"])) {
            Auth::logIn($_POST["email"], $_POST["pass"]);
        }
        if (isset($_POST["logout"])) {
            Auth::logOut();
        }
        $smarty = new Smarty;
        $smarty->caching = false;
        $this->data["_POST"] = $_POST;
        $this->data["_GET"] = $_GET;
        $this->data["webLangVersion"] = Database::getTable("web_lang_version", ["lang.ID", "lang.NAME", "lang.CODE", "web_lang_version.BETA"], "1", [], "JOIN lang ON web_lang_version.LANG_ID = lang.ID");

        foreach ($this->data as $key => $data) {
            $smarty->assign($key, $data);
        }
        $smarty->assign("lang", lang());

        $smarty->assign("alerts", Alert::get());
        $smarty->assign("user", $_SESSION["user"]);
        $smarty->assign("params", $params);


        if (!$this->rawOutput) {
            $smarty->display("view/inc/header.tpl");
        }
        if (!empty($this->view)) {
            $smarty->display("view/" . $this->view . ".tpl");
        }
        if (!$this->rawOutput) {
            $smarty->display("view/inc/footer.tpl");
            debug($_POST, '$_POST');
            debug($_GET, '$_GET');
            debug($_SESSION, '$_SESSION');
            if ($_GET["lang"]) {
                debug(lang(), '$lang');
            }
            $smarty->display("view/inc/postfooter.tpl");
        }
    }

    // Přesměruje na dané URL
    public function redirect($url) {
        header("Location: /$url");
        header("Connection: close");
        exit;
    }

    public function addAlert($type, $text) {
        Alert::add($type, $text);
    }

    public function getAlert($url) {
        Alert::get();
    }

    // Hlavní metoda controlleru
    abstract function exec($params);
}
