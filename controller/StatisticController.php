<?php

/**
 * Controller for statistics
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class StatisticController extends Controller {

    public function user($params) {
        if (isset($_POST["filter"])) {
            foreach ($_POST["lang"] as $key => $row) {
                if (!is_numeric($row)) {
                    unset($_POST["lang"][$key]);
                }
            }
            $langs = " LANG_ID IN(" . implode(", ", $_POST["lang"]) . ")";
        } else {
            $langs = "1=1";
        }

        $x = User::getAll();
        $users = [];
        foreach ($x as $user) {
            $id = $user->ID;
            $users[$id]["user"] = $user;
            $point = 0;
            $point = Database::getValue("point", "SUM(VALUE)", "point.USER_ID = :uid AND $langs", ["uid" => $id], "JOIN translated_text ON point.PARAMETER = translated_text.ID");
            $users[$id]["point"] = ($point)? $point: 0;
            $users[$id]["sug"] = Database::getValue("translated_text", "COUNT", "USER_ID = :uid AND $langs", ["uid" => $id]);
            $users[$id]["app"] = Database::getValue("translated_text", "COUNT", "USER_ID = :uid AND STATUS = 'approved' AND $langs", ["uid" => $id]);
            $users[$id]["vote"] = Database::getValue("vote", "COUNT", "vote.USER_ID = :uid AND $langs", ["uid" => $id], "JOIN translated_text ON vote.TRANSLATED_TEXT_ID = translated_text.ID");
        }
        $this->data["langs"] = Database::getTable("lang", array("ID", "NAME"), "STATUS = :status", array("status" => "visible"));
        $this->data["users"] = $users;
        $this->data["title"] = "Statistiky uživatelů";
        $this->view = 'statistic/user';
    }

    public function project($params) {
        $this->data["title"] = "Statistiky";
        $this->view = 'contact';
    }

    public function exec($params) {
        $this->user($params);
    }

}
