<?php

/**
 * Controller for projects
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class ProjectController extends Controller {

    public function info($params) {
        $projectId = Database::getValue("project", "ID", "URL = :url", array("url" => $params[0]));
        $project = Project::get($projectId);
        $owner = User::find($project->USER_ID, array("PHOTO", "NICK"));
        $managers = Project::getManagers($projectId, array("PHOTO", "NICK"));
        $projectTraslateTo = Project::getTranslateToLangs($projectId);
        $translateTo = $files = [];
        $textCount = Database::getValue("original_text", "COUNT", "PROJECT_ID = :pid AND STATUS != 'hidden'", ["pid" => $projectId]);
        foreach (Database::getTable("files", "*", "PROJECT_ID = :pid AND STATUS = :status", ["pid" => $projectId, "status" => "visible"]) as $file) {
            $files[$file->LANG_ID][] = $file;
        }

        foreach ($projectTraslateTo as $row) {


            $transTextCount = Database::getValue("translated_text", "COUNT", "translated_text.LANG_ID = :lid AND translated_text.STATUS = 'approved' AND original_text.PROJECT_ID = :pid", ["lid" => $row->LANG_ID, "pid" => $projectId], "JOIN original_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID");
            $done = ($textCount != 0) ? round($transTextCount / ($textCount / 100)) : 0;
            $notDone = 100 - $done;
            $translateTo[] = ["id" => $row->LANG_ID,
                "name" => $row->LANG,
                "favorite" => Database::isExist("favorite_project", "PROJECT_ID = :pid AND USER_ID = :uid AND LANG_ID = :lang_id", ["pid" => $project->ID, "uid" => $_SESSION["user"]["id"], "lang_id" => $row->LANG_ID]),
                "done" => $done,
                "not_done" => $notDone];
        }
        $this->data["project"] = array("id" => $project->ID,
            "name" => $project->NAME,
            "url" => $project->URL,
            "language" => $project->LANG,
            "image" => $project->THUMBNAIL,
            "description" => $project->DESCRIPTION,
            "owner" => $owner,
            "managers" => $managers,
            "translateTo" => $translateTo,
            "files" => $files
        );

        $this->data["title"] = "Informace o projektu " . $project->NAME;
        $this->view = "project/info";
    }

    public function text($params) {
        $project = Database::getRow("project", ["ID", "LANG_ID", "NAME"], "URL = :url", array("url" => $params[0]));
        $projectId = $project->ID;
        $langId = $this->data["lang"] = (!empty($params[1])) ? $params[1] : Project::getTranslateToLangs($projectId)[0]->LANG_ID;
        $page = $this->data["page"] = (!empty($params[3]) and $params[3] > 0) ? ceil($params[3]) : 1; //pageof texts
        $filter = !empty($params[2]) ? $params[2] : "non-approved";

        $textPerPage = 10; //how many originals per page display
        $countText = Project::countOriginals($projectId, $filter);
        $this->data["maxPage"] = ceil($countText / $textPerPage);
        $this->data["allowedToProcess"] = Project::allowedToProcess($projectId, $langId);
        $this->data["project"]["name"] = $project->NAME;
        $originals = Project::getOriginalText($projectId, $langId, $filter, $page, $textPerPage);
        $this->data["project"]["translateTo"] = Project::getTranslateToLangs($projectId);
        if ($originals) {
            foreach ($originals as $original) {
                $vocabularyId = $vocabulary = FALSE;
                $vocabularyId = Database::getValue("original_vocabulary", "ID", "TEXT = :text AND LANG_ID = :lid", ["text" => $original->TEXT, "lid" => $project->LANG_ID]);
                if ($vocabularyId) {
                    $vocabulary = Database::getTable("translated_vocabulary", ["translated_vocabulary.ID", "translated_vocabulary.TEXT"], "translated_vocabulary.ORIGINAL_VOCABULARY_ID = :ovid AND translated_vocabulary.USER_ID = :uid AND translated_vocabulary.LANG_ID = :lang", ["ovid" => $vocabularyId, "uid" => $_SESSION["user"]["id"], "lang" => $langId], "JOIN original_vocabulary ON translated_vocabulary.ORIGINAL_VOCABUlARY_ID = original_vocabulary.ID");
                }
                $translatedTexts = Database::getTable("translated_text", array("translated_text.ID", "user.NICK", "translated_text.TEXT", "translated_text.DESCRIPTION", "translated_text.STATUS"), "translated_text.ORIGINAL_TEXT_ID = :ori_id AND translated_text.LANG_ID = :lang AND translated_text.STATUS NOT IN ('closed',  'debug_closed')", array("ori_id" => $original->ID, "lang" => $params[1]), "JOIN user ON translated_text.USER_ID = user.ID");
                $suggestions = array();
                foreach ($translatedTexts as $translatedText) {
                    $like = Database::getValues("vote", "user.NICK AS `NICK`", "TRANSLATED_TEXT_ID = :trans_id AND type = :type", array("trans_id" => $translatedText->ID, "type" => "like"), "JOIN user ON vote.USER_ID = user.ID");
                    $dislike = Database::getValues("vote", "user.NICK AS `NICK`", "TRANSLATED_TEXT_ID = :trans_id AND type = :type", array("trans_id" => $translatedText->ID, "type" => "dislike"), "JOIN user ON vote.USER_ID = user.ID");
                    $suggestions[$translatedText->ID] = array("id" => $translatedText->ID, "author" => $translatedText->NICK, "text" => $translatedText->TEXT, "description" => $translatedText->DESCRIPTION, "status" => $translatedText->STATUS, "like" => $like, "dislike" => $dislike);
                }
                $this->data["texts"][$original->ID] = array("id" => $original->ID, "id_web" => $original->ID_WEB, "text" => $original->TEXT, "description" => $original->DESCRIPTION, "suggestions" => $suggestions, "status" => $original->STATUS, "vocabulary" => $vocabulary);
            }
        }
        $this->data["title"] = "Texty k přeložení projektu " . $project->NAME;
        $this->view = "project/text";
    }

    public function export_preview($params) {
        $project = Database::getRow("project", ["ID", "LANG_ID", "NAME"], "URL = :url", array("url" => $params[0]));
        $projectId = $project->ID;
        $langId = $this->data["lang"] = (!empty($params[1])) ? $params[1] : Project::getTranslateToLangs($projectId)[0]->LANG_ID;

        $this->data["project"]["name"] = $project->NAME;
        $originals = Database::getTable("original_text", ["ID", "ID_WEB", "DESCRIPTION", "TEXT", "STATUS"], "PROJECT_ID = :project_id AND STATUS != :status", array("project_id" => $projectId, "status" => "hidden"));
        $traslates = Project::getTranslateToLangs($projectId);
        $textCount = Database::getValue("original_text", "COUNT", "PROJECT_ID = :pid AND STATUS != 'hidden'", ["pid" => $projectId]);

        $translateTo = [];
        foreach ($traslates as $row) {
            $transTextCount = Database::getValue("translated_text", "COUNT", "translated_text.LANG_ID = :lid AND translated_text.STATUS = 'approved' AND original_text.PROJECT_ID = :pid", ["lid" => $row->LANG_ID, "pid" => $projectId], "JOIN original_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID");
            $progress = ($textCount != 0) ? round($transTextCount / ($textCount / 100)) : 0;

            $translateTo[] = ["lang" => $row->LANG, "id" => $row->LANG_ID, "progress" => $progress];
        }

        $this->data["project"]["translateTo"] = $translateTo;
        if ($originals) {
            foreach ($originals as $original) {
                $vocabularyId = $vocabulary = FALSE;
                $vocabularyId = Database::getValue("original_vocabulary", "ID", "TEXT = :text AND LANG_ID = :lid", ["text" => $original->TEXT, "lid" => $project->LANG_ID]);
                if ($vocabularyId) {
                    $vocabulary = Database::getTable("translated_vocabulary", ["translated_vocabulary.ID", "translated_vocabulary.TEXT"], "translated_vocabulary.ORIGINAL_VOCABULARY_ID = :ovid AND translated_vocabulary.USER_ID = :uid AND translated_vocabulary.LANG_ID = :lang", ["ovid" => $vocabularyId, "uid" => $_SESSION["user"]["id"], "lang" => $langId], "JOIN original_vocabulary ON translated_vocabulary.ORIGINAL_VOCABUlARY_ID = original_vocabulary.ID");
                }
                $translatedText = Database::getRow("translated_text", array("translated_text.ID", "user.NICK", "translated_text.TEXT", "translated_text.DESCRIPTION", "translated_text.STATUS"), "translated_text.ORIGINAL_TEXT_ID = :ori_id AND translated_text.LANG_ID = :lang AND translated_text.STATUS = 'approved'", array("ori_id" => $original->ID, "lang" => $params[1]), "JOIN user ON translated_text.USER_ID = user.ID");
                $this->data["texts"][$original->ID] = array("id" => $original->ID, "id_web" => $original->ID_WEB, "text" => $original->TEXT, "description" => $original->DESCRIPTION, "translate" => $translatedText, "status" => $original->STATUS, "vocabulary" => $vocabulary);
            }
        }

        $this->data["title"] = "Náhled překladu projektu" . $project->NAME;
        $this->view = "project/export-preview";
    }

    public function export($params) {
        
    }

    public function add($params) {
        $this->data["langs"] = Database::getTable("lang", "ID, NAME", "STATUS = 'visible'");

        if (isset($_POST["add_project"])) {
            $data = Controller::treat($_POST); //data protection
            $projectId = Project::add($data["name"], $data["description"], $data["source_lang"], $data["translateTo"]); //add project
            if (is_numeric($projectId)) {
                $filename = File::upload($_FILES["source_file"], $projectId); //upload text file
                Project::import("files/$projectId/import/$filename", $projectId); //import text file
                Database::update("project", ["FILENAME" => $filename], "ID = :pid", ["pid" => $projectId]);
            }
        }
        $this->data["title"] = "Přidání projektu";
        $this->view = "project/import/new";
    }

    public function import($params) {
        $this->view = "project/import/exist";
    }

    public function my_projects($params) {
        $projects = Database::getTable("project", "project.*, lang.NAME AS `LANG`", "USER_ID = :uid", array("uid" => $_SESSION["user"]["id"]), "JOIN lang ON project.LANG_ID = lang.ID");
        foreach ($projects as $project) {
            $this->data["projects"][] = array("id" => $project->ID,
                "name" => $project->NAME,
                "url" => $project->URL,
                "language" => $project->LANG,
                "image" => $project->THUMBNAIL,
                "description" => $project->DESCRIPTION,
                "user_id" => $project->USER_ID,
                "translateTo" => Project::getTranslateToLangs($project->ID, FALSE)
            );
        }
        $this->data["title"] = "Výpis mých projektů";
        $this->view = "project/list";
    }

    public function settings($params) {
        $projectId = Database::getValue("project", "ID", "URL = :url", array("url" => $params[0]));
        if (isset($_POST["export"])) {
            $ok = TRUE;
            if ($_POST["lang"] <= 0) {
                Alert::add("warning", "Musíte zvolit jazyk k přeložení");
                $ok = FALSE;
            }
            if (!isset($_POST["type"])) {
                Alert::add("danger", "Není zvolen typ překadu!");
                $ok = FALSE;
            }
            if (empty($_POST["version"])) {
                Alert::add("warning", "Musíte pojmenovat verzi");
                $ok = FALSE;
            }
            if ($ok) {
                $status = (isset($_POST["status"])) ? "visible" : "hidden";
                Project::export($projectId, $_POST["lang"], $_POST["type"], $_POST["version"], $_POST["description"], $status);
            }
        }
        $project = Project::get($projectId);
        $files = [];
        foreach (Database::getTable("files", "*", "PROJECT_ID = :pid", ["pid" => $projectId]) as $file) {
            $files[$file->LANG_ID][] = $file;
        }

        $traslates = Project::getTranslateToLangs($projectId);
        $textCount = Database::getValue("original_text", "COUNT", "PROJECT_ID = :pid AND STATUS != 'hidden'", ["pid" => $projectId]);
        $translateTo = [];
        foreach ($traslates as $row) {
            $transTextCount = Database::getValue("translated_text", "COUNT", "translated_text.LANG_ID = :lid AND translated_text.STATUS = 'approved' AND original_text.PROJECT_ID = :pid", ["lid" => $row->LANG_ID, "pid" => $projectId], "JOIN original_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID");
            $progress = ($textCount != 0) ? round($transTextCount / ($textCount / 100)) : 0;

            $translateTo[] = ["name" => $row->LANG, "id" => $row->LANG_ID, "done" => $progress];
        }

        $this->data["project"] = array("id" => $project->ID,
            "name" => $project->NAME,
            "url" => $project->URL,
            "language" => $project->LANG_ID,
            "image" => $project->THUMBNAIL,
            "description" => $project->DESCRIPTION,
            "user_id" => $project->USER_ID,
            "translateTo" => $translateTo,
            "managers" => Project::getManagers($projectId, array("ID", "NICK", "PHOTO")),
            "files" => $files
        );
        $this->data["langs"] = Database::getTable("lang", array("ID", "NAME"), "STATUS = :status", array("status" => "visible"));

        $this->data["title"] = "Nastavení projektu " . $project->NAME;
        $this->view = "project/settings";
    }

    public function projects_list($params) {

        $this->data["langs"] = Database::getTable("lang", array("ID", "NAME"), "STATUS = :status", array("status" => "visible"));

        if (isset($_POST["filter_project"])) {
            $fromlang = $_POST["fromlang"];
            $tolang = $_POST["tolang"];
            $fromIn = $fromlangValue = $toIn = $tolangValue = array();
            if (!empty($fromlang)) {
                foreach ($fromlang as $key => $value) {
                    $fromIn[] = ":langfrom$key";
                    $fromlangValue["langfrom$key"] = $value;
                }
                $fromIn = "project.LANG_ID IN (" . implode(", ", $fromIn) . ")"; //výsledkem je IN (:langfrom0, :langfrom1,...)
            } else {
                $fromIn = "1=1"; //pokud ne, do SQL se doplní univerzální 1=1
            }
            if (!empty($tolang)) {//pokud byl zvolen cílový jazyk
                foreach ($tolang as $key => $value) {
                    $toIn[] = ":langin$key";
                    $tolangValue["langin$key"] = $value;
                }
                $toIn = "project_translate_to.LANG_ID IN (" . implode(", ", $toIn) . ")"; //výsledkem je IN (:langto0, :langto1,...)
            } else {
                $toIn = "1=1"; //pokud ne, do SQL se doplní univerzální 1=1
            }

            $projects = Database::getTable("project", "project.*, lang.NAME AS `LANG`", "$fromIn AND $toIn AND VISIBLE = 1 GROUP BY project.ID", array_merge($tolangValue, $fromlangValue), "JOIN lang ON project.LANG_ID = lang.ID LEFT JOIN project_translate_to ON project_translate_to.PROJECT_ID = project.ID");
        } else {
            $projects = Project::get();
        }
        foreach ($projects as $project) {
            $this->data["projects"][] = array("id" => $project->ID,
                "name" => $project->NAME,
                "url" => $project->URL,
                "language" => $project->LANG,
                "image" => $project->THUMBNAIL,
                "description" => $project->DESCRIPTION,
                "user_id" => $project->USER_ID,
                "translateTo" => Project::getTranslateToLangs($project->ID, FALSE),
            );
        }

        $this->data["title"] = "Seznam projektů";
        $this->view = "project/list";
    }

    public function exec($params) {
        $this->projects_list($params);
    }

}
