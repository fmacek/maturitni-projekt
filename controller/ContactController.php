<?php

/**
 * Controller for user
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class ContactController extends Controller {

    public function exec($params) {

        if (isset($_POST["email"])) {
            if ($_POST['year'] == date("Y")) {
                $email = new Email();
                $email->send("admin@adresa.cz", "Email z webu", $_POST['zprava'], $_POST['email']);
            }
        }
        $this->data["title"] = "Kontakt";
        $this->data["head"] = "Kontakt";

        $this->data["content"] = "Jakékoliv připomínky můžete posílat na preklad@filipmacek.cz";

        $this->view = 'article/single';
    }

}
