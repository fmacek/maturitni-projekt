<?php

/**
 * Controller for articles
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
// Controller pro výpis článků

class ArticleController extends Controller {

    public function exec($params) {
        // Vytvoření instance modelu, který nám umožní pracovat s články
        $articleManager = new ArticleManager();

        // Je zadáno URL článku
        if (!empty($params[0])) {
            // Získání článku podle URL
            $article = $articleManager->returnArtice($params[0]);
            // Pokud nebyl článek s danou URL nalezen, přesměrujeme na ChybaController
            if (!$article)
                $this->redirect('error');


            // Naplnění proměnných pro šablonu		
            $this->data['head'] = $article['title'];
            $this->data['content'] = $article['content'];

            // Nastavení šablony
            $this->data["title"] = $article['title'];
            $this->view = 'article/single';
        }
        else {
            // Není zadáno URL článku, vypíšeme všechny
            $articles = $articleManager->returnArticles();
            $this->data['articles'] = $articles;
            
            $this->data["title"] = "Seznam článků";
            $this->view = 'article/list';
        }
    }

}
