<?php

/**
 * Controller for user
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class UserController {

    public function login($param) {
        $this->data["title"] = "Přihlášení uživatele";
        $this->view = "user/login";
    }

    public function logout($param) {
        Auth::logOut();
        $this->view = "user/logout";
    }

    public function register($param) {
        if (isset($_POST["register"])) {
            Auth::register($_POST);
        }
        $this->data["title"] = "Registrace";
        $this->view = "user/register";
    }

    public function authorize($param) {

        Auth::authorize($param[0], $param[1]);
        $this->data["title"] = "Autorizace účtu";
        $this->view = "article/single";
    }

    public function settings($param) {
        if (isset($_POST["change_profile_info"])) {
            User::changeData($_POST);
        }
        if (isset($_POST["change_profile_password"])) {
            User::changePassword($_POST["oldpass"], $_POST["newpass"], $_POST["newpass2"]);
        }

        $this->data["title"] = "Nastavení účtu";
        $this->view = "user/settings";
    }

    public function vocabulary($param) {
        $this->data["languages"] = Language::get();

        if ($param[0] == "add") {
            if (isset($_POST["add_vocabulary"])) {
                $original[] = ["text" => $_POST["sourcetext"], "lang" => $_POST["sourcelang"]];
                foreach ($_POST["translatedtext"] as $id => $text) {
                    foreach ($text as $key => $value) {
                        if (!empty($value)) {
                            $translated[$id][] = ["text" => $value, "lang" => $id];
                        }
                    }
                }
                Vocabulary::addToUserVocabulary($original, $translated);
            }
            $this->data["title"] = "Přidání slova do slovníku";
            $this->view = "vocabulary/add";
        } else {
            $this->data["vocabularys"] = Vocabulary::getUserVocabulary($_GET["sourcelang"], $_GET["tolang"]);

            $this->data["title"] = "Osobní slovník";
            $this->view = "vocabulary/main";
        }
    }

    public function profile($param) {
        if (!empty($param[0])) {
            $id = Database::getValue("user", "ID", "NICK = :nick", array("nick" => $param[0]));
            $user = User::find($id);
        } else {
            $user = User::find($_SESSION["user"]["id"]);
        }
        $this->data["profile"] = $user;
        $this->data["title"] = "Profil uživatele " . $user->NICK;
        $this->view = "user/profile";
    }

    public function exec($params) {
        
    }

}
