<?php

/**
 * Router controller, calling current controller
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
// Router je speciální typ controlleru, který podle URL adresy zavolá
// správný controller a jím vytvořený pohled vloží do šablony stránky

class Router extends Controller {

    // Instance controlleru
    protected $controller;

    private function camelCaps($text) {
        $camelCaps = str_replace('-', ' ', $text);
        $camelCaps = ucwords($camelCaps);
        $camelCaps = str_replace(' ', '', $camelCaps);
        return $camelCaps;
    }

    // Naparsuje URL adresu podle lomítek a vrátí pole parametrů
    private function parseURL($url) {
        // Naparsuje jednotlivé části URL adresy do asociativního pole
        $parsedURL = parse_url($url);
        // Odstranění počátečního lomítka
        $parsedURL["path"] = ltrim($parsedURL["path"], "/");
        // Odstranění bílých znaků kolem adresy
        $parsedURL["path"] = trim($parsedURL["path"]);
        // Rozbití řetězce podle lomítek
        $array = explode("/", $parsedURL["path"]);
        return $array;
    }

    // Naparsování URL adresy a vytvoření příslušného controlleru
    public function exec($params) {
        $parsedURL = $parsedURLBackup = $this->parseURL($params);
        $this->data["url"] = $parsedURL;

        if (empty($parsedURL[0])) {
            $this->redirect('home');
        }
        $parsedURL[1] = str_replace("-", "_", $parsedURL[1]);

        // kontroler je 1. parametr URL
        $classController = $this->camelCaps(array_shift($parsedURL));
        if (file_exists('controller/' . $classController . 'Controller.php')) {
            $classController = $classController . "Controller";
            if ($classController == "AjaxController") {
                $this->rawOutput = TRUE;
            }
            $this->controller = new $classController;
            // Volání controlleru
            if (method_exists($this->controller, "preexec")) {
                $this->controller->preexec($parsedURL);
            }
            if (method_exists($this->controller, $parsedURL[0])) {
                $method = array_shift($parsedURL);
                $this->controller->$method($parsedURL);
            } else {

                $this->controller->exec($parsedURL);
            }
            if (method_exists($this->controller, "postexec")) {
                $this->controller->preexec($parsedURL);
            }
        } elseif (Database::isExist("eshop_article", "URL = :url", ["url" => $parsedURLBackup[0]])) {
            $this->controller = new ArticleController();
            $this->controller->exec($parsedURLBackup);
        } else {
            $this->controller = new ErrorController();
            $this->controller->exec($parsedURL);
        }
        // Nastavení proměnných pro šablonu

        if (!empty($this->controller->data)) {
            foreach ($this->controller->data as $key => $value) {
                $this->data[$key] = $value;
            }
        }
        // Nastavení hlavní šablony
        $this->view = $this->controller->view;
    }

}
