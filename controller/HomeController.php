<?php

/**
 * Controller for homepage
 * @author Filip Macek <macek97@gmail.com>
 * @version 1.0
 */
class HomeController extends Controller {

    public function exec($params) {
        $users = Database::getTable("user", ["user.NICK", "SUM(point.VALUE) AS `POINT`"], "1 GROUP BY user.ID ORDER BY POINT DESC LIMIT 5", [], "JOIN `point` ON point.USER_ID = user.ID ");
        $topProjects = [];
        foreach (Database::getTable("project", ["ID", "NAME"], "STATUS = 'active'") as $project) {
            $projectTransTo = Project::getTranslateToLangs($project->ID);
            $done = 0;
            foreach ($projectTransTo as $lang) {
                $textCount = Database::getValue("original_text", "COUNT", "PROJECT_ID = :pid AND STATUS != 'hidden'", ["pid" => $project->ID]);
                $transTextCount = Database::getValue("translated_text", "COUNT", "translated_text.LANG_ID = :lid AND translated_text.STATUS = 'approved' AND original_text.PROJECT_ID = :pid", ["lid" => $lang->LANG_ID, "pid" => $project->ID], "JOIN original_text ON translated_text.ORIGINAL_TEXT_ID = original_text.ID");
                $done += ($textCount != 0) ? round($transTextCount / ($textCount / 100)) : 0;
            }
            $done /= count($projectTransTo);
            if ($done != 100) {
                $topProjects[] = ["project" => $project, "done" => $done];
            }
        }
        usort($topProjects, function($a, $b) {
            return $b["done"] - $a["done"];
        });
        $topProjects = array_slice($topProjects, 0, 5);
        $this->data["statistics"]["projectCount"] = Database::getValue("project", "COUNT", "STATUS = 'active'");
        $this->data["statistics"]["textCount"] = Database::getValue("original_text", "COUNT", "STATUS != 'hidden'");
        $this->data["statistics"]["translateCount"] = Database::getValue("translated_text", "COUNT", "STATUS = 'approved'");
        $this->data["statistics"]["userCount"] = Database::getValue("user", "COUNT", "STATUS IN ('user', 'admin')");
        $this->data["statistics"]["pointCount"] = Database::getValue("point", "SUM(`VALUE`)", "STATUS = 'active'");
        $this->data["topUsers"] = $users;
        $this->data["topProjects"] = $topProjects;
        $this->data["head"] = "Překlady";
        $this->data["content"] = lipsum("all");

        $this->data["title"] = "Hlavní strana";
        $this->view = "home";
    }

}
