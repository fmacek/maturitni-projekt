<?php


function textToURI($title) {
    static $convertTable = array(
        'á' => 'a', 'Á' => 'A', 'ä' => 'a', 'Ä' => 'A', 'č' => 'c',
        'Č' => 'C', 'ď' => 'd', 'Ď' => 'D', 'é' => 'e', 'É' => 'E',
        'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'í' => 'i',
        'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ľ' => 'l', 'Ľ' => 'L',
        'ĺ' => 'l', 'Ĺ' => 'L', 'ň' => 'n', 'Ň' => 'N', 'ń' => 'n',
        'Ń' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O',
        'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R', 'š' => 's',
        'Š' => 'S', 'ś' => 's', 'Ś' => 'S', 'ť' => 't', 'Ť' => 'T',
        'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u',
        'Ü' => 'U', 'ý' => 'y', 'Ý' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y',
        'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z',
    );
    $title = strtolower(strtr($title, $convertTable));
    $title = preg_replace('/[^a-zA-Z0-9]+/u', '-', $title);

   // do {
        $title = str_replace('--', '-', $title);
        $old = $title;
   // } while ($old != $title);

    $title = trim($title, '-');

    return $title;
}


$examples = ["ifgvud.f sdfuifui", "příliš žluťoučký kůň úpěl ďábelské ódy", "kůň", "Můj úplně božský projektíkček no #2 (CZ)", "Acer A,spire V165#;15 Nitro (VN7-571G- (CZ)", "---------------", "aby se vědělo kdo co tam dělá právě kvůli tomu když někdo udělá trestný čin", "Texty písní – Japonské – ポケットモンスター - Pikachu.cz", "(Katakana) 片仮名; アァ, イィ, ウゥヴ, エェ, オォ, カヵガ, キギ, クグ, ケヶゲ, コゴ, サザ, シジ, スズ, セゼ, ソゾ, タダ, チヂ, ツッヅ, テデ, トド, ナ, ニ, ヌ, ネ, ノ, ハバパ, ヒビピ, フブプ, ヘベペ, ホボポ, マ, ミ, ム, メ, モ, ヤャ, ユュ, ヨョ, ラ, リ, ル, レ, ロ, ワヮ, ヰ, ヱ, ヲ, ン, ー, ..."];

foreach ($examples as $text) {
    echo "<u>$text (".  mb_strlen($text).")</u> - ".textToURI($text)."(".mb_strlen(textToURI($text)).")<br />";
    
    
}