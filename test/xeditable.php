
<?php //jendořádkový komentář, toto je začátek PHP
/*
  Víceřádkový komentář
  PHP umí například tyto věci
 */
echo "hello world";
$text = "100";
$number = 100;
if ($text == $number) {
    echo "PHP nehledí na datový typ <br />";
} else {
    echo "Toto nenastane <br />";
}
for ($i = 0; $i < 10; $i++) {
    echo "Je možné takto slepovat řetězce i funkce" . rand(0, 10) . 'V apostrovech můžete napsat    
             $promenna a nic se nestane <br />';
}
highlight_file(__FILE__); //jednořádkový komentář. vypsání kódu aktuálního souboru (__FILE__ je implementovaná definice v PHP)