<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title></title>
        <link href="../inc/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen" />
        <link href="../inc/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <link href="../inc/style.css" rel="stylesheet" />
        <link rel="shortcut icon" href="../img/cz.ico" />
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js'></script>
        <meta name="description" content="Komunitní češtiny - Od hráčů pro hráče! Pojdťe překládat hry s námi a pro nejlepší bude i odměna!" />
        <meta name="keywords" content="čeština,komunitní češtiny, překlad her, Tropico 5, Goat Simulator, Cities: Skylines" />
        <meta name="robots" content="index,follow" />
        <meta name="author" content="Filip Macek" />

        <meta name="google-signin-client_id" content="466576004805-abacbb0u89suus3o8tmoenod8vm5i1ob.apps.googleusercontent.com" />
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("div").click(function () {
                    var id = $(this).attr('id'); // This would be 'some-id' in our example
                    var data = $(this).data('target'); // This would be 'some-id' in our example
                            var dataString = {"id": id, "data":data}

                    $.ajax({
                        type: "post",
                        url: "ajax.php",
                        data: dataString,
                        success: function (html) {
                            $('#feedback').html(html);
                        }
                    });
                });
            });

        </script>
    </head>
    <body>
    <center><div id="feedback" class="alert alert-success" style="font-size: 16px; font-weight: bold; background: #eee; padding: 10px; color: green;"></div></center>

    <?php
    for ($i = 0; $i <= 10; $i++) {
        echo " <div id='$i' class='btn btn-warning' data-target='sms_box' >
     $i XXX
 </div>";
    }
    ?>
</body>
</html>