<?php
session_start();
require_once '../inc/libs/FacebookSDK/src/Facebook/autoload.php';
require_once '../inc/fce.php';

$fb = new Facebook\Facebook([
  'app_id' => '1609062912680709',
  'app_secret' => '33b906e401ecd20051977cfe468d49fe',
  'default_graph_version' => 'v2.2',
  ]);
$helper = $fb->getRedirectLoginHelper();

$permissions = ['email', 'public_profile', 'user_about_me', 'user_birthday']; // Optional permissions
$loginUrl = $helper->getLoginUrl('http://mvc.komunitnicestiny.cz/test/fbcallback.php', $permissions);

echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
