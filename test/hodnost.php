

<table border="1">
    <tr><th>Funkce</th><th>Název</th><th>popis práce</th><tr>
    <tr><td>admin</td><td>Bůh</td><td>Prostě já</td><tr>
    <tr><td>majitel projektu</td><td>nakladatelství</td><td>člověk který má aktivní projekt</td><tr>
    <tr><td>schvalovač</td><td>odškrtávač</td><td>člověk co má povolení schvalovat návrhy</td><tr>
    <tr><td>správce slovníku</td><td>sběratel slov</td><td>schvaluje nový návrhy do globálního slovníku, (vyšší úroveň ruší špatný)</td><tr>
    <tr><td>feedbacker/support</td><td>zpovědník</td><td>Odpovídá na feedback a support. případně přeposílá na jiné funkce</td><tr>
    <tr><td>správce uživatelů</td><td>Dektetiv</td><td>kontroluje uživatele (nicky), o mně,fotky,...</td><tr>
    <tr><td>správce návštěvní knihy</td><td>Správce zdi nářků</td><td>odpovídá na dotazy a jiné reakce na návštěvní knize, maže nevhodné příspěvky</td><tr>
    <tr><td>správce území (jazykové mutace webu)</td><td>Správce nářečí</td><td>kontroluje jazykovou mutaci webu, vyřizuje nahlašování špatného překladu.</td><tr>
</table>
<br /><br /><br />
<table border="1">
    <tr><th>#</th><th>Hodnost</th><th>Počet XP</th><tr>

        <?php
        $hodnosti = ["slabikář", "mluvící batole", "zvídavé dítě", "čtečka", "zabiják diktátů", "student jazykové školy", "šprt", "strojový překladač", "gramar nazi", "slovník", "učitel", "básník", "jazykový profesor", "katedra jazykovědců", "překladatelská firma", "jazykový expert", "jazykový ústav", "Stvořitel jazyka"];
        $xp = 100;
        $pos = 1;
        foreach ($hodnosti as $hodnost) {
            for ($i = 1; $i <= 3; $i++) {
                $star = ["1" => "*", "**", "***", "****", "*****"];
                echo "<tr><td>$pos</td><td><b>$hodnost $star[$i]</b></td><td> " . number_format($xp, 0, ',', ' ') . "</td><tr>";
                $xp = round($xp + ($xp / 4));
                $pos++;
            }
        }
        ?>
</table>
